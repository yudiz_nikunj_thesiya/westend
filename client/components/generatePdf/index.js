
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import moment from 'moment';

const generatePDF = (tickets, Partner_Name, Company_Name, Email) => {
  
  var today = moment(new Date()).format('DD/MM/YYYY');
  var newdate = 'Date: ' + today;
  var Partner_Name = 'Partner_Name : ' + Partner_Name;
  var Company_Name = 'Company_Name : ' +Company_Name;
  var Email = 'Email :' + Email;

  const doc = new jsPDF();
  const tableColumn = [
    'Partner_Name',
    'Company_Name',
    'Date/Time',
    'Email',
    
  ];
  const tableRows = [];
  // console.log('tickets', tickets);
  tickets.forEach((ticket) => {
    const ticketData = [
      ticket.name,
      ticket.company_name,
      moment(ticket.createdAt).format('DD/MM/YYYY HH:mm'),
      ticket.email,
      //   format(new Date(ticket.updated_at), 'yyyy-MM-dd'),
    ];
    tableRows.push(ticketData);
  });
  // doc.text(145, 8, newdat);
  doc.setFont('', 'bold');
  doc.text('Partner Data', 105, 10, null, null, 'center');
  doc.text(newdate, 160, 25);
  // doc.text('Admin’s client :  ', 14, 35);
  doc.setFont('', 'bold');
  doc.text(Partner_Name, 75, 30, null, null, 'right');
  doc.autoTable(tableColumn, tableRows, {
    startY: 54,
    theme: 'grid',
    styles: {
      halign: 'center',
    },
  });
  doc.setFontSize(10);
  // doc.text('Total Re-Invoicing : ', 185, 100, null, null, 'right');
  // doc.text('Total Amount : ', 185, 105, null, null, 'right');
  doc.setTextColor(255, 0, 0);
  // doc.text(totalReinvoices, 195, 100, null, null, 'right');
  // doc.text(totalPrice, 195, 105, null, null, 'right');
  const date = Date().split(' ');
  const dateStr = date[0] + '_' + date[1] + '_' + date[2];
  // doc.text('Hello downloaded the  tickets within the last one month.', 14, 30);
  doc.save(`${dateStr}.pdf`);
};
export default generatePDF;