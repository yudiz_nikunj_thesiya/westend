import React from "react";
import Router from "next/router";
import dynamic from "next/dynamic";
const OwlCarousel = dynamic(import("react-owl-carousel3"));

import ImageCard from "../../components/ImageCard";
import Carousel from "./Carosoul";
const options = {
  loop: true,
  nav: true,
  margin: 60,
  items: 6,
  dots: false,
  //   autoplayTimeout: 2000,
  autoplaySpeed: 500,
  autoplayHoverPause: false,
  autoplay: true,
  navText: [
    "<i class='bx bx-chevron-left'></i>",
    "<i class='bx bx-chevron-right'></i>",
  ],
  responsive: {
    0: {
      items: 2,
      margin: 20,
    },
    600: {
      items: 3,
    },
    768: {
      items: 4,
      margin: 30,
    },
    1000: {
      items: 5,
    },
  },
};
const Countries = () => {
  const [display, setDisplay] = React.useState(false);

  React.useEffect(() => {
    setDisplay(true);
  }, []);
  return (
    <div className="partner-area ptb-70 bg-f9fbff">
      <div className="container">
        <h2 className="section-title">
            <b>Countries</b>
        </h2> 
        <div className="row align-items-center">
        <div className="col-lg-12 col-md-12">
          {/* <Carousel /> */}
          {display ? (
            <OwlCarousel
              className="partner-slides owl-carousel owl-theme"
              {...options}
            >
              <div
                onClick={() => Router.push("/country/0")}
                className="single-partner-item"
              >
                <ImageCard src="/images/partner/united-states.png" alt="image" />
                <div>United States</div>
              </div>

              <div
                onClick={() => Router.push("/country/1")}
                className="single-partner-item"
              >
                <ImageCard src="/images/partner/australia.png" alt="image" />
                <div>Australia</div>
              </div>

              <div
                onClick={() => Router.push("/country/2")}
                className="single-partner-item"
              >
                <ImageCard src="/images/partner/uk.png" alt="image" />
                <div>United Kingdom</div>
              </div>

              <div
                onClick={() => Router.push("/country/3")}
                className="single-partner-item"
              >
                <ImageCard src="/images/partner/canada.png" alt="image" />
                <div>Canada</div>
              </div>

              <div
                onClick={() => Router.push("/country/4")}
                className="single-partner-item"
              >
                <ImageCard src="/images/partner/new-zealand.png" alt="image" />
                <div>New Zealand</div>
              </div>

              <div
                onClick={() => Router.push("/country/5")}
                className="single-partner-item"
              >
                <ImageCard src="/images/partner/germany.png" alt="image" />
                <div>Germany</div>
              </div>
            </OwlCarousel>
          ) : (
            ""
          )}
          <br></br>
          <br></br>
          <p>We support your education dreams at the country of your choice. Your dreams should not stop just because of temporary financial crunch. We understand how important it is to fulfil.</p>
      </div>
      </div>
      </div>
    </div>
  );
};

export default Countries;
