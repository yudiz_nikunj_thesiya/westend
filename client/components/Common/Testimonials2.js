import React from "react";
import Router from "next/router";

const Testimonials2 = () => {
  const [display, setDisplay] = React.useState(false);
  const [activeTab, setActiveTab] = React.useState(1);
  const [activeIndex, setactiveIndex] = React.useState(1);
  React.useEffect(() => {
    setDisplay(true);
  }, []);
  const toggle = async (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
    setactiveIndex(tab);
  };

  React.useEffect(() => {
    console.log(Router, "Router.pathname");
    if (Router.pathname == "/") {
      var i = 0;
      var myInterval = setInterval(() => {
        var elements = document.querySelectorAll(
          ".about-image .col-lg-6 .image"
        );
        let temp = elements.length;
        if (temp > 0) {
          if (i <= temp) {
            elements[i].classList.add("shows");
            if (i > 0) {
              elements[i - 1].classList.remove("shows");
            }
          }

          i++;
          setActiveTab(i);

          if (i > 3) {
            setTimeout(() => {
              elements[3].classList.remove("shows");
            }, 2000);

            i = 0;
          }
        }
      }, 3000);
    }
  }, []);
  function Page() {
    if (activeTab == 1) {
      return (
        <div className="single-feedback-item">
          <p>
            Westend has been a great support by helping and guiding me
            throughout my loan process and supported me alot. I have even
            referred them to my few friends and they were also glad to have such
            strong and reliable support.
            <br></br>
            <br></br>{" "}
            <div className="right plr-50">
              <b>- Rushi Modi</b>
            </div>
          </p>
        </div>
      );
    } else if (activeTab == 2) {
      return (
        <div className="single-feedback-item">
          <p>
            With the help of Westend Educorp, I got my loan sanctioned in 5
            working days. I had absolutely lost hopes before I found this
            service. Thankyou so much! 10/10 would definitely recommend.
            <br></br>
            <br></br>{" "}
            <div className="right plr-50">
              <b>- Prerna Jani</b>
            </div>
          </p>
        </div>
      );
    } else if (activeTab == 3) {
      return (
        <div className="single-feedback-item">
          <p>
            I had the best experience with westend. Very cooperative people and
            would help you out with every query that you have and also give best
            advice.
            <br></br>
            <br></br>{" "}
            <div className="right plr-50">
              <b>- Prakruti Prajapati</b>
            </div>
          </p>
        </div>
      );
    } else if (activeTab == 4) {
      return (
        <div className="single-feedback-item">
          <p>
            I would highly recommend this place for all your financial and
            education loan purposes as the staff over their is very kind and
            do's your all process while keep on updating you about your things
            and even they have solution of all problems.
            <br></br>
            <br></br>{" "}
            <div className="right plr-50">
              <b>- Priyam Shah</b>
            </div>
          </p>
        </div>
      );
    }
  }

  return (
    <div className="about-area ptb-100 bg-fff8f8">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-6 col-md-12">
            <div className="imageAnimate">
              <div className="about-image d-flex">
                <div
                  className="col-lg-6 col-sm-6 col-md-6 col-6"
                  onClick={() => toggle(1)}
                >
                  <div className="image">
                    <img src="/images/students/rushi-modi.jpg" alt="image" />
                  </div>
                </div>
                <div
                  className="col-lg-6 col-sm-6 col-md-6 col-6"
                  onClick={() => toggle(2)}
                >
                  <div className="image">
                    <img src="/images/students/prerna-jani.png" alt="image" />
                  </div>
                </div>
              </div>
              <div className="about-image d-flex ">
                <div
                  className="col-lg-6 col-sm-6 col-md-6 col-6"
                  onClick={() => toggle(3)}
                >
                  <div className="image">
                    <img src="/images/students/prakruti.jpg" alt="image" />
                  </div>
                </div>
                <div
                  className="col-lg-6 col-sm-6 col-md-6 col-6"
                  onClick={() => toggle(4)}
                >
                  <div className="image">
                    <img src="/images/students/priyam-shah.jpg" alt="image" />
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-6 col-md-12">
            <div className="about-content">
              <div className="feedback-content">
                <span className="sub-title">
                  We don't speak, Our Students do
                </span>
                <h2>Our Student's Feedback</h2>
                <br></br>
                <div className="td-text-area">
                  <div className="chevronIcons">{Page()}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="shape1">
        <img src="/images/shape1.png" alt="image" />
      </div>
      <div className="shape2">
        <img src="/images/shape2.png" alt="image" />
      </div>
      <div className="shape3">
        <img src="/images/shape3.png" alt="image" />
      </div>
      <div className="shape4">
        <img src="/images/shape4.png" alt="image" />
      </div>
    </div>
  );
};

export default Testimonials2;
