import React from 'react'; 

const FunFacts = () => {
    return (
        <div className="funfacts-area bg-f5f7fa">
            <div className="container">
                <div className="row">
                    <div className="col-lg-3 col-md-3 col-sm-6">
                        <div className="single-funfacts-item">
                            <h3>3500+</h3>
                            <p>Satisfied Students</p>
                        </div>
                    </div>

                    <div className="col-lg-3 col-md-3 col-sm-6">
                        <div className="single-funfacts-item">
                            <h3>₹785000200+</h3>
                            <p>Student Loan Disburssed</p>
                        </div>
                    </div>

                    <div className="col-lg-3 col-md-3 col-sm-6">
                        <div className="single-funfacts-item">
                            <h3>15+</h3>
                            <p>Lending Partners</p>
                        </div>
                    </div>

                    <div className="col-lg-3 col-md-3 col-sm-6">
                        <div className="single-funfacts-item">
                            <h3>100%</h3>
                            <p>Satisfaction Rate</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FunFacts;