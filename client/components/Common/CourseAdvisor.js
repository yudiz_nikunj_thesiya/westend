import React from "react";
import dynamic from "next/dynamic";
const OwlCarousel = dynamic(import("react-owl-carousel3"));

const options = {
  loop: true,
  nav: false,
  dots: true,
  autoplayHoverPause: true,
  autoplay: true,
  margin: 30,
  navText: [
    "<i class='bx bx-chevron-left'></i>",
    "<i class='bx bx-chevron-right'></i>",
  ],
  responsive: {
    0: {
      items: 1,
    },
    576: {
      items: 2,
    },
    768: {
      items: 1,
    },
    992: {
      items: 2,
    },
  },
};

const CourseAdvisor = () => {
  const [display, setDisplay] = React.useState(false);

  React.useEffect(() => {
    setDisplay(true);
  }, []);
  return (
    <div className="advisor-area bg-f9f9f9 ptb-100">
      <div className="container">
        <div className="section-title">
          <span className="sub-title">Westend</span>
          <h2>Our Team of Experts</h2>
        </div>

        {display ? (
          <OwlCarousel
            className="advisor-slides owl-carousel owl-theme"
            {...options}
          >
            <div className="single-advisor-box">
              <div className="row align-items-center">
                <div className="col-lg-4 col-md-4">
                  <div className="advisor-image">
                    <img src="/images/team/Rishi.jpg" alt="image" />
                  </div>
                </div>

                <div className="col-lg-8 col-md-8">
                  <div className="advisor-content">
                    <h3>
                      <a href="profile.html">Rasik Prajapati</a>
                    </h3>
                    <span className="sub-title">Founder & Managing Director</span>
                    <p>
                      The brain behind Westend, with more than 12 years of experience into this domain, Mr. Prajapati is a name to reckon within the finance education industry. He has a great guidance skills in the educational loan support and disbursal industry. Mr. Rasik holds a Master Degree in Finance from University of East London.
                    </p>

                    <ul className="social-link">
                      <li>
                        <a href="https://www.linkedin.com/in/rasik-prajapati-79b02b79" className="d-block" target="_blank">
                          <img src="/images/linkedin-full.png" alt="image" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <div className="single-advisor-box">
              <div className="row align-items-center">
                <div className="col-lg-4 col-md-4">
                  <div className="advisor-image">
                    <img src="/images/team/shailesh.jpg" alt="image" />
                  </div>
                </div>

                <div className="col-lg-8 col-md-8">
                  <div className="advisor-content">
                    <h3>
                      <a href="profile.html">Shailesh Prajapati</a>
                    </h3>
                    <span className="sub-title">Manager</span>
                    <p>
                      Trustworthiness merged with more than 10 years of experience, Shailesh brings in the management outcomes of Westend Consultancy. Shailesh brings the covetted experience and insightful guidance from the banking sector. Mr. Shailesh holds a BBA degree from the University of East London.
                    </p>
                    <ul className="social-link">
                    <li>
                        <a href="https://www.linkedin.com/in/shailesh-prajapati-698ab420a" className="d-block" target="_blank">
                          <img src="/images/linkedin-full.png" alt="image" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <div className="single-advisor-box">
              <div className="row align-items-center">
                <div className="col-lg-4 col-md-4">
                  <div className="advisor-image">
                    <img src="/images/team/dharti.jpg" alt="image" />
                  </div>
                </div>

                <div className="col-lg-8 col-md-8">
                  <div className="advisor-content">
                    <h3>
                      <a href="profile.html">Dharti Rajput</a>
                    </h3>
                    <span className="sub-title">Business Development Manager</span>
                    <p>
                      Dharti is passionate about building strong relationships our banking partners and operation, Data researches analytics to generate models for various products using the Westend platform. Dharti holds a Bachelor Degree in IT from GTU and she has more than 4 years of experience in the Education Industry.
                    </p>
                    <ul className="social-link">
                      <li>
                        <a href="https://www.linkedin.com/in/dharti-rajpoot-4ab200150" className="d-block" target="_blank">
                          <img src="/images/linkedin-full.png" alt="image" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <div className="single-advisor-box">
              <div className="row align-items-center">
                <div className="col-lg-4 col-md-4">
                  <div className="advisor-image">
                    <img src="/images/team/nikita.jpg" alt="image" />
                  </div>
                </div>

                <div className="col-lg-8 col-md-8">
                  <div className="advisor-content">
                    <h3>
                      <a href="profile.html">Nikita Desai</a>
                    </h3>
                    <span className="sub-title">Senior Loan Officer</span>
                    <p>
                    Nikita has deep knowledge of variegated Banks education loan process and She engages with banks and students until the final stage to ensure timely loan approvals makes Nikita one of the busiest person. She has close to 4 years of experience in the Education domain. She has done her B.COM from H.K College, Ahmedabad.
                    </p>
                    <ul className="social-link">
                    <li>
                        <a href="https://www.linkedin.com/in/nikita-desai-a5660517b" className="d-block" target="_blank">
                          <img src="/images/linkedin-full.png" alt="image" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <div className="single-advisor-box">
              <div className="row align-items-center">
                <div className="col-lg-4 col-md-4">
                  <div className="advisor-image">
                    <img src="/images/team/krishna.jpg" alt="image" />
                  </div>
                </div>

                <div className="col-lg-8 col-md-8">
                  <div className="advisor-content">
                    <h3>
                      <a href="profile.html">Krishna Dhakan</a>
                    </h3>
                    <span className="sub-title">Loan Officer</span>
                    <p>
                      Krishna is a part of the communications and Branding team at Westend. She helps students to understand the whole loan process and leads the Social Media efforts at Westend. Krishan has close to 5 years of experience in Banking and Marketing. She has done her M.Com in Marketing from M.S University, Vadodara.
                    </p>
                    <ul className="social-link">
                    <li>
                        <a href="https://www.linkedin.com/in/krishna-dhakan-a5188919b" className="d-block" target="_blank">
                          <img src="/images/linkedin-full.png" alt="image" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            {/* <div className="single-advisor-box">
              <div className="row align-items-center">
                <div className="col-lg-4 col-md-4">
                  <div className="advisor-image">
                    <img src="/images/team/kirti.jpg" alt="image" />
                  </div>
                </div>

                <div className="col-lg-8 col-md-8">
                  <div className="advisor-content">
                    <h3>
                      <a href="profile.html">Kirti Parmar</a>
                    </h3>
                    <span className="sub-title">Banking Operations</span>
                    <p>
                      Banking and financial expertise is the strength of Westend Consultancy. With more than 5 years of experience, Kirti brings in insightful guidance from the banking sector.
                    </p>
                    <ul className="social-link">
                    <li>
                        <a href="https://www.linkedin.com/in/kirti-parmar-5b5244206" className="d-block" target="_blank">
                          <img src="/images/linkedin-full.png" alt="image" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div> */}
          </OwlCarousel>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};

export default CourseAdvisor;
