import React from "react"
import Link from "next/link"
import Modal from "../Modals/index";
import RequestCalllback from "../../pages/request-callback";

const MainBanner = () => {
  const [modal, setModal] = React.useState(false);
  const toggle = () => setModal(!modal);

  return (
    <>
    <Modal
        size="xl"

        modal={modal}
        toggle={toggle}
        content={
          <RequestCalllback
          toggle={toggle}
         
          />
        }
      />
    <div className="main-banner-area">
      <div className="container-fluid">
        <div className="row align-items-center">
          <div className="col-lg-5 col-md-12">
            <div className="main-banner-content-style-two">
              <h1>Funding Hurdles, </h1>
              <h1>End with WestEnd !!</h1>
              <p>
              End all funding hurdles and fuel your higher education dream. Get the tailor made support in 1-click.
              </p>
              <Link href="#">
                <a className="default-btn" onClick={()=>toggle()}>
                  <i className="flaticon-calendar"></i>Request Callback<span></span>
                </a>
              </Link>
              <Link href="/education-loan">
                <a className="default-btn ml-2">
                  <i className="flaticon-arrow"></i>Know More<span></span>
                </a>
              </Link>
            </div>
          </div>
          <div className="col-lg-7 col-md-12">
            <div className="main-banner-image-style-two">
              <img src="/images/home2.png" alt="image" />
            </div>
          </div>
        </div>
      </div>
      <div className="banner-shape13">
        <img src="/images/banner-shape2.png" alt="image" />
      </div>
      <div className="banner-shape1">
        <img src="/images/banner-shape1.png" alt="image" />
      </div>
      <div className="banner-shape2">
        <img src="/images/banner-shape2.png" alt="image" />
      </div>
      <div className="banner-shape3">
        <img src="/images/banner-shape3.png" alt="image" />
      </div>
      <div className="shape4">
          <img src="/images/shape4.png" alt="image" />
      </div>
    </div>
    </>
  )
}

export default MainBanner
