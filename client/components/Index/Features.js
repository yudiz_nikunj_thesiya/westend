import React from "react";
import Link from "next/link";

const Features = () => {
  return (
    <div className="features-area ptb-70 ">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 text-left">
            <h2 className="pb-10 ">
              <b>Services</b>
            </h2>
          </div>
        </div>
        <div className="row justify-content-center">
          <div className="col-lg-4 col-sm-6 col-md-6">
            <div className="features-box">
              <div className="icon">
                <i className="flaticon-distance-learning"></i>
              </div>
              <h3>Student Loans</h3>
              <p>
                Our Educational loan service helps to make you financially able
                to fulfill your dreams of higher studies abroad.{" "}
              </p>
              <Link href="/education-loan">
                <a className="link-btn">Check Now!</a>
              </Link>
              <div className="back-icon">
                <i className="flaticon-distance-learning"></i>
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-sm-6 col-md-6">
            <div className="features-box">
              <div className="icon">
                <i className="flaticon-credit-card-1"></i>
              </div>
              <h3>Forex Remittances</h3>
              <p>
                Get the best exchange rates for your cross-border investment
                related payments only on WESTEND{" "}
              </p>
              <Link href="/forex-remittance">
                <a className="link-btn">Check Now!</a>
              </Link>
              <div className="back-icon">
                <i className="flaticon-credit-card-1"></i>
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-sm-6 col-md-6">
            <div className="features-box">
              <div className="iconpng">
                <img src="/images/accomodation.png" alt="image" />
              </div>
              <h3>Student Accomodation</h3>
              <p>
                Westend provides a personalized guidance, ensuring quality
                properties and hassle-free accommodations.{" "}
              </p>
              <Link href="/student-accommodation">
                <a className="link-btn">Check now !</a>
              </Link>
              <div className="back-iconpng">
                <img src="/images/accomodation.png" alt="image" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Features;
