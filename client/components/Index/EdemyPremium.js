import React from "react";
import Link from "next/link";
import WhyWestend from "./WhyWestend";
import Router from "next/router";

const EdemyPremium = () => {
  const [display, setDisplay] = React.useState(false);
  const [activeTab, setActiveTab] = React.useState(1);
  const [activeIndex, setactiveIndex] = React.useState(1);

  React.useEffect(() => {
    if (Router.pathname == "/") {
      var i = 0;
      var myInterval = setInterval(() => {
        var elements = document.querySelectorAll(".single-funfacts-box");
        let temp = elements.length;
        if (temp > 0) {
          if (i < temp) {
            elements[i].classList.add("shows");
            if (i > 0) {
              elements[i - 1].classList.remove("shows");
            }
          }

          i++;
          setActiveTab(i);

          if (i > 5) {
            setTimeout(() => {
              if (temp == 6) {
                temp = 5;
              }
              elements[temp].classList.remove("shows");
            }, 2000);

            i = 0;
          }
        }
      }, 3000);
    } else {
      return;
    }
  }, []);
  const toggle = async (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
    setactiveIndex(tab);
  };
  function Page() {
    if (activeTab == 1) {
      return (
        <div className="single-feedback-item">
          <p>
            Our Loan officer lets you ensure that all the necessary loan
            documents, financial reports and collateral documents exist, and
            that they are up to date. Use it to handle borrower details & check
            current status of your loan.
          </p>
        </div>
      );
    } else if (activeTab == 2) {
      return (
        <div className="single-feedback-item">
          <p>
            Westend guarantees to find the best lender for you, we have a
            strategic partnership with top National and International lenders.
            Students have a variety of sources to choose the most affordable
            education loan, no matter which part of the world you are headed to.
          </p>
        </div>
      );
    } else if (activeTab == 3) {
      return (
        <div className="single-feedback-item">
          <p>
            We provide detailed information on several student loan rates
            against categories like Bank, Interest Rate, Processing Fee, Maximum
            Loan Amount. Get the best interest rates for your university/country
            so that you can compare and choose right.
          </p>
        </div>
      );
    } else if (activeTab == 4) {
      return (
        <div className="single-feedback-item">
          <p>
            We support 24*7. Student loan counselling is a great way for
            borrowers to take advantage of our expert assistance and tailor made
            solutions and achieve financial freedom in the most effective way.
          </p>
        </div>
      );
    } else if (activeTab == 5) {
      return (
        <div className="single-feedback-item">
          <p>
            The no collateral lending model relies on the borrower’s and the
            cosigner’s future income and creditworthiness, respectively. Now
            fulfill your dreams by getting an education loan without pledging
            any security for up to Rs 35 lakhs.
          </p>
        </div>
      );
    } else if (activeTab == 6) {
      return (
        <div className="single-feedback-item">
          <p>
            Student loan counselling is a great way for borrowers to take
            advantage of our expert assistance and tailor made solutions and
            achieve financial freedom in the most effective way.
          </p>
        </div>
      );
    }
  }
  return (
    <div>
      <div className="view-all-courses-area-three bg-f9fbff">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-6 col-md-12">
              <div className="feedback-content">
                <span className="sub-title">Your Dream Partner</span>
                <h2>Why Choose Westend ?</h2>
                <br></br>
                <br></br>
                <div className="td-text-area">{Page()}</div>
              </div>
            </div>
            <div className="col-lg-6 col-md-12">
              <WhyWestend toggle={toggle} isActive={activeIndex} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EdemyPremium;
