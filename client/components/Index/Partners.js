import React from "react";
import Link from "next/link";

const Features = () => {
  return (
    <div className="features-area  ptb-70">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <h2 className="pb-10">
              <b>Our Lending Partners</b>
            </h2>
          </div>
        </div>
        <div className="row justify-content-center">
          <div className="col-lg-3 col-sm-6 col-md-6">
            <Link href="#">
              <div className="partners-box">
                <img src="/images/banks/axis.jpg" alt="image" />
              </div>
            </Link>
          </div>
          <div className="col-lg-3 col-sm-6 col-md-6">
            <Link href="#">
              <div className="partners-box">
                <img src="/images/banks/avanse.jpg" alt="image" />
              </div>
            </Link>
          </div>
          <div className="col-lg-3 col-sm-6 col-md-6">
            <Link href="#">
              <div className="partners-box">
                <img src="/images/banks/auxilo.png" alt="image" />
              </div>
            </Link>
          </div>
          <div className="col-lg-3 col-sm-6 col-md-6">
            <Link href="#">
              <div className="partners-box">
                <img src="/images/banks/BOI.jpg" alt="image" />
              </div>
            </Link>
          </div>
        </div>

        <div className="row justify-content-center">
          <div className="col-lg-3 col-sm-6 col-md-6">
            <Link href="#">
              <div className="partners-box">
                <img src="/images/banks/Canara.jpg" alt="image" />
              </div>
            </Link>
          </div>

          <div className="col-lg-3 col-sm-6 col-md-6">
            <Link href="#">
              <div className="partners-box">
                <img src="/images/banks/icici_bank.jpg" />
              </div>
            </Link>
          </div>

          <div className="col-lg-3 col-sm-6 col-md-6">
            <Link href="#">
              <div className="partners-box">
                <img src="/images/banks/cridelo.png" alt="image" />
              </div>
            </Link>
          </div>
          <div className="col-lg-3 col-sm-6 col-md-6">
            <Link href="#">
              <div className="partners-box">
                <img src="/images/banks/mPower.png" alt="image" />
              </div>
            </Link>
          </div>
        </div>
        <div className="row ">
          <div className="col-lg-3 col-sm-6 col-md-6">
            <Link href="#">
              <div className="partners-box">
                <img src="/images/banks/sbi.jpg" alt="image" />
              </div>
            </Link>
          </div>
          <div className="col-lg-3 col-sm-6 col-md-6">
            <Link href="#">
              <div className="partners-box">
                <img src="/images/banks/prodigy.jpg" alt="image" />
              </div>
            </Link>
          </div>
          <div className="col-lg-3 col-sm-6 col-md-6">
            <Link href="#">
              <div className="partners-box">
                <img src="/images/banks/incred.png" alt="image" />
              </div>
            </Link>
          </div>
          <div className="col-lg-3 col-sm-6 col-md-6">
            <Link href="#">
              <div className="partners-box">
                <img src="/images/banks/leap-finance.png" alt="image" />
              </div>
            </Link>
          </div>
        </div>

        <div className="row ">
          <div className="col-lg-3 col-sm-6 col-md-6">
            <Link href="#">
              <div className="partners-box">
                <img src="/images/banks/IDBI NEW.png" alt="image" />
              </div>
            </Link>
          </div>
          <div className="col-lg-3 col-sm-6 col-md-6">
            <Link href="#">
              <div className="partners-box">
                <img src="/images/banks/HDFC.png" alt="image" />
              </div>
            </Link>
          </div>
          <div className="col-lg-3 col-sm-6 col-md-6">
            <Link href="#">
              <div className="partners-box">
                <img src="/images/banks/RBL.png" alt="image" />
              </div>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Features;
