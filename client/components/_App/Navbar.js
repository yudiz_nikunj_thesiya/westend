import React from "react";
import Link from "@/utils/ActiveLink";
import { handleLogout } from "@/utils/auth";
import Modal from "../Modals/index";
import SuccessMessage from "../../pages/successMessage";
import ForgotPassword from "../../pages/forgot-password";
import VerifyOtpLogin from "../../pages/verifyOtp";
import Login from "../../pages/auth/login";
import OtpLogin from "../../pages/otp-login";
import { signout } from "next-auth/client";

import Partner from "../../pages/becomePartner";
import { Store } from "../../Context";
import cookie from "js-cookie";
import Router from "next/router";
import {
  faPhoneAlt,
  faHome,
  faCheckCircle,
} from "@fortawesome/free-solid-svg-icons";
import { useSession } from "next-auth/client";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
const Navbar = ({ user }) => {
  const [session, loading] = useSession();

  let cooky = cookie.get("user");
  let loggedInUser = cooky && JSON.parse(cooky);
  const [tokenState, settokenState] = React.useState("");
  const [userState, setuserState] = React.useState("");

  const [menu, setMenu] = React.useState(true);
  const [modal, setModal] = React.useState(false);
  const [partnerModal, setpartnerModal] = React.useState(false);
  const [successmodal, setsuccessModal] = React.useState(false);
  const [forgotModal, setforgotModal] = React.useState(false);
  const [otpModal, setotpModal] = React.useState(false);
  const [verifyotpModal, setverifyotpModal] = React.useState(false);
  const onSessionLogout = () => {
    signout();
    cookie.remove("token");
    cookie.remove("user");
    cookie.remove("path");

    Router.push("/");
  };
  const toggle = () => setModal(!modal);
  const otpToggle = () => {
    setotpModal(!otpModal);
    setModal(false);
  };
  const verifyotpToggle = () => {
    setverifyotpModal(!verifyotpModal);
    setotpModal(false);
  };

  const successToggle = () => {
    setsuccessModal(!successmodal);
  };
  const Partnertoggle = () => setpartnerModal(!partnerModal);

  const toggleNavbar = () => {
    setMenu(!menu);
  };

  const forgotToggle = () => {
    setforgotModal(!forgotModal);
    setModal(false);
  };

  React.useEffect(() => {
    let elementId = document.getElementById("navbar");
    document.addEventListener("scroll", () => {
      if (window.scrollY > 170) {
        elementId.classList.add("is-sticky");
      } else {
        elementId.classList.remove("is-sticky");
      }
    });
    window.scrollTo(0, 0);
  });
  const GoDashboard = () => {
    let token = tokenState;
    let user = userState;

    cookie.set("token", token);
    cookie.set("user", JSON.stringify(user));
    cookie.set("path", path);

    let path;
    if (user.role === "student") {
      path = "/student/dashboard";
    } else if (user.role === "admin") {
      path = "/admin/dashboard";
    } else {
      path = "/partner/dashboard";
    }

    successToggle();
    Router.push(path);
  };
  const isAdmin = loggedInUser && loggedInUser.role === "admin";
  const isStudent = loggedInUser && loggedInUser.role === "student";
  const isPartner = loggedInUser && loggedInUser.role === "partner";
  const classOne = menu
    ? "collapse navbar-collapse"
    : "collapse navbar-collapse show";
  const classTwo = menu
    ? "navbar-toggler navbar-toggler-right collapsed"
    : "navbar-toggler navbar-toggler-right";

  return (
    <React.Fragment>
      <Modal
        size="md"
        modal={otpModal}
        toggle={otpToggle}
        content={
          <OtpLogin toggle={otpToggle} verifyotpToggle={verifyotpToggle} />
        }
      />
      <Modal
        size="md"
        modal={verifyotpModal}
        toggle={verifyotpToggle}
        content={
          <VerifyOtpLogin
            toggle={verifyotpToggle}
            successToggle={successToggle}
            setuserState={setuserState}
            settokenState={settokenState}
            forgotToggle={forgotToggle}
            otpToggle={otpToggle}
          />
        }
      />
      <Modal
        modal={modal}
        toggle={toggle}
        content={
          <Login
            loginToggle={toggle}
            toggle={otpToggle}
            verifyotpToggle={verifyotpToggle}
            successToggle={successToggle}
            setuserState={setuserState}
            settokenState={settokenState}
            forgotToggle={forgotToggle}
          />
        }
      />
      <Modal
        size="xl"
        modal={partnerModal}
        toggle={Partnertoggle}
        content={
          <Partner
            loginToggle={Partnertoggle}
            successToggle={successToggle}
            setuserState={setuserState}
            settokenState={settokenState}
          />
        }
      />
      <Modal
        size="md"
        modal={forgotModal}
        toggle={forgotToggle}
        content={<ForgotPassword forgotToggle={forgotToggle} />}
      />
      <Modal
        size="lg"
        modal={successmodal}
        toggle={successToggle}
        content={
          <SuccessMessage
            toggle={successToggle}
            onClick={GoDashboard}
            icon={faCheckCircle}
            title="Welcome to Westend !” You are just one step away from your dream
          education. Please complete your registration."
            buttonText="Go To Dashboard"
          />
        }
      />
      <div id="navbar" className="navbar-area">
        <div className="edemy-nav">
          <div className="container-fluid">
            <div className="navbar navbar-expand-lg navbar-light">
              <Link href="/">
                <a onClick={toggleNavbar} className="navbar-brand">
                  <img src="/images/mainLogo.png" alt="logo" />
                </a>
              </Link>

              <button
                onClick={toggleNavbar}
                className={classTwo}
                type="button"
                data-toggle="collapse"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="icon-bar top-bar"></span>
                <span className="icon-bar middle-bar"></span>
                <span className="icon-bar bottom-bar"></span>
              </button>

              <div className={classOne} id="navbarSupportedContent">
                {loggedInUser ? (
                  <ul className="navbar-nav loggedIn">
                    <li className="nav-item">
                      <Link href="/" activeClassName="active">
                        <a className="nav-link ">
                          <FontAwesomeIcon icon={faHome} /> Home
                        </a>
                      </Link>
                    </li>
                    {loggedInUser && loggedInUser.role === "partner" ? (
                      <li className="nav-item">
                        <Link
                          href={`/partner/${loggedInUser.verifyPartnerToken}&&partner=${loggedInUser.company_name}`}
                          activeClassName="active"
                        >
                          <a className="nav-link">UTM Link</a>
                        </Link>
                      </li>
                    ) : (
                      ""
                    )}
                    {loggedInUser && loggedInUser.role === "student" ? (
                      <li className="nav-item">
                        <Link href="/sop" activeClassName="active">
                          <a className="nav-link ">SOP Review</a>
                        </Link>
                      </li>
                    ) : (
                      ""
                    )}
                    {loggedInUser && loggedInUser.role === "student" ? (
                      <li className="nav-item">
                        <Link
                          href="/apply-scholarship"
                          activeClassName="active"
                        >
                          <a className="nav-link ">Apply for Scholarship</a>
                        </Link>
                      </li>
                    ) : (
                      ""
                    )}
                    <li className="nav-item">
                      <Link href="tel:+919023726795">
                        <a className="nav-link">
                          <FontAwesomeIcon icon={faPhoneAlt} /> +919023726795
                        </a>
                      </Link>
                    </li>
                  </ul>
                ) : (
                  <ul className="navbar-nav">
                    <li className="nav-item">
                      <Link href="/" activeClassName="active">
                        <a className="nav-link">Home</a>
                      </Link>
                    </li>

                    <li className="nav-item">
                      <Link href="/">
                        <a
                          // onClick={(e) => e.preventDefault()}
                          className="nav-link"
                        >
                          Tools <i className="bx bx-chevron-down"></i>
                        </a>
                      </Link>

                      <ul className="dropdown-menu">
                        <li className="nav-item">
                          <Link href="/checkEligibility">
                            <a className="nav-link">Check Eligibility</a>
                          </Link>
                        </li>
                        <li className="nav-item">
                          <Link href="/emi-calculator">
                            <a className="nav-link">EMI Calculator</a>
                          </Link>
                        </li>
                        {/* <li className="nav-item">
                          <Link href="repaymentCalculator">
                            <a
                              className="nav-link"
                            >
                              Repayment Calculator
                            </a>
                          </Link>
                        </li> */}
                        <li className="nav-item">
                          <Link href="/sop" activeClassName="active">
                            <a className="nav-link ">SOP Review</a>
                          </Link>
                        </li>
                        <li className="nav-item">
                          <Link
                            href="/apply-scholarship"
                            activeClassName="active"
                          >
                            <a className="nav-link ">Apply for Scholarship</a>
                          </Link>
                        </li>
                      </ul>
                    </li>
                    <li className="nav-item">
                      <Link href="/about-2" activeClassName="active">
                        <a onClick={toggleNavbar} className="nav-link">
                          About Us
                        </a>
                      </Link>
                    </li>
                    <li className="nav-item">
                      <Link href="/contact" activeClassName="active">
                        <a onClick={toggleNavbar} className="nav-link">
                          Contact Us
                        </a>
                      </Link>
                    </li>
                  </ul>
                )}
                <div className="others-option d-flex align-items-center">
                  <div className="option-item">
                    {loggedInUser ? (
                      <div className="user-dropdown">
                        <Link href="/">
                          <a
                            onClick={(e) => e.preventDefault()}
                            className="default-btn text-capitalize"
                          >
                            <i className="flaticon-user"></i>
                            {loggedInUser.firstName} <span></span>
                          </a>
                        </Link>

                        <ul className="dropdown-menu">
                          <li className="nav-item">
                            <Link
                              href={`/${loggedInUser.role}/dashboard`}
                              activeClassName="active"
                            >
                              <a onClick={toggleNavbar} className="nav-link">
                                Dashboard
                              </a>
                            </Link>
                          </li>

                          <li className="nav-item">
                            <Link href="/">
                              <a
                                className="nav-link"
                                onClick={(e) => {
                                  e.preventDefault();
                                  if (!session) {
                                    handleLogout();
                                  } else {
                                    onSessionLogout();
                                  }
                                }}
                              >
                                Logout
                              </a>
                            </Link>
                          </li>
                        </ul>
                      </div>
                    ) : (
                      <>
                        <Link href="/">
                          <a className="no-icon default-btn" onClick={toggle}>
                            Student Login <span></span>
                          </a>
                        </Link>
                        <Link href="/">
                          <a
                            className="default-btn2 ml-4"
                            onClick={Partnertoggle}
                          >
                            {/* <FontAwesomeIcon
                              className="icon "
                              icon={faHandshake}
                            /> */}
                            
                            Partner <span></span>
                          </a>
                        </Link>
                      </>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Navbar;
