import React from "react";
import Head from "next/head";
import { ToastProvider } from "react-toast-notifications";
import { Toaster } from "react-hot-toast";
import Router from "next/router";
import GoTop from "./GoTop";
import Navbar from "./Navbar";
import Footer from "./Footer";
import Preloader from "./Preloader";

const Layout = (props) => {
  const { children, user } = props;
  const [loader, setLoader] = React.useState(true);
  React.useEffect(() => {
    setTimeout(() => {
      setLoader(false);
    }, 1000);
  }, []);

  Router.events.on("routeChangeStart", () => {
    setLoader(true);
  });
  Router.events.on("routeChangeComplete", () => {
    setLoader(false);
  });
  Router.events.on("routeChangeError", () => {
    setLoader(false);
  });
  return (
    <React.Fragment>
      <Head>
        <title>Westend Educorp</title>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <meta
          name="description"
          content="Westend Educorp - We fund your study abroad dreams"
        />
        <meta
          name="og:title"
          property="og:title"
          content="Westend Educorp - We fund your study abroad dreams"
        ></meta>
        <meta
          name="twitter:card"
          content="Westend Educorp - We fund your study abroad dreams"
        ></meta>
        <link rel="canonical" href="https://westend.co.in"></link>
      </Head>

      {loader && <Preloader />}

      <Toaster position="top-left" reverseOrder={false} />

      <ToastProvider
        placement="bottom-left"
        autoDismissTimeout={10000}
        autoDismiss
      >
        <Navbar user={user} />

        {children}

        <GoTop scrollStepInPx="100" delayInMs="10.50" />
        <div className="whatsappIcon">
          <a
            href="https://api.whatsapp.com/send?phone=+919023726795"
            target="_blank"
          >
            <img src="/icons/chat-with-us.png" alt="image" />
          </a>
        </div>
        <Footer />
      </ToastProvider>
    </React.Fragment>
  );
};

export default Layout;
