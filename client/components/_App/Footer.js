import React from "react"
import Link from "next/link"

const Footer = () => {
  const currentYear = new Date().getFullYear()

  return (
    <footer className="footer-area">
      <div className="container">
        <div className="row">
          <div className="col-lg-4 col-md-6 col-sm-6">
            <div className="single-footer-widget">
              <Link href="/">
                {/* <a className="logo">
                  <img src="/images/mainLogo.png" alt="logo" />
                </a> */}
                <h2 className="text-white pb-2">Westend</h2>
              </Link>

              <p>
              For any kind of educational loan query or clarification related to your study abroad, 
              don’t miss on the right information and direction. 
              There are multiple ways to connect to us.
              </p>

              <ul className="social-link">
                <li>
                  <a href="https://www.facebook.com/westend.educorp/" className="d-block" target="_blank">
                    <i className="bx bxl-facebook"></i>
                  </a>
                </li>
                <li>
                  <a href="https://twitter.com/westend_educorp" className="d-block" target="_blank">
                    <i className="bx bxl-twitter"></i>
                  </a>
                </li>
                <li>
                  <a href="https://www.instagram.com/westend_educorp/" className="d-block" target="_blank">
                    <i className="bx bxl-instagram"></i>
                  </a>
                </li>
                <li>
                  <a href="https://www.linkedin.com/company/westend-educorp/" className="d-block" target="_blank">
                    <i className="bx bxl-linkedin"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>

          <div className="col-lg-2 col-md-6 col-sm-6">
            <div className="single-footer-widget pl-5">
              <h3>Explore</h3>
              <ul className="footer-links-list">
                <li>
                  <Link href="/">
                    <a>Home</a>
                  </Link>
                </li>
                <li>
                  <Link href="/about-2">
                    <a>About</a>
                  </Link>
                </li>
                <li>
                  <Link href="/contact">
                    <a>Contact</a>
                  </Link>
                </li>
              </ul>
            </div>
          </div>

          <div className="col-lg-2 col-md-6 col-sm-6">
            <div className="single-footer-widget">
              <h3>Resources</h3>
              <ul className="footer-links-list">
                <li>
                  <Link href="/emi-calculator">
                    <a>EMI Calculator</a>
                  </Link>
                </li>
                <li>
                  <Link href="/checkEligibility">
                    <a>Check Eligibility</a>
                  </Link>
                </li>
                <li>
                  <Link href="/sop">
                    <a>SOP Review</a>
                  </Link>
                </li>
                <li>
                  <Link href="/apply-scholarship">
                    <a>Scholarship</a>
                  </Link>
                </li>
              </ul>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 col-sm-6">
            <div className="single-footer-widget">
              <h3>Address</h3>
              <ul className="footer-contact-info">
                <li>
                  <i className="bx bx-map"></i>
                  <Link href="https://g.page/westend-educorp-pvt-ltd?share">
                    <a>
                  Westend Educorp Pvt Ltd <br></br>1005, B- Square -2, Near to Hotel
                  Double Tree by hilton, Iscon-Ambali Road, Ahmedabad-380054
                  </a>
                  </Link>
                </li>
                <li>
                  <i className="bx bx-phone-call"></i>
                  <a href="tel:+919023726795">+91 90237 26795</a>
                  <ul>
                  <i className="bx bx-phone-call"></i>
                  <a href="tel:+919313924262">+91 93139 24262</a>
                  </ul>
                </li>
                <li>
                  <i className="bx bx-envelope"></i>
                  <a href="mailto:inquiry@westend.co.in">inquiry@westend.co.in</a>
                </li>
                
              </ul>
            </div>
          </div>
        </div>

        <div className="footer-bottom-area">
          <div className="row align-items-center">
            <div className="col-lg-6 col-md-6">
              {/* <p>
                <i className="bx bx-copyright"></i>
                {currentYear} eDemy is Proudly Powered by{" "}
                <a target="_blank" href="https://envytheme.com/">
                  EnvyTheme
                </a>
              </p> */}
            </div>

            <div className="col-lg-6 col-md-6">
              <ul>
                <li>
                  <Link href="/privacy-policy">
                    <a>Privacy Policy</a>
                  </Link>
                </li>
                <li>
                  <Link href="/privacy-policy">
                    <a>Terms & Conditions</a>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div className="lines">
        <div className="line"></div>
        <div className="line"></div>
        <div className="line"></div>
      </div>
    </footer>
  )
}

export default Footer
