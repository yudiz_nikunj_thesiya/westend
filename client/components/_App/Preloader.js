import React from "react";

const Preloader = () => {
  return (
    <>
      <div className="preloader">
        <div className="loader">
          <img
            className="rotating"
            src="/images/favicon-196x196.png"
            alt="logo"
          />
          <div className="dot-stretching"></div>
        </div>
      </div>

      <style jsx>{`
        .preloader {
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          z-index: 999999999999;
          position: fixed;
          background-color: rgba(255, 255, 255, 0.85);
        }

        .preloader .loader {
          top: 50%;
          left: 50%;
          position: absolute;
          -webkit-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
        }

        .dot-stretching {
          top: 100%;
          left: 40%;
          position: absolute;
          width: 15px;
          height: 15px;
          border-radius: 10px;
          background-color: #152f68;
          color: #152f68;
          transform: scale(1.25, 1.25);
          animation: dotStretching 2s infinite ease-in;
        }

        .dot-stretching::before,
        .dot-stretching::after {
          content: "";
          display: inline-block;
          position: absolute;
          top: 0;
        }

        .dot-stretching::before {
          width: 15px;
          height: 15px;
          border-radius: 10px;
          background-color: #152f68;
          color: #152f68;
          animation: dotStretchingBefore 2s infinite ease-in;
        }

        .dot-stretching::after {
          width: 15px;
          height: 15px;
          border-radius: 10px;
          background-color: #152f68;
          color: #152f68;
          animation: dotStretchingAfter 2s infinite ease-in;
        }

        @keyframes dotStretching {
          0% {
            transform: scale(1.25, 1.25);
          }
          50%,
          60% {
            transform: scale(0.8, 0.8);
          }
          100% {
            transform: scale(1.25, 1.25);
          }
        }

        @keyframes dotStretchingBefore {
          0% {
            transform: translate(0) scale(0.7, 0.7);
          }
          50%,
          60% {
            transform: translate(-20px) scale(1, 1);
          }
          100% {
            transform: translate(0) scale(0.7, 0.7);
          }
        }

        @keyframes dotStretchingAfter {
          0% {
            transform: translate(0) scale(0.7, 0.7);
          }
          50%,
          60% {
            transform: translate(20px) scale(1, 1);
          }
          100% {
            transform: translate(0) scale(0.7, 0.7);
          }
        }
      `}</style>
    </>
  );
};

export default Preloader;
