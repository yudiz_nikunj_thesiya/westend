import React from "react";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";

export default function RadioButtonsGroup(props) {
  return (
    <FormControl>
      {/* <FormLabel component="legend">{props.label}</FormLabel> */}
      <RadioGroup
        name={props.name}
        value={props.value}
        onChange={props.onChange}
        row
      >
        {props.options.map((rado, i) => (
          <FormControlLabel
            key={i}
            value={rado.value}
            control={<Radio />}
            label={rado.label}
          />
        ))}
      </RadioGroup>
    </FormControl>
  );
}
