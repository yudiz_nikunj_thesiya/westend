import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
  },
  textField: {
    border:' 0.1px solid rgba(0, 0, 0, 0.23)',
    padding: '12px 7px',
    width: '17rem',
    borderRadius: '4px',
  },
}));

export default function DatePickers(props) {
  const classes = useStyles();

  return (
    <form className={classes.container} noValidate>
      
      <input
        id="date"
        label={props.label}
        type="date"
        name={props.name}
        value={props.value}
        onChange={props.onChange}
        className={classes.textField}
      
      />
    </form>
  );
}
