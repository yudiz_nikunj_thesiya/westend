import React from "react";
import { Table } from "reactstrap";
import { faEdit, faTrash } from "@fortawesome/free-solid-svg-icons";
import FontAwesomeIcon from "../FontAwesomeIcon/index.js";
const Example = (props) => {
  return (
    <Table striped size="small">
      <thead>
        <tr>
          {props.headers &&
            props.headers.map((header, index) => (
              <th align="center" key={index}>
                {header}
              </th>
            ))}
        </tr>
      </thead>
      <tbody>
        {props.rows &&
          props.rows.map((row, i) => (
            <tr key={i}>
              {props.rowKeys ? (
                <>
                  {props.rowKeys.map((cell, index) => (
                    <th key={index} scope="row">
                      {row[cell]}
                    </th>
                  ))}{" "}
                  {props.actions && props.actions == true ? (
                    <tr>
                      <th className="actions">
                        <FontAwesomeIcon
                          icon={faEdit}
                          className="editIcon"
                          onClick={props.handleEdit}
                        />
                      </th>
                      <th className="actions">
                        <FontAwesomeIcon
                          icon={faTrash}
                          className="delIcon"
                          onClick={props.handleDelete}
                        />
                      </th>
                    </tr>
                  ) : (
                    ""
                  )}
                </>
              ) : (
                row.map((cell, index) => (
                  <th key={index} scope="row">
                    {cell}
                  </th>
                ))
              )}
            </tr>
          ))}
      </tbody>
    </Table>
  );
};

export default Example;
