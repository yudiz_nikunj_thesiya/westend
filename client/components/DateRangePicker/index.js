import React, { useState } from "react";
import { DatePicker, Space } from "antd";
import "antd/dist/antd.css";
const { RangePicker } = DatePicker;
export default function SomeReactComponent(props) {
  return (
    <RangePicker
      size="large"
      onCalendarChange={props.onChange}
      format="DD-MM-YYYY"
    />
  );
}
