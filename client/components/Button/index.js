import React from 'react';
import FontAwesomeIcon from "..//FontAwesomeIcon/index.js";

export default function Button(props) {

  if (props.icon) {
    return (
      <button
        onClick={props.onClick}
        className={props.className}
      >
        <FontAwesomeIcon icon={props.icon} className="mr-1" />
        Back<span></span>
      </button>
    )
  } else {
    return (
      <button
        onClick={props.onClick}
        className={props.className}
      >
        {props.label}<span></span>
      </button>
    )
  }
}