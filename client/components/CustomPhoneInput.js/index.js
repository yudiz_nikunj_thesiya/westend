import React from "react";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
export default function index(props) {
  return (
    <PhoneInput
      onlyCountries={["in"]}
      country={"in"}
      className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
      disableDropdown={true}
      value={props.value}
      disableCountryCode={true}
      inputProps={{
        autoFocus: true,
      }}
      onChange={props.onChange}
    />
  );
}
