import React from "react";
import Link from "next/link";
import dynamic from "next/dynamic";
const ModalVideo = dynamic(import("react-modal-video"));

const AboutUsContentTwo = () => {
	const [display, setDisplay] = React.useState(false);

	React.useEffect(() => {
		setDisplay(true);
	}, []);
	// Popup Video
	const [isOpen, setIsOpen] = React.useState(true);
	const openModal = () => {
		setIsOpen(!isOpen);
	};
	return (
		<React.Fragment>
			<div className="about-area-two ptb-100">
				<div className="container">
					<div className="row align-items-center">
						<div className="col-lg-5 col-md-12">
							<div className="about-content-box">
								<span className="sub-title">
									How the Journey Started
								</span>
								<h2>
									The Founder Story
								</h2>
								<p>
								   It began in 2009 when the influx of studying abroad was high but at the same time there was a lot of chaos in the educational loans and foreign study industry. 
								   </p>
								   <p>This mainly stemmed from the lack of funding and bank rules that were stringent. 
								   A student who wanted to study abroad or even work abroad while gaining a professional degree had to go through a painful process of acquiring loans. 
								   </p>
								   <p>Looking into this huge concern that was not being raised with appropriate solutions, 
								   <b> Mr. Rasik Prajapati</b> realised that he was already helping people sort out the problems in his friend and family circle. This made him want to take a step further to reduce this gap by helping people in a thorough and streamlined manner. 
								   </p>
								   <p><b>Westend</b> is the result of this endeavor. With more than 12 years into this domain, Mr Prajapati is a name to reckon within the industry focussed on financial services for overseas education.
								</p>
								<p>
									<strong>
										
									</strong>
								</p>
							</div>
						</div>

						<div className="col-lg-7 col-md-12">
							<div className="about-video-box">
								<div className="image">
									<img
										src="/images/team/about-us.jpg"
										alt="image"
									/>
								</div>

								
								<div className="shape10">
									<img src="/images/shape9.png" alt="image" />
								</div>
							</div>
						</div>
					</div>
				</div>

				<div className="shape3">
					<img src="/images/shape3.png" alt="image" />
				</div>
				<div className="shape4">
					<img src="/images/shape4.png" alt="image" />
				</div>
				<div className="shape2">
					<img src="/images/shape2.png" alt="image" />
				</div>

				{/* If you want to change the video need to update videoID */}
				{display ? (
					<ModalVideo
						channel="youtube"
						isOpen={!isOpen}
						videoId="bk7McNUjWgw"
						onClose={() => setIsOpen(!isOpen)}
					/>
				) : (
					""
				)}
			</div>
		</React.Fragment>
	);
};

export default AboutUsContentTwo;
