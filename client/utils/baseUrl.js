/*comment*/
const baseUrl =
  process.env.NODE_ENV === "production"
    ? "https://my-app-33x58.ondigitalocean.app/api/v1"
    : "http://localhost:5050/api/v1";

export default baseUrl;
