export const DEFAULT_IMAGE =
  "/images/user9.png";

export const decision = [
  {
    label: "Yes",
    value: "Yes",
  },
  {
    label: "No",
    value: "No",
  },
];
export const application_status = [
  {
    label: "Inquired",
  },
  {
    label: "Lead Created",
  },
  {
    label: "Application Started",
  },
  {
    label: "Application Submitted",
  },
  {
    label: "IELTS Pending",
  },
  {
    label: "Connected to bank",
  },
  {
    label: "Documents sent to Bank",
  },
  {
    label: "File logged in",
  },
  {
    label: "File on hold by bank",
  },
  {
    label: "File on hold by student",
  },
  {
    label: "Sanctioned",
  },
  {
    label: "Disbursed",
  },
  {
    label: "Applied for SOP",
  },
  {
    label: "Applied for Scholarship",
  },
  {
    label: "Not Responding",
  },
  {
    label: "Not interested",
  },
  {
    label: "Sanctioned by other bank",
  },
  {
    label: "Country not eligible",
  },
  {
    label: "No Co-signer",
  },
  {
    label: "Backlog issue",
  },
  {
    label: "Incomplete information",
  },
  {
    label: "Offer letter pending",
  },
  {
    label: "Repeat lead",
  },
  {
    label: "No collateral",
  },
  {
    label: "CIBIL issue in co-applicant",
  },
  {
    label: "Document issues",
  },
  {
    label: "Domestic Loan",
  },
  {
    label: "Applied in other bank",
  },
  {
    label: "Declined",
  },
  {
    label: "File under processing",
  },
  {
    label: "Wrong No.",
  },
  {
    label: "Not Doable",
  },
];

export const StatusList = [
  {
    label: "Applied",
  },
  {
    label: "Waiting Decision",
  },
  {
    label: "Received Offer",
  },
  { label: "Rejected" },
  { label: "Not applied yet " },
];
export const Bachelors = [
  {
    label: "Post Graduate",
    value: "PG",
  },
  {
    label: "Under Graduate",
    value: "UG",
  },
];
export const RelationWithCoApplicant = [
  {
    label: "Father",
  },
  {
    label: "Mother",
  },
  {
    label: "Sister",
  },
  {
    label: "Brother",
  },
  {
    label: "Others",
  },
];
export const DocumentList = [
  {
    name: "Student Aadhar Card",
    file: "",
  },
  {
    name: "Student Pan Card",
    file: "",
  },
  {
    name: "Student Offer letter",
    file: "",
  },
  {
    name: "Co-Applicant Aadhar Card ",
    file: "",
  },
  {
    name: "Co-Applicant Pan Card",
    file: "",
  },
  {
    name: "Co-Applicant last 6 month bank statement",
    file: "",
  },
  {
    name: "Co-Applicant last 1 year ITR",
    file: "",
  },
];

export const TestScores = [
  {
    label: "TOEFL",
  },
  {
    label: "GRE",
  },
  {
    label: "SAT",
  },
  {
    label: "GMAT",
  },
  { label: "PTE" },
  { label: "IELTS" },
  { label: "Duo Lingo" },
];
export const currencies = [
  {
    label: "USD($)",
  },
  {
    label: "CAD($)",
  },
  {
    label: "AUS($)",
  },
  {
    label: "NZD($)",
  },
  { label: "EUR(€)" },
  { label: "INR(₹)" },
];

export const universities = [
  {
    label: "University of Oxford ",
  },
  {
    label: " Stanford University ",
  },
  {
    label: " University of Cambridge  ",
  },
  {
    label: " Harvard University ",
  },
  {
    label: " Imperial College London   ",
  },
  {
    label: " University of Chicago   ",
  },
  {
    label: " National University of Singapore (NUS)   ",
  },
  {
    label: " Other  ",
  },
];

export const degrees = [
  {
    label: "Bachelor of Engineering",
  },
  {
    label: "Bachelor of Architecture",
  },
  {
    label: "Bachelor of Science",
  },
  {
    label: "Bachelor of Applied Arts (BAA)",
  },
  {
    label: "Bachelor of Business Administration (BBA)",
  },
  {
    label: "Bachelor of Computer Applications (BCA)",
  },
  {
    label: "Master of Philosophy (M.Phil.)   ",
  },
  {
    label: "Bachelor of Applied Studies (BAS)  ",
  },
  {
    label: "Bachelor of Religious Studies (BRS)  ",
  },
  {
    label: "Other",
  },
];

export const educations = [
  {
    label: "Computer Science",
  },
  {
    label: "Computer Engineering",
  },
  {
    label: "Business Administration",
  },
  {
    label: "Architecture",
  },
  {
    label: "Medical Science",
  },
  {
    label: "Civil Engineering",
  },
  {
    label: "Mechanical Engineering",
  },
  {
    label: "Electronics Engineering",
  },
  {
    label: "Automobile Engineering",
  },
  {
    label: "Mathematics",
  },
  {
    label: "Science",
  },
  {
    label: "Biology",
  },
  {
    label: "Finance",
  },
  {
    label: "Philosophy",
  },
  {
    label: "Nursing",
  },
  {
    label: "Others",
  },
];
export const departments = [
  {
    label: "Diploma",
  },
  {
    label: "BTech",
  },
  {
    label: "BE",
  },
  {
    label: "BCA",
  },
  {
    label: "MS/MSc",
  },
  {
    label: "MEng",
  },
  {
    label: "MBA",
  },
  {
    label: "MD",
  },
  {
    label: "PHD/Doctorate",
  },
  {
    label: "PGDM",
  },
  {
    label: "Medical",
  },
  {
    label: "Vocational",
  },
  {
    label: "MCA",
  },
  {
    label: "Other",
  },
];
export const countries = [
  { code: "AU", label: "Australia", phone: "61", suggested: true },
  { code: "CA", label: "Canada", phone: "1", suggested: true },
  { code: "DE", label: "Germany", phone: "49", suggested: true },
  { code: "NZ", label: "New Zealand", phone: "64" },
  { code: "GB", label: "United Kingdom", phone: "44" },
  { code: "US", label: "United States", phone: "1", suggested: true },
];
//******** */
// Table headings

export const StudentListheadCells = [
  {
    id: "id",
    numeric: true,
    disablePadding: false,
    label: "#Sr",
  },
  {
    id: "firstName",
    numeric: false,
    disablePadding: true,
    label: "Name",
  },
  {
    id: "email",
    numeric: false,
    disablePadding: true,
    label: "Email",
  },
  { id: "phone", numeric: true, disablePadding: false, label: "Phone" },
  {
    id: "partner_name",
    numeric: true,
    disablePadding: false,
    label: "Partner Name",
  },
  {
    id: "app_status",
    numeric: true,
    disablePadding: false,
    label: "Application Status",
  },

  {
    id: "comment",
    numeric: true,
    disablePadding: false,
    label: "Comment",
  },
  {
    id: "date",
    numeric: true,
    disablePadding: false,
    label: "Date/Time",
  },
  {
    id: "actions",
    numeric: true,
    disablePadding: false,
    label: "Actions ",
  },
];

export const PartnerListheadCells = [
  {
    id: "company_name",
    numeric: false,
    disablePadding: true,
    label: "Company Name",
  },
  {
    id: "email",
    numeric: false,
    disablePadding: true,
    label: "Email",
  },
  { id: "phone", numeric: true, disablePadding: false, label: "Phone" },
  {
    id: "firstName",
    numeric: true,
    disablePadding: false,
    label: "Partner Name",
  },
  {
    id: "approved",
    numeric: true,
    disablePadding: false,
    label: "Approved",
  },
  {
    id: "date",
    numeric: true,
    disablePadding: false,
    label: "Date/Time",
  },
  {
    id: "Actions",
    numeric: true,
    disablePadding: true,
    label: "Actions ",
  },
  {
    id: "link",
    numeric: true,
    disablePadding: false,
    label: "Link ",
  },
];

export const ActiveLoansheadCells = [
  {
    id: "name",
    numeric: false,
    disablePadding: true,
    label: "Name",
  },
  // {
  //   id: "email",
  //   numeric: false,
  //   disablePadding: true,
  //   label: "Email",
  // },
  { id: "phone", numeric: true, disablePadding: false, label: "Phone" },
  {
    id: "loan",
    numeric: true,
    disablePadding: false,
    label: "Loan Amount",
  },
  {
    id: "status",
    numeric: true,
    disablePadding: false,
    label: "Status",
  },
  // {
  //   id: "actions",
  //   numeric: true,
  //   disablePadding: false,
  //   label: "Actions ",
  // },
];
// **************************
// *******
