import React, { useEffect } from "react";
import {
    faPhoneAlt,
    faHome,
    faCheckCircle,
} from "@fortawesome/free-solid-svg-icons";
import { useRouter } from "next/router";
import Modal from "../../components/Modals/index";
import baseUrl from "../../utils/baseUrl";
import Axios from "axios";
import Notifier from "../../utils/Notifier";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
export default function successMessage() {
    const router = useRouter();

    useEffect(() => {
        if (router.query && router.query.verify) {
            handleVerify()
        }
    }, [])

    const handleVerify = async () => {
        let _id = router.query.verify;
        try {
            let response = await Axios({
                method: "post",
                url: `${baseUrl}/auth/verifyPartner`,
                data: { passwordToken: _id },
            });

            Notifier(response.data.message, "success");

        } catch (err) {
            Notifier(err.response.data.message, "error");
        }
    }
    const handleRedirect = async () => {

        router.push("/")

    }
    return (
        <>
         <div className="privacy-policy-area ptb-100">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12 text-center ">
                        <h1>
                            <FontAwesomeIcon icon={faCheckCircle} color="green" />
                        </h1>

                        <h2>Your Email is successfully verified</h2>
                        <h5>Welcome to westend!</h5>
                        <h5>Our Business development team will shortly get back to you. We are looking forward to doing business with you.</h5>
                        <button onClick={handleRedirect} className="no-icon default-btn mt-2">
                            Go to Home
                            <span></span>
                        </button>
                    
                    </div>
                </div>
            </div>
            </div>
        </>
    );
}
