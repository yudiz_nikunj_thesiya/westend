import React from 'react';

const EducationLoan = () => {
    return (
        <>
            <div className="about-area-three ptb-70">
                <div className="container">
                    <div className="row ">
                        <div className="col-lg-12 col-md-12 text-center">
                            <h1 className="pb-5">Why do you need an education loan?</h1>
                        </div>
                    </div>
                    <div className="row ">
                        <div className="col-lg-12 col-md-12">

                            <div className="about-content-box">
                                <h3>Don’t let your talent suffer!
                                </h3>
                                <p>
                                    Higher education gets you enhanced learning, practical curriculum, better jobs and above all a secured career. Behind this huge dream, there is a long complex eligibility to meet including competitive exams, required documents and most importantly financial ability. Not all are born with a silver spoon. We, at Westend Educorp believe that any individual must be known for its talent rather than just his/her financial stability. Educational loan is one such factor that helps in making you financially able to fulfill your dreams of higher studies in India and abroad.
                                </p>
                                <h3>
                                    How does Westend work for your Education loan?
                                </h3>
                                <h5>
                                    We truly value your dream of studying!
                                </h5>
                                <p>
                                    Education loans are the wings beneath your dream of higher studies in India as well as foreign education. Knowing its importance, we at Westend allot a team of experts to check your student loan eligibility. We do this on the basis of your educational documents, income proofs and your co-applicant details. By using our 12 years of experience, and thorough research of your case, we will provide you with the estimated loan amount for which you are eligible. All this process will be executed with just a moment of filling our contact form online, without stepping out of your house. This will save your time, money, and other resources too.
                                </p>
                                <p>
                                    Most importantly, we provide guidance without charging any single fee for it. We are the bridge between loan seeking students and our trusted financing partners.
                                </p>
                            </div>
                            <div className="about-content-box">
                                <h3 >Our Concrete Process Framework
                                </h3>
                                <h5 className="pt-2">
                                    We work tirelessly to fulfill your dreams
                                </h5>
                                <div className="blog-details-desc article-content ">
                                    <ul class="features-list">
                                        <li><i class="bx bx-badge-check"></i> Collecting your information personally via telephone media</li>
                                        <li><i class="bx bx-badge-check"></i> Applying to our partner firms</li>
                                        <li><i class="bx bx-badge-check"></i> Get provincial educational loan letter stating estimated loan amount</li>
                                        <li><i class="bx bx-badge-check"></i> By making trivial decisions, we consult you on the right decision best for you.</li>
                                        <li><i class="bx bx-badge-check"></i>Moving onto further process of finalizing the loan</li>
                                        <li><i class="bx bx-badge-check"></i>Get your loan disbursement status</li>


                                    </ul>

                                </div>
                            </div>

                            <div className="about-content-box">
                                <h3>Features and Benefits
                                </h3>
                                <h5 className="pt-2">
                                    Choosing Westend will provide you the following features and benefits:
                                </h5>
                                <div className="blog-details-desc article-content ">
                                    <ul class="features-list">
                                        <li><i class="bx bx-badge-check"></i> Get 100% loan amount including fees and living expense </li>
                                        <li><i class="bx bx-badge-check"></i> Virtual yet transparent loan process</li>
                                        <li><i class="bx bx-badge-check"></i> Get affordable interest rates</li>
                                        <li><i class="bx bx-badge-check"></i> Get 24*7 support and guidance</li>
                                        <li><i class="bx bx-badge-check"></i>Transparent workflow</li>
                                        <li><i class="bx bx-badge-check"></i>Education loans without co-signer or co-laterals</li>
                                        <li><i class="bx bx-badge-check"></i>Pre admission sanction</li>
                                        <li><i class="bx bx-badge-check"></i>Flexibility in loan disbursement</li>



                                    </ul>

                                </div>
                            </div>


                            <div className="about-content-box">
                                <h3>Student Eligibility Criteria
                                </h3>
                                <h5 className="pt-2">
                                    It will be easier for you to avail education loan if you meet the below eligibility criteria:
                                </h5>
                                <div className="blog-details-desc article-content ">
                                    <ul class="features-list">
                                        <li><i class="bx bx-badge-check"></i> Citizen of India </li>
                                        <li><i class="bx bx-badge-check"></i> Minimum 50% marks in HSC</li>
                                        <li><i class="bx bx-badge-check"></i> Admitted to one of the STEM (Science, technology, engineering, Medicine) courses</li>
                                        <li><i class="bx bx-badge-check"></i>Qualified with a good score in any of the Competitive/English proficiency tests like GATE, IELTS, TOEFL, SAT, GRE, etc.</li>
                                        <li><i class="bx bx-badge-check"></i>Regular income proof of your co-applicant (Parents/relatives/friends)</li>




                                    </ul>

                                </div>
                            </div>
                            <div className="about-content-box">
                                <h3>Required Documents
                                </h3>
                                <h5 className="pt-2">
                                    Keep your below documents ready before checking the education loan eligibility:
                                </h5>
                                <div className="blog-details-desc article-content ">
                                    <ul class="features-list">
                                        <li><i class="bx bx-badge-check"></i> Admission/offer letter from institutes</li>
                                        <li><i class="bx bx-badge-check"></i> KYC documents</li>
                                        <li><i class="bx bx-badge-check"></i> Last 6 months bank statement</li>
                                        <li><i class="bx bx-badge-check"></i>Mark sheets – HSC, SSC, Degree</li>
                                        <li><i class="bx bx-badge-check"></i>Guarantor form (optional)</li>




                                    </ul>

                                </div>
                                <p>Reports show that nearly 60% of aspiring students drop their plan of further studies because of the lack of guidance regarding university atmosphere, financial help, post admission process, etc.</p>
                                <p>Westend Educorp won’t let that happen! We have done thorough research, collected plenty of information on how a student can seek his/her bright future without worrying about the hurdles.</p>
                            </div>


                            <div className="features-area  ptb-70 bg-f9fbff">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-lg-12 text-center">
                                            <h2 className="pb-10">
                                                <b>Our Lending Partners</b>
                                            </h2>
                                        </div>
                                    </div>
                                    <div className="row justify-content-center">
                                        <div className="col-lg-3 col-sm-6 col-md-6">
                                            <div className="partners-box">
                                                <img src="/images/banks/axis.jpg" alt="image" />
                                            </div>
                                        </div>
                                        <div className="col-lg-3 col-sm-6 col-md-6">
                                            <div className="partners-box">
                                                <img src="/images/banks/avanse.jpg" alt="image" />
                                            </div>
                                        </div>
                                        <div className="col-lg-3 col-sm-6 col-md-6">
                                            <div className="partners-box">
                                                <img src="/images/banks/auxilo.png" alt="image" />
                                            </div>
                                        </div>
                                        <div className="col-lg-3 col-sm-6 col-md-6">
                                            <div className="partners-box">
                                                <img src="/images/banks/BOI.jpg" alt="image" />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row justify-content-center">
                                        <div className="col-lg-3 col-sm-6 col-md-6">
                                            <div className="partners-box">

                                                <img src="/images/banks/Canara.jpg" alt="image" />

                                            </div>
                                        </div>
                                        <div className="col-lg-3 col-sm-6 col-md-6">
                                            <div className="partners-box">
                                                <img src="/images/banks/icici_bank.jpg" alt="image" />
                                            </div>
                                        </div>
                                        <div className="col-lg-3 col-sm-6 col-md-6">
                                            <div className="partners-box">
                                                <img src="/images/banks/cridelo.png" alt="image" />
                                            </div>
                                        </div>
                                        <div className="col-lg-3 col-sm-6 col-md-6">
                                            <div className="partners-box">
                                                <img src="/images/banks/mPower.png" alt="image" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row ">
                                        <div className="col-lg-3 col-sm-6 col-md-6">
                                            <div className="partners-box">
                                                <img src="/images/banks/sbi.jpg" alt="image" />
                                            </div>
                                        </div>
                                        <div className="col-lg-3 col-sm-6 col-md-6">
                                            <div className="partners-box">
                                                <img src="/images/banks/prodigy.jpg" alt="image" />
                                            </div>
                                        </div>
                                        <div className="col-lg-3 col-sm-6 col-md-6">
                                            <div className="partners-box">
                                                <img src="/images/banks/incred.png" alt="image" />
                                            </div>
                                        </div>
                                        <div className="col-lg-3 col-sm-6 col-md-6">
                                            <div className="partners-box">
                                                <img src="/images/banks/leap-finance.png" alt="image" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>





                        </div>


                    </div>


                </div>
            </div>

        </>
    )
}

export default EducationLoan;