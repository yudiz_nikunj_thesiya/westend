import Document, { Html, Head, Main, NextScript } from "next/document";
const APP_NAME = "Westend";

class MyDocument extends Document {
  render() {
    return (
      <Html lang="zxx">
        <Head>
          <link rel="icon" type="image/png" href="/icons/favicon.png"></link>
          <meta name="application-name" content={APP_NAME} />
          <meta name="apple-mobile-web-app-capable" content="yes" />
          <meta
            name="apple-mobile-web-app-status-bar-style"
            content="default"
          />
          <meta name="apple-mobile-web-app-title" content={APP_NAME} />
          <meta name="theme-color" content="#FFFFFF" />
          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/icons/favicon.png"
          />
          <link rel="manifest" href="/manifest.json" />
          <link rel="shortcut icon" href="/icons/favicon.png" />
          <link
            rel="stylesheet"
            href="https://unpkg.com/tailwindcss@1.3.4/dist/tailwind.min.css"
          />
        {/* Global Site Tag (gtag.js) - Google Analytics */}
          <script
            async
            src={`https://www.googletagmanager.com/gtag/js?id=${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}`}
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}', {
              page_path: window.location.pathname,
            });
          `,}}
          />
        </Head>
        <body>
          <Main />
          <NextScript />
         <script defer type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"/>
         {/* <script defer src="js/main.js" /> */}
        </body>
      </Html>
    );
  }
}

export default MyDocument;
