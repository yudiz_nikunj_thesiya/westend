import React, { useState, useEffect } from "react";
import Sidebar from "./components/sidebar";
import Partners from "./containers/Partners";
import StudentPartners from "../../partner/dashboard/containers/Students";

import Students from "./containers/Students";
import EditProfile from "../../edit-profile";
import ChangePassword from "../../edit-password";
import baseUrl from "../../../utils/baseUrl";
import Axios from "axios";

import StudentSidebar from "../../student/dashboard/components/sidebar/index.js";
import PartnerSidebar from "../../partner/dashboard/components/sidebar/index.js";

import Application from "../../student/dashboard/containers/Application/index.js";
import ActiveLoans from "../../student/dashboard/containers/ActiveLoans/index.js";
import Tools from "../../student/dashboard/containers/Tools/index.js";
import Faq from "../../student/dashboard/containers/Faq/index.js";
const Dashboard = (props) => {
  const [Data, setData] = useState("");
  const [activeTab, setActiveTab] = React.useState(1);
  const [activeIndex, setactiveIndex] = React.useState(1);
  const [studentID, setstudentID] = React.useState(0);
  const [partnerID, setpartnerID] = React.useState(0);

  const [studentTab, setstudentTab] = React.useState(false);
  const [partnerTab, setpartnerTab] = React.useState(false);

  const toggle = async (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
    setactiveIndex(tab);
    if (tab == 1) {
      try {
        let url =
          activeTab == 1
            ? `${baseUrl}/admin/partner`
            : `${baseUrl}/admin/student`;
        let response = await Axios.get(url);
        setData(response.data.data);
      } catch (err) {
        // console.log(err.response, "err");
        if (err.response) {
          console.log(err.response.data.message, "error");
        }
      }
    } else if (tab == 2) {
      try {
        let response = await Axios.get(`${baseUrl}/admin/student`);
        setData(response.data.data);
      } catch (err) {
        // console.log(err.response, "err");
        if (err.response) {
          console.log(err.response.data.message, "error");
        }
      }
    }
  };
  function handleSwitch(id) {
    if (id) {
      setstudentID(id);
      setpartnerID(0);

      setstudentTab(!studentTab);
      setactiveIndex(1);
      setActiveTab(1);
    }
  }
  function handleSwitchPartner(id) {
    if (id) {
      setpartnerID(id);
      setstudentID(0);
      setpartnerTab(!partnerTab);
      setactiveIndex(1);
      setActiveTab(1);
    }
  }
  const handleBack = async () => {
    try {
      let response = await Axios.get(`${baseUrl}/admin/student`);
      setData(response.data.data);
    } catch (err) {
      // console.log(err.response, "err");
      if (err.response) {
        console.log(err.response.data.message, "error");
      }
    }

    setactiveIndex(2);

    setActiveTab(2);
    setpartnerID(0);
    setstudentID(0);
    setstudentTab(false);
    setpartnerTab(false);
  };
  function Page() {
    if (activeTab == 1) {
      return (
        <Partners
          data={Data}
          setData={setData}
          handleSwitchPartner={handleSwitchPartner}
          activeTab={activeTab}
        />
      );
    } else if (activeTab == 2) {
      return (
        <Students
          data={Data}
          setData={setData}
          handleSwitch={handleSwitch}
          activeTab={activeTab}
        />
      );
    } else if (activeTab == 3) {
      return <EditProfile />;
    } else if (activeTab == 4) {
      return <ChangePassword />;
    }
  }

  function StudentPage() {
    if (activeTab == 1) {
      return <Application studentID={studentID} />;
    } else if (activeTab == 2) {
      return <ActiveLoans setActiveTab={setActiveTab} studentID={studentID} />;
    } else if (activeTab == 3) {
      return <Tools />;
    } else if (activeTab == 4) {
      return <Faq />;
    } else if (activeTab == 5) {
      return <EditProfile />;
    } else if (activeTab == 6) {
      return <ChangePassword />;
    }
  }
  function PartnerPage() {
    if (activeTab == 1) {
      return (
        <StudentPartners partnerID={partnerID} handleSwitch={handleSwitch} />
      );
    } else if (activeTab == 2) {
      return <Tools />;
    } else if (activeTab == 3) {
      return <Faq />;
    }
  }
  useEffect(() => {
    if (activeTab == 1) {
      getAllUsers();
    }
  }, [activeTab]);

  const getAllUsers = () => {
    if (props.todos && props.todos.length > 0) {
      setData(props.todos);
    } else {
      return;
    }
  };
  return (
    <React.Fragment>
      <div className="admin_dashbaord ptb-100">
        <div className="container-fluid">
          <div className="row">
            {studentTab === true ? (
              <StudentSidebar
                toggle={toggle}
                isActive={activeIndex}
                studentTab={studentTab}
                handleBack={handleBack}
              />
            ) : partnerTab === true ? (
              <PartnerSidebar
                toggle={toggle}
                isActive={activeIndex}
                partnerTab={partnerTab}
                handleBack={handleBack}
              />
            ) : (
              <Sidebar toggle={toggle} isActive={activeIndex} />
            )}
            {studentTab === true ? (
              <div className=" col-lg-9">
                <div className="td-text-area">{StudentPage()}</div>
              </div>
            ) : partnerTab === true ? (
              <div className=" col-lg-9">
                <div className="td-text-area">{PartnerPage()}</div>
              </div>
            ) : (
              <div className="rightSidebar col-lg-9">
                <div className="td-text-area">{Page()}</div>
              </div>
            )}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export const getServerSideProps = async () => {
  let todos = [];
  try {
    const res = await Axios.get(`${baseUrl}/admin/partner`);
    todos = res.data.data;
  } catch (error) {
    todos = [];
  }

  return { props: { todos: todos } };
};

export default Dashboard;
