import React, { useEffect } from "react";
import Axios from "axios";
import Notifier from "../../../../../../utils/Notifier";
import baseUrl from "../../../../../../utils/baseUrl";
import PhoneInput from "../../../../../../components/CustomPhoneInput.js";

import { Store } from "../../../../../../Context";

import { useFormik } from "formik";
import * as Yup from "yup";
import { getUser, getToken } from "../../../../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
export default function Create(props) {
  const [loading, setloading] = React.useState(false);

  useEffect(async () => {
    if (token) {
      try {
        const config = {
          headers: { Authorization: `Bearer ${token}` },
        };
        let response = await Axios.get(`${baseUrl}/admin/partner`, config);
        response.data.data && props.setData(response.data.data);
      } catch (err) {
        if (err.response) console.log(err.response.data.message, "err");
      }
    }
  }, [props.createModal, props.setData]);
  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      phone: "",
      address: "",
      website: "",
      reason: "",
      password: "",

      company_name: "",
    },
    validationSchema: Detail_YUP,
    onSubmit: async (values) => {
      setloading(true);

      try {
        let response = await Axios({
          method: "post",
          url: `${baseUrl}/admin/createPartner`,
          data: values,
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        Notifier(response.data.message, "success");

        props.toggle();
        setloading(false);
        props.setData(response.data.data);
      } catch (err) {
        setloading(false);
        if (err.response) {
          Notifier(err.response.data.message, "error");
        }
      }
    },
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "phone") {
      formik.setFieldValue(name, value.replace(/\D/g, ""));
    } else {
      formik.setFieldValue(name, value);
    }
  };
  const onNumberChange = (value) => {
    formik.setFieldValue("phone", value);
  };
  return (
    <div>
      <form className="w-full ">
        <div className="flex no-wrap -mx-3 mb-2">
          <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-first-name"
            >
              First Name
            </label>
            <input
              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
              type="text"
              placeholder="Full Name"
              name="firstName"
              onChange={handleChange}
              value={formik.values.firstName}
            />
            <div className="text-danger ">
              {formik.touched.firstName && formik.errors.firstName ? (
                <small>{formik.errors.firstName}</small>
              ) : null}
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-first-name"
            >
              Last Name
            </label>
            <input
              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
              type="text"
              placeholder="Last Name"
              name="lastName"
              onChange={handleChange}
              value={formik.values.lastName}
            />
            <div className="text-danger ">
              {formik.touched.lastName && formik.errors.lastName ? (
                <small>{formik.errors.lastName}</small>
              ) : null}
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-last-name"
            >
              Email
            </label>
            <input
              className="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
              type="email"
              placeholder="Email"
              name="email"
              onChange={handleChange}
              value={formik.values.email}
            />
            <div className="text-danger ">
              {formik.touched.email && formik.errors.email ? (
                <small>{formik.errors.email}</small>
              ) : null}
            </div>
          </div>
        </div>
        <div className="flex no-wrap -mx-3 mb-2">
          <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-first-name"
            >
              Password
            </label>
            <input
              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
              type="password"
              placeholder="Password"
              name="password"
              onChange={handleChange}
              value={formik.values.password}
            />
            <div className="text-danger ">
              {formik.touched.password && formik.errors.password ? (
                <small>{formik.errors.password}</small>
              ) : null}
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-first-name"
            >
              Phone Number
            </label>
            <PhoneInput value={formik.values.phone} onChange={onNumberChange} />

            <div className="text-danger ">
              {formik.touched.phone && formik.errors.phone ? (
                <small>{formik.errors.phone}</small>
              ) : null}
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-first-name"
            >
              Company Name
            </label>
            <input
              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
              type="text"
              placeholder="Company Name"
              name="company_name"
              onChange={handleChange}
              value={formik.values.company_name}
            />
            <div className="text-danger ">
              {formik.touched.company_name && formik.errors.company_name ? (
                <small>{formik.errors.company_name}</small>
              ) : null}
            </div>
          </div>
        </div>
        <div className="flex no-wrap -mx-3 mb-2">
          <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-first-name"
            >
              Company Address
            </label>
            <input
              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
              type="text"
              placeholder="Company Address"
              name="address"
              onChange={handleChange}
              value={formik.values.address}
            />
            <div className="text-danger ">
              {formik.touched.address && formik.errors.address ? (
                <small>{formik.errors.address}</small>
              ) : null}
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-first-name"
            >
              Website
            </label>
            <input
              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
              type="text"
              placeholder="Website"
              name="website"
              onChange={handleChange}
              value={formik.values.website}
            />
            <div className="text-danger ">
              {formik.touched.website && formik.errors.website ? (
                <small>{formik.errors.website}</small>
              ) : null}
            </div>
          </div>
        </div>
        <div className="w-full md:w-1/2 px-1 mb-6 md:mb-0">
          <label
            className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
            htmlFor="grid-first-name"
          >
            Why would you like to partner with WESTEND?
          </label>

          <textarea
            id="noter-text-area"
            placeholder="Reason"
            className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
            name="reason"
            onChange={handleChange}
            value={formik.values.reason}
          />
        </div>
        <div className="row pb-2 pt-3 ">
          <div className="col-lg-12 text-right">
            <button
              // disabled={activeStep === 0}
              onClick={formik.handleSubmit}
              className="default-btn4"
            >
              {loading ? "Creating..." : "Create"} <span></span>
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}

const Detail_YUP = Yup.object({
  firstName: Yup.string().required("Required"),
  lastName: Yup.string().required("Required"),
  password: Yup.string().required("Required"),

  email: Yup.string().email("Invalid email address").required("Required"),
  phone: Yup.string()
    .min(10, "Must 10 characters long")
    // .max(15, "Must be equal to 10 characters ")
    .required("Required"),
  address: Yup.string().required("Required"),
  company_name: Yup.string().required("Required"),
});
