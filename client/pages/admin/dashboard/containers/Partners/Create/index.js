import React from "react";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import FontAwesomeIcon from "../../../../../../components/FontAwesomeIcon/index.js";

export default function Create(props) {
  return (
    <button
      // disabled={activeStep === 0}
      onClick={props.toggle}
      className="default-btn3"
    >
      <FontAwesomeIcon icon={faPlus} className="mr-1" />
      Create<span></span>
    </button>
  );
}
