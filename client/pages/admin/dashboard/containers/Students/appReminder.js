import React, { useEffect } from "react";
import Axios from "axios";
import Notifier from "../../../../../utils/Notifier";
import baseUrl from "../../../../../utils/baseUrl";

import { Store } from "../../../../../Context";

import { useFormik } from "formik";
import * as Yup from "yup";
import { useRouter } from "next/router";
import { getUser, getToken } from "../../../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
export default function Create(props) {
  const [loading, setloading] = React.useState(false);

  const formik = useFormik({
    initialValues: {
      subject: "",
      content: "",
      email: "",
    },
    validationSchema: Detail_YUP,
    onSubmit: async (values) => {
      setloading(true);
      values.email = props.studentEmail;
      values.subject = values.subject
        ? values.subject
        : "Application Reminder";
      values.content = values.content
        ? values.content
        : "Your application is yet not completed. Do not stop to fulfil your dreams. Please complete your application to get started.";

      try {
        let response = await Axios({
          method: "post",
          url: `${baseUrl}/auth/applicationReminder`,
          data: values,
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        props.toggle();
        setloading(false);

        Notifier(response.data.message, "success");

        // router.push("/profile");
      } catch (err) {
        setloading(false);
        if (err.response) {
          Notifier(err.response.data.message, "error");
        }
      }
    },
  });

  return (
    <div>
      <form className="w-full ">
        <div className="flex  -mx-3 mb-2">
          <div className="w-full md:w-1/2  pl-3 mb-6 md:mb-0">
            <label htmlFor="username">Subject</label>
            <input
              type="text"
              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
              placeholder="Subject"
              id="subject"
              name="subject"
              onChange={formik.handleChange}
              value={formik.values.subject}
            />
            <div className="text-danger pt-1">
              {formik.touched.subject && formik.errors.subject ? (
                <div className="formikError">{formik.errors.subject}</div>
              ) : null}
            </div>
          </div>
          <div className="w-full  md:w-1/2 px-3 mb-6 md:mb-0">
            <label htmlFor="username">Description</label>

            <textarea
              id="noter-text-area"
              placeholder="Description"
              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
              name="content"
              onChange={formik.handleChange}
              value={formik.values.content}
            />
            <div className="text-danger pt-1">
              {formik.touched.content && formik.errors.content ? (
                <div className="formikError">{formik.errors.content}</div>
              ) : null}
            </div>
          </div>
        </div>
        <div className="row pb-2 ">
          <div className="col-lg-12 text-right">
            <button
              // disabled={activeStep === 0}
              onClick={formik.handleSubmit}
              className="default-btn4"
            >
              {loading ? "Creating..." : "Create"} <span></span>
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}

const Detail_YUP = Yup.object({});
