import React from "react";
import Table from "../../../../../components/Table";
import Table2 from "../../../../../components/Table/table.js";
import Notifier from "../../../../../utils/Notifier";
import { Store } from "../../../../../Context";
import baseUrl from "../../../../../utils/baseUrl";
import imageUrl from "../../../../../utils/imageUrl";

import Axios from "axios";

import Modal from "../../../../../components/Modals";
import { StudentListheadCells } from "../../../../../utils/Globals/index.js";
import CreateStudent from "./Create/create";
import EditStudent from "./Edit/index";
import AppReminder from "./appReminder";
import jsPDF from "jspdf";
import "jspdf-autotable";
import moment from "moment";
import Filter from "../filter";
import { getUser, getToken } from "../../../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
export default function listOfPartners(props) {
  const [createModal, setcreateModal] = React.useState(false);
  const [appModal, setappModal] = React.useState(false);

  const [studentId, setstudentId] = React.useState(0);
  const [studentEmail, setstudentEmail] = React.useState("");

  const createToggle = () => {
    setcreateModal(!createModal);
  };
  const appToggle = (email) => {
    if (email) {
      setstudentEmail(email);
    }
    setappModal(!appModal);
  };
  const [editModal, seteditModal] = React.useState(false);
  const editToggle = (id) => {
    if (id) {
      setstudentId(id);
    }
    seteditModal(!editModal);
  };
  const handleDelete = async (id) => {
    const confirmis = window.confirm("Are you sure you want to delete?");

    if (confirmis === true) {
      let fill = props.data.filter((dt) => dt.id !== id);
      props.setData(fill);
      try {
        let response = await Axios({
          method: "delete",
          url: `${baseUrl}/admin/deleteStudent/${id}`,

          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        Notifier(response.data.message, "success");
        // router.reload();
      } catch (err) {
        if (err.response) {
          Notifier(err.response.data.message, "error");
        }
      }
    }
  };

  // expport data
  const generatePDF = async (studentId, setLoading) => {
    try {
      let response = await Axios({
        method: "put",
        url: `${baseUrl}/admin/generatePdf/${studentId}`,
        data: {
          url: imageUrl,
        },
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      let { mediaLink, fileName } = response.data;
      fetch(mediaLink, {
        method: "GET",
        headers: {
          "Content-Type": "application/pdf",
        },
      })
        .then((response) => response.blob())
        .then((blob) => {
          // Create blob link to download
          const url = window.URL.createObjectURL(new Blob([blob]));
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download", fileName);
          // Append to html link element page
          document.body.appendChild(link);
          // Start download
          link.click();
          // Clean up and remove the link
          link.parentNode.removeChild(link);
        });
    } catch (error) {}
  };

  return (
    <div>
      <Modal
        title="Appplication Reminder"
        modal={appModal}
        toggle={appToggle}
        content={
          <AppReminder
            setData={props.setData}
            toggle={appToggle}
            setData={props.setData}
            studentEmail={studentEmail}
          />
        }
      />
      <Modal
        title="Create Student"
        modal={createModal}
        toggle={createToggle}
        content={
          <CreateStudent
            setData={props.setData}
            toggle={createToggle}
            setData={props.setData}
          />
        }
      />
      <Modal
        title="Edit Student"
        modal={editModal}
        toggle={editToggle}
        content={
          <EditStudent
            data={props.data}
            setData={props.setData}
            id={studentId}
            toggle={editToggle}
          />
        }
      />
      <Filter
        setData={props.setData}
        createToggle={createToggle}
        data={props.data}
        filter="STUDENT"
        activeTab={props.activeTab}
        tableID="studentID"
      />

      {props.data && props.data.length > 0 ? (
        <Table2
          id="studentID"
          data={props.data}
          handleEdit={editToggle}
          toggle={appToggle}
          handleDelete={handleDelete}
          handleSwitch={props.handleSwitch}
          headCells={StudentListheadCells}
          generatePDF={generatePDF}
        />
      ) : (
        <h6 className="text-center">No Data Found</h6>
      )}
    </div>
  );
}
