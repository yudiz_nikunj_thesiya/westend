import React, { useEffect } from "react";
import {
  faPlus,
  faSort,
  faFileExcel,
  faCheck,
} from "@fortawesome/free-solid-svg-icons";
import FontAwesomeIcon from "../../../../components/FontAwesomeIcon/index.js";
import List from "../../../../components/List/index.js";
import DateRangePicker from "../../../../components/DateRangePicker/index.js";
import AutoComplete from "../../../../components/AutoComplete/Index.js";
import { application_status, countries } from "../../../../utils/Globals";
import Popover from "@material-ui/core/Popover";
import "jspdf-autotable";
import Notifier from "../../../../utils/Notifier";
import baseUrl from "../../../../utils/baseUrl";
import Axios from "axios";
import { getUser, getToken } from "../../../../utils/auth";
import moment from "moment";
const token = getToken();
export default function filter(props) {
  const { createToggle } = props;
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [searchInput, setsearchInput] = React.useState("");
  const [date, setDate] = React.useState("");
  const [app_status, setapp_status] = React.useState("");
  const [country, setCountry] = React.useState("");

  const [applications, setApplications] = React.useState("");

  let checkedArr =
    props.activeTab == 2
      ? ["student_name", "partner_name", "email"]
      : ["partner_name", "phone", "email"];
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  const [checked, setChecked] = React.useState();
  useEffect(() => {
    let checkedArray =
      props.activeTab == 2
        ? ["student_name", "partner_name", "email"]
        : ["partner_name", "phone", "email"];
    setChecked(checkedArray);
  }, []);
  useEffect(() => {
    getAllApplications();
  }, [setDate, date]);
  useEffect(() => {
    if (
      date === "" &&
      app_status === "" &&
      country === "" &&
      props.filter == "STUDENT"
    ) {
      getUsers();
    } else {
      if (props.filter == "STUDENT") {
        getFilteredData();
      }
      if (props.filter == "PARTNER" && date == "" && searchInput == "") {
        getUsers();
      }
    }
  }, [date, app_status, country]);
  const getFilteredData = async () => {
    let url;
    if (date == "" || country == "" || app_status === "") {
      url = `${baseUrl}/admin/search/searchFilter?country=${country}&date=${date}&status=${app_status}`;
    }

    try {
      let response = await Axios({
        method: "get",
        url: url,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      props.setData(response.data.data);

      // router.reload();
    } catch (err) {
      if (err.response) {
        console.log(err.response.data.message, "error");
      }
    }
  };
  const getUsers = async () => {
    try {
      let res;
      props.filter == "STUDENT" && props.activeTab == 2
        ? (res = await Axios.get(`${baseUrl}/admin/student`))
        : (res = await Axios.get(`${baseUrl}/admin/partner`));

      props.setData(res.data.data);
    } catch (error) {}
  };
  const getAllApplications = async () => {
    try {
      let response = await Axios({
        method: "get",
        url: `${baseUrl}/admin/application/get`,
      });
      setApplications(response.data.data);

      // router.reload();
    } catch (err) {
      if (err.response) {
        console.log(err.response.data.message, "error");
      }
    }
  };
  const handleChange = async (e) => {
    let value = e.target.value;
    setsearchInput(value);

    let url =
      props.filter == "STUDENT"
        ? `${baseUrl}/admin/search/filter?q=${value}&date=${date}&status=${app_status}&country=${country}&email=${checked.includes(
            "email"
          )}&name=${checked.includes(
            "student_name"
          )}&partner_name=${checked.includes("partner_name")}`
        : `${baseUrl}/admin/?q=${value}&date=${date}&email=${checked.includes(
            "email"
          )}&name=${checked.includes("partner_name")}&phone=${checked.includes(
            "phone"
          )}`;
    try {
      let response = await Axios({
        method: "get",
        url: url,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      props.setData(response.data.data);

      // router.reload();
    } catch (err) {
      if (err.response) {
        Notifier(err.response.data.message, "error");
      }
    }
  };

  const handleDateChange = async (dates, datesStrings, info) => {
    if (datesStrings[0] == "" || datesStrings[1] == "") {
      setDate("");
    } else {
      let value = datesStrings[0] + " - " + datesStrings[1];
      // 10/20/2021 - 11/22/2021
      setDate(value);
      let url =
        props.filter == "STUDENT"
          ? `${baseUrl}/admin/search/searchFilter?country=${country}&date=${value}&status=${app_status}&role=student`
          : `${baseUrl}/admin/search/searchFilter?country=${country}&date=${value}&status=${app_status}&role=partner`;

      try {
        let response = await Axios({
          method: "get",
          url: url,
        });
        props.setData(response.data.data);

        // router.reload();
      } catch (err) {
        if (err.response) {
          console.log(err.response.data.message, "error");
        }
      }
    }
  };

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setChecked(newChecked);
  };

  function ExportToExcel(type = "xlsx", id) {
    let filteredArray = [];
    let NewArray = [];
    let unmatching = [...applications];
    let matching = [];
    let sortedArray = props.data.sort(function (a, b) {
      let result = new Date(b.createdAt) - new Date(a.createdAt);
      return result;
    });
    if (props.filter === "STUDENT") {
      sortedArray.forEach((user) => {
        var index = unmatching.findIndex(({ userId }) => user.id == userId);
        if (index !== -1) {
          matching.push(
            Object.assign({}, unmatching.splice(index, 1)[0], user)
          );
        }
      });
      if (matching && matching.length > 0) {
        filteredArray = matching.map((user) => {
          let obj = {
            FullName: user.firstName + " " + user.lastName,
            Email: user.email,
            Phone: user.phone,
            ApplicationStatus: user.app_status,
            LoanAmount: user.loanAmount,
            Country: user.country,
            ApplyingFor: user.apply,
            CourseStatus: user.status,
            Comment: user.comment,
            "Date/Time": moment(user.createdAt).format("DD/MM/YYYY HH:mm"),
          };
          NewArray.push(obj);
          return NewArray;
        });
        filteredArray = [...NewArray];
      }
    } else {
      filteredArray = sortedArray.map((user) => {
        return {
          PartnerName: user.firstName + " " + user.lastName,
          Email: user.email,
          Phone: user.phone,
          CompanyName: user.company_name,
          Approved: user.approved,
          "Date/time": moment(user.createdAt).format("DD/MM/YYYY HH:mm"),
        };
      });
    }

    let name = Math.floor(Math.random() * 1000000000);
    const ws = XLSX.utils.json_to_sheet(filteredArray);
    const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
    XLSX.writeFile(wb, `${name}.` + type);
  }
  const handleStatusChange = async (name, value) => {
    setapp_status(value);
    let url = `${baseUrl}/admin/search/searchFilter?country=${country}&date=${date}&status=${value}&role=student`;

    try {
      let response = await Axios({
        method: "get",
        url: url,
      });
      props.setData(response.data.data);
    } catch (err) {
      if (err.response) {
        console.log(err.response.data.message, "error");
      }
    }
  };
  const handleCountryChange = async (name, value) => {
    setCountry(value);
    if (value !== "") {
      let url = `${baseUrl}/admin/search/searchFilter?country=${value}&date=${date}&status=${app_status}&role=student`;

      try {
        let response = await Axios({
          method: "get",
          url: url,
        });
        props.setData(response.data.data);
      } catch (err) {
        if (err.response) {
          console.log(err.response.data.message, "error");
        }
      }
    }
  };
  // ********
  return (
    <div className="d-flex mb-2 justify-content-between">
      <div className="d-flex mt-1">
        <div>
          <button
            // disabled={activeStep === 0}
            onClick={createToggle}
            className="no-icon default-btn"
          >
            <FontAwesomeIcon icon={faPlus} className="mr-1" />
            Create<span></span>
          </button>
        </div>
        <div>
          {props.filter != "STUDENT" ? (
            <button
              // disabled={activeStep === 0}
              onClick={props.handleApprove}
              className="no-icon default-btn ml-3"
            >
              <FontAwesomeIcon icon={faCheck} className="mr-1" />
              Approve<span></span>
            </button>
          ) : (
            ""
          )}
        </div>
      </div>

      <div className=" d-flex">
        {props.filter === "STUDENT" ? (
          <>
            <div className="app_status">
              <AutoComplete
                data={application_status}
                name="app_status"
                label="Choose a status"
                onChange={handleStatusChange}
                value={app_status}
              />
            </div>
            <div className="app_status">
              <AutoComplete
                data={countries}
                name="country"
                label="Choose a country"
                onChange={handleCountryChange}
                value={country}
              />
            </div>
          </>
        ) : (
          ""
        )}
        <div className="d-flex searchInput">
          <input
            className="appearance-none block w-40  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
            type="text"
            placeholder="Search"
            name="searchInput"
            value={searchInput}
            onChange={handleChange}
          />
          <div>
            <FontAwesomeIcon
              color="#d33"
              aria-describedby={id}
              icon={faSort}
              onClick={handleClick}
            />
          </div>
        </div>
        <div className="ml-2 w-40">
          <DateRangePicker onChange={handleDateChange} />
        </div>
        <div className="ml-2 mt-2 faAngleDoubleDown">
          <Popover
            id={id}
            open={open}
            anchorEl={anchorEl}
            onClose={handleClose}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "center",
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "center",
            }}
          >
            <List onClick={handleToggle} data={checkedArr} checked={checked} />
          </Popover>
          <FontAwesomeIcon
            color="#152f68"
            aria-describedby={id}
            icon={faFileExcel}
            onClick={() => ExportToExcel("xlsx", props.tableID)}
            className="faExcel ml-1 "
          />
        </div>
      </div>
    </div>
  );
}
