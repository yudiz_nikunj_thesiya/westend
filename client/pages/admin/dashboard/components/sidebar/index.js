import React from "react";

export default function index(props) {
  return (
    <div className="sidebar ">
      <div className="td-sidebar">
        <ul>
          <li>
            <a
              onClick={() => props.toggle(1)}
              className={props.isActive === 1 ? "active" : "album"}
            >
              Partners
            </a>
          </li>
          <li>
            <a
              onClick={() => props.toggle(2)}
              className={props.isActive === 2 ? "active" : "album"}
            >
              Students
            </a>
          </li>
          <li>
            <a
              onClick={() => props.toggle(3)}
              className={props.isActive === 3 ? "active" : "album"}
            >
              Edit Profile
            </a>
          </li>
          <li>
            <a
              onClick={() => props.toggle(4)}
              className={props.isActive === 4 ? "active" : "album"}
            >
              Change Password
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}
