import React, { useState, useEffect } from "react";
import Sidebar from "./components/sidebar";
import Students from "./containers/Students";
import EditProfile from "./edit-profile";
import ChangePassword from "../../edit-password";
import { parseCookies } from "nookies";
import cookie from "js-cookie";
import StudentSidebar from "../../student/dashboard/components/sidebar/index.js";
import Application from "../../student/dashboard/containers/Application/index.js";
import ActiveLoans from "../../student/dashboard/containers/ActiveLoans/index.js";

import baseUrl from "../../../utils/baseUrl";
import Notifier from "../../../utils/Notifier";
import Axios from "axios";
import Tools from "./containers/Tools/index.js";
import Faq from "./containers/Faq/index.js";
import { Store } from "../../../Context";
import { useRouter } from "next/router";
import Button from "../../../components/Button/index";
import {
  faLongArrowAltLeft,
  faSort,
  faDownload,
  faCalendarAlt,
} from "@fortawesome/free-solid-svg-icons";
import FontAwesomeIcon from "../../../components/FontAwesomeIcon/index.js";

import { getUser, getToken } from "../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
const Index = (props) => {
  const router = useRouter();
  const [Tab, setTab] = useState("partner");
  const [Data, setData] = useState("");
  const [activeTab, setActiveTab] = React.useState(1);
  const [activeIndex, setactiveIndex] = React.useState(1);
  const [studentID, setstudentID] = React.useState(0);
  const [studentTab, setstudentTab] = React.useState(false);
  const toggle = async (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
    setactiveIndex(tab);
    if (tab == 1) {
      let todos = [];
      try {
        const res = await Axios.get(`${baseUrl}/partner/${loggedInUser.id}`);
        todos = res.data.data;
        setData(todos);
      } catch (error) {
        todos = [];
      }
    }
  };

  useEffect(async () => {
    if (props.todos && props.todos.length > 0) {
      setData(props.todos);
    } else {
      let todos = [];
      try {
        const res = await Axios.get(`${baseUrl}/partner/${loggedInUser.id}`);
        todos = res.data.data;
        setData(todos);
      } catch (error) {
        todos = [];
      }
    }
  }, []);
  function handleSwitch(id) {
    if (id) {
      setstudentID(id);
      setstudentTab(!studentTab);
      setactiveIndex(1);
      setActiveTab(1);
    }
  }
  const handleBack = () => {
    setstudentTab(!studentTab);
  };
  function Page() {
    if (activeTab == 1) {
      return (
        <Students
          data={Data}
          setData={setData}
          handleSwitch={handleSwitch}
          activeTab={activeTab}
        />
      );
    } else if (activeTab == 2) {
      return <Tools />;
    } else if (activeTab == 3) {
      return <Faq />;
    } else if (activeTab == 4) {
      return <EditProfile />;
    } else if (activeTab == 5) {
      return <ChangePassword />;
    }
  }

  function StudentPage() {
    if (activeTab == 1) {
      return <Application studentID={studentID} />;
    } else if (activeTab == 2) {
      return <ActiveLoans setActiveTab={setActiveTab} />;
    } else if (activeTab == 3) {
      return <Tools />;
    } else if (activeTab == 4) {
      return <Faq />;
    } else if (activeTab == 5) {
      return <EditProfile />;
    } else if (activeTab == 6) {
      return <ChangePassword />;
    }
  }
  return (
    <React.Fragment>
      <div className="ptb-100">
        <div className="container">
          <div className="row">
            {studentTab === false ? (
              <Sidebar toggle={toggle} isActive={activeIndex} />
            ) : (
              <StudentSidebar toggle={toggle} isActive={activeIndex} />
            )}

            <div className="col-lg-9">
              <div className="td-text-area">
                {studentTab === false ? Page() : StudentPage()}
              </div>
            </div>
          </div>
        </div>
      </div>
      {studentTab === false ? (
        ""
      ) : (
        <div className="backButton ">
          <div className="container">
            <div className="row">
              <div className="col-lg-5 ">
                <div className="d-flex justify-content-center">
                  <Button
                    onClick={handleBack}
                    className="no-icon default-btn"
                    icon={faLongArrowAltLeft}
                    label="Back"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </React.Fragment>
  );
};

export const getServerSideProps = async () => {
  let todos = [];
  let cooky = cookie.get("user");
  let loggedInUser = cooky && JSON.parse(cooky);

  try {
    const res = await Axios.get(`${baseUrl}/partner/${loggedInUser.id}`);
    todos = res.data.data;
  } catch (error) {
    todos = [];
  }

  return { props: { todos: todos } };
};
export default Index;
