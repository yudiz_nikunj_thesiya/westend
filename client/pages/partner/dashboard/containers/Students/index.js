import React from "react";
import Table from "../../../../../components/Table";
import Table2 from "../../../../../components/Table/table.js";
import Notifier from "../../../../../utils/Notifier";
import { Store } from "../../../../../Context";
import baseUrl from "../../../../../utils/baseUrl";
import Axios from "axios";
import Data from "../../../../../utils/data.json";
import CreateModal from "../../../../../components/Modals";
import EditModal from "../../../../../components/Modals";
import { StudentListheadCells } from "../../../../../utils/Globals/index.js";
import CreateStudent from "./Create/create";
import EditStudent from "./Edit/index";

import Filter from "../filter";
import { getUser, getToken } from "../../../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
export default function listOfPartners(props) {
  const [createModal, setcreateModal] = React.useState(false);
  const [studentId, setstudentId] = React.useState(false);
  const [Data, setData] = React.useState("");

  const createToggle = () => {
    setcreateModal(!createModal);
  };
  const [editModal, seteditModal] = React.useState(false);
  const editToggle = (id) => {
    if (id) {
      setstudentId(id);
    }
    seteditModal(!editModal);
  };
  const handleDelete = async (id) => {
    const confirmis = window.confirm("Are you sure you want to delete?");

    if (confirmis === true) {
      let fill = Data.filter((dt) => dt.id !== id);
      setData(fill);
      try {
        let response = await Axios({
          method: "delete",
          url: `${baseUrl}/admin/deleteStudent/${id}`,
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        Notifier(response.data.message, "success");
        // router.reload();
      } catch (err) {
        if (err.response) {
          Notifier(err.response.data.message, "error");
        }
      }
    }
  };
  React.useEffect(async () => {
    if (props.data) {
      setData(props.data);
    } else {
      let todos = [];
      let _id = loggedInUser.id;
      if (props.partnerID) {
        _id = props.partnerID;
      }
      try {
        const res = await Axios.get(`${baseUrl}/partner/${_id}`);
        todos = res.data.data;
        setData(todos);
      } catch (error) {
        console.log(error, "error");
        todos = [];
      }
    }
  }, [props.partnerID, props.data]);
  return (
    <div>
      <CreateModal
        title="Create Student"
        modal={createModal}
        toggle={createToggle}
        content={
          <CreateStudent
            setData={setData}
            toggle={createToggle}
            partnerID={props.partnerID}
          />
        }
      />
      <EditModal
        title="Edit Student"
        modal={editModal}
        toggle={editToggle}
        content={
          <EditStudent
            data={Data}
            setData={setData}
            id={studentId}
            partnerID={props.partnerID}
            toggle={editToggle}
          />
        }
      />
      <Filter
        setData={setData}
        createToggle={createToggle}
        data={Data}
        filter="STUDENT"
        partnerID={props.partnerID}
        activeTab={props.activeTab}
        tableID="studentID"
      />

      {Data && Data.length > 0 ? (
        <Table2
          id="studentID"
          data={Data}
          handleEdit={editToggle}
          handleDelete={handleDelete}
          headCells={StudentListheadCells}
          handleSwitch={props.handleSwitch}
        />
      ) : (
        <h6 className="text-center">No Data Found</h6>
      )}
    </div>
  );
}
