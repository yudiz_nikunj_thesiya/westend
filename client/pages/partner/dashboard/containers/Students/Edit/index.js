import React, { useEffect } from "react";
import Axios from "axios";
import Notifier from "../../../../../../utils/Notifier";
import baseUrl from "../../../../../../utils/baseUrl";
import PhoneInput from "../../../../../../components/CustomPhoneInput.js";

import { Store } from "../../../../../../Context";

import { useFormik } from "formik";
import * as Yup from "yup";
import { getUser, getToken } from "../../../../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
export default function Create(props) {
  const [loading, setloading] = React.useState(false);
  useEffect(() => {
    if (props.data && props.id) {
      let temp = props.data.find((dt) => dt.id == props.id);
      if (temp) {
        formik.setFieldValue("firstName", temp.firstName);
        formik.setFieldValue("lastName", temp.lastName);

        formik.setFieldValue("email", temp.email);
        formik.setFieldValue("phone", temp.phone);
      }
    }
  }, [props]);
  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      phone: "",
    },
    validationSchema: Detail_YUP,
    onSubmit: async (values) => {
      setloading(true);
      if (loggedInUser) {
        let _id = loggedInUser.id;
        if (props.partnerID) {
          _id = props.partnerID;
        }
        values["partnerId"] = _id;
        try {
          let response = await Axios({
            method: "put",
            url: `${baseUrl}/partner/updateStudent/${props.id}`,
            data: values,
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          props.toggle();
          setloading(false);
          props.setData(response.data.data);

          Notifier(response.data.message, "success");

          // router.push("/profile");
        } catch (err) {
          setloading(false);
          if (err.response) {
            Notifier(err.response.data.message, "error");
          }
        }
      }
    },
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "phone") {
      formik.setFieldValue(name, value.replace(/\D/g, ""));
    } else {
      formik.setFieldValue(name, value);
    }
  };
  const onNumberChange = (value) => {
    formik.setFieldValue("phone", value);
  };
  return (
    <div>
      <form className="w-full ">
        <div className="flex  -mx-3 mb-2">
          <div className="w-full md:w-1/2  px-3 mb-6 md:mb-0">
            {/* <label htmlFor="username">Username</label> */}
            <input
              type="text"
              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
              placeholder="First Name"
              id="firstName"
              name="firstName"
              onChange={formik.handleChange}
              value={formik.values.firstName}
            />
            <div className="text-danger pt-1">
              {formik.touched.firstName && formik.errors.firstName ? (
                <div className="formikError">{formik.errors.firstName}</div>
              ) : null}
            </div>
          </div>
          <div className="w-full md:w-1/2  px-3 mb-6 md:mb-0">
            {/* <label htmlFor="username">Username</label> */}
            <input
              type="text"
              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
              placeholder="Last Name"
              id="lastName"
              name="lastName"
              onChange={formik.handleChange}
              value={formik.values.lastName}
            />
            <div className="text-danger pt-1">
              {formik.touched.lastName && formik.errors.lastName ? (
                <div className="formikError">{formik.errors.lastName}</div>
              ) : null}
            </div>
          </div>
        </div>
        <div className="flex no-wrap -mx-3 mb-0">
          <div className="w-full md:w-1/2 px-3">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-last-name"
            >
              Email
            </label>
            <input
              className="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
              type="email"
              placeholder="Email"
              name="email"
              onChange={handleChange}
              value={formik.values.email}
            />
            <div className="text-danger ">
              {formik.touched.email && formik.errors.email ? (
                <div>{formik.errors.email}</div>
              ) : null}
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-first-name"
            >
              Phone Number
            </label>
            <PhoneInput value={formik.values.phone} onChange={onNumberChange} />

            <div className="text-danger ">
              {formik.touched.phone && formik.errors.phone ? (
                <div>{formik.errors.phone}</div>
              ) : null}
            </div>
          </div>
        </div>
        <div className="row pb-2 ">
          <div className="col-lg-12 text-right">
            <button
              // disabled={activeStep === 0}
              onClick={formik.handleSubmit}
              className="default-btn4"
            >
              {loading ? "Updating..." : "Update"} <span></span>
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}

const Detail_YUP = Yup.object({
  firstName: Yup.string().required("Required"),
  lastName: Yup.string().required("Required"),
  email: Yup.string().email("Invalid email address").required("Required"),

  phone: Yup.string()
    .min(10, "Must 10 characters long")
    // .max(15, "Must be equal to 10 characters ")
    .required("Required"),
});
