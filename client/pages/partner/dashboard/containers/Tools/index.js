import React from "react";
import Link from "next/link";

const Features = () => {
  return (
    <div className="tools-area">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-3 col-sm-6 col-md-6">
            <Link href="/emiCalculator">
              <div className="features-box">
                <div className="icon">
                  <i className="flaticon-brain-process"></i>
                </div>
                <h3>EMI Calculator</h3>
                <p>
                  Make a prudent estimation of your education loan EMIs with the
                  Westend Loan EMI Calculator
                </p>
                <Link href="/emiCalculator">
                  <a className="link-btn">Check Now</a>
                </Link>
                <div className="back-icon">
                  <i className="flaticon-brain-process"></i>
                </div>
              </div>
            </Link>
          </div>
          <div className="col-lg-3 col-sm-6 col-md-6">
            <div className="features-box">
              <div className="icon">
                <i className="flaticon-shield-1"></i>
              </div>
              <h3>Check Eligibilty</h3>
              <p>
                Use our proprietary algorithms to find out if you're eligible
                for a loan at the best rate.
              </p>
              <Link href="/checkEligibility">
                <a className="link-btn">Check Now</a>
              </Link>
              <div className="back-icon">
                <i className="flaticon-shield-1"></i>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-sm-6 col-md-6">
            <div className="features-box">
              <div className="icon">
                <i className="flaticon-world"></i>
              </div>
              <h3>SOP Review</h3>
              <p>
                Get your SOP reviewed by us for Free and get the admit in your dream college.
              </p>
              <Link href="/coming-soon">
                <a className="link-btn">Check Now</a>
              </Link>
              <div className="back-icon">
                <i className="flaticon-world"></i>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-sm-6 col-md-6">
            <div className="features-box">
              <div className="icon">
                <i className="flaticon-world"></i>
              </div>
              <h3>Apply for Scholarship</h3>
              <p>
                Apply for Scholarship.
              </p>
              <Link href="/coming-soon">
                <a className="link-btn">Check Now</a>
              </Link>
              <div className="back-icon">
                <i className="flaticon-world"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Features;
