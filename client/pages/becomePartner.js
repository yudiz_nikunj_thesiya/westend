import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import baseUrl from "../utils/baseUrl";
import Axios from "axios";
import Notifier from "../utils/Notifier";
import { handleLogin } from "../utils/auth";
import Link from "next/link";
import { useRouter } from "next/router";
import PhoneInput from "../components/CustomPhoneInput.js";

export default function Index(props) {
  const Router = useRouter();

  const [isloading, setisloading] = React.useState(false);
  const [loginState, setLoginState] = useState(false);
  const login_formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: LOGIN_YUP,
    onSubmit: async (values) => {
      setisloading(true);

      try {
        let response = await Axios.post(`${baseUrl}/auth/login`, values);
        setisloading(false);
        props.loginToggle();

        const { token, user } = response.data.data;

        let path;
        if (user.role === "partner") {
          path = "/student/dashboard";
        } else if (user.role === "admin") {
          path = "/admin/dashboard";
        } else {
          Notifier("Student are not allowed to login ", "error");
          return;
        }

        handleLogin(token, user, path);
        // setModal(false);
        Notifier(response.data.message, "success");
      } catch (err) {
        setisloading(false);

        Notifier(err.response.data.message, "error");
      }
    },
  });
  const signup_formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",

      email: "",
      phone: "",
      address: "",
      website: "",
      reason: "",
      company_name: "",
    },
    validationSchema: SIGNUP_YUP,
    onSubmit: async (values) => {
      setisloading(true);
      try {
        let response = await Axios.post(`${baseUrl}/partner/signup`, values);
        setisloading(false);
        if (response.data.data) {
          props.loginToggle();
          Notifier(response.data.message, "success");
        } else {
          Notifier(response.data.message, "error");
        }
      } catch (err) {
        Notifier(err.response.data.message, "error");
        setisloading(false);
      }
    },
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "phone" || name === "loanAmount") {
      signup_formik.setFieldValue(name, value.replace(/\D/g, ""));
    } else {
      signup_formik.setFieldValue(name, value);
    }
  };
  const onNumberChange = (value) => {
    signup_formik.setFieldValue("phone", value);
  };
  return (
    <div>
      <div className="loginContent">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 order-md-2 partner"></div>
            <div className="col-md-12 col-lg-6 col-sm-12 col-xs-12 loginContents">
              <div className="row justify-loginContent-center">
                <div className="col-md-12">
                  <div className="col-lg-12 text-center pb-2">
                    <div className="mb-4">
                      <h3>Become Our Partner</h3>
                    </div>
                  </div>
                  {!loginState ? (
                    <>
                      <form onSubmit={signup_formik.handleSubmit}>
                        <div className="flex no-wrap -mx-3 mb-2">
                          <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            {/* <label htmlFor="username">Username</label> */}
                            <input
                              type="text"
                              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                              placeholder="First Name"
                              id="firstName"
                              name="firstName"
                              onChange={signup_formik.handleChange}
                              value={signup_formik.values.firstName}
                            />
                            <div className=" pt-1">
                              {signup_formik.touched.firstName &&
                              signup_formik.errors.firstName ? (
                                <div className="formikError">
                                  {signup_formik.errors.firstName}
                                </div>
                              ) : null}
                            </div>
                          </div>
                          <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            {/* <label htmlFor="username">Username</label> */}
                            <input
                              type="text"
                              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                              placeholder="Last Name"
                              id="lastName"
                              name="lastName"
                              onChange={signup_formik.handleChange}
                              value={signup_formik.values.lastName}
                            />
                            <div className=" pt-1">
                              {signup_formik.touched.lastName &&
                              signup_formik.errors.lastName ? (
                                <div className="formikError">
                                  {signup_formik.errors.lastName}
                                </div>
                              ) : null}
                            </div>
                          </div>
                        </div>
                        <div className="flex no-wrap -mx-3 mb-2">
                          <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            {/* <label htmlFor="password">Password</label> */}
                            <input
                              type="email"
                              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                              placeholder="Email"
                              id="email"
                              name="email"
                              onChange={signup_formik.handleChange}
                              value={signup_formik.values.email}
                            />
                            <div className=" pt-1">
                              {signup_formik.touched.email &&
                              signup_formik.errors.email ? (
                                <div className="formikError">
                                  {signup_formik.errors.email}
                                </div>
                              ) : null}
                            </div>
                          </div>
                          <div className="w-full  md:w-1/2  px-3 mb-6 md:mb-0">
                            {/* <label htmlFor="password">Password</label> */}
                            <PhoneInput
                              value={signup_formik.values.phone}
                              onChange={onNumberChange}
                            />
                            <div className=" pt-1">
                              {signup_formik.touched.phone &&
                              signup_formik.errors.phone ? (
                                <div className="formikError">
                                  {signup_formik.errors.phone}
                                </div>
                              ) : null}
                            </div>
                          </div>
                        </div>

                        <div className="flex no-wrap -mx-3 mb-2">
                          <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            {/* <label htmlFor="username">Username</label> */}
                            <input
                              type="text"
                              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                              placeholder="Company Name"
                              id="company_name"
                              name="company_name"
                              onChange={signup_formik.handleChange}
                              value={signup_formik.values.company_name}
                            />
                            <div className=" pt-1">
                              {signup_formik.touched.company_name &&
                              signup_formik.errors.company_name ? (
                                <div className="formikError">
                                  {signup_formik.errors.company_name}
                                </div>
                              ) : null}
                            </div>
                          </div>
                          <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            {/* <label htmlFor="username">Username</label> */}
                            <input
                              type="text"
                              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                              placeholder="Company Address"
                              id="address"
                              name="address"
                              onChange={signup_formik.handleChange}
                              value={signup_formik.values.address}
                            />
                            <div className=" pt-1">
                              {signup_formik.touched.address &&
                              signup_formik.errors.address ? (
                                <div className="formikError">
                                  {signup_formik.errors.address}
                                </div>
                              ) : null}
                            </div>
                          </div>
                        </div>
                        <div className="flex no-wrap -mx-3 mb-2">
                          <div className="w-full   px-3 mb-6 md:mb-0">
                            {/* <label htmlFor="password">Password</label> */}
                            <input
                              type="text"
                              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                              placeholder="Website"
                              id="website"
                              name="website"
                              onChange={signup_formik.handleChange}
                              value={signup_formik.values.website}
                            />
                            <div className=" pt-1">
                              {signup_formik.touched.website &&
                              signup_formik.errors.website ? (
                                <div className="formikError">
                                  {signup_formik.errors.website}
                                </div>
                              ) : null}
                            </div>
                          </div>
                        </div>
                        <div className="flex no-wrap -mx-3 mb-2">
                          <div className="w-full  px-3 mb-6 md:mb-0">
                            <textarea
                              id="noter-text-area"
                              placeholder="Why would you like to partner with WESTEND?"
                              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                              name="reason"
                              onChange={signup_formik.handleChange}
                              value={signup_formik.values.reason}
                            />
                          </div>
                        </div>
                        <button type="submit" className="default-btn btn-block">
                          {isloading ? "Signing up..." : "Partner Sign Up"}{" "}
                          <span></span>
                        </button>
                      </form>
                    </>
                  ) : (
                    <form onSubmit={login_formik.handleSubmit}>
                      <div className="form-group first">
                        {/* <label htmlFor="username">Username</label> */}
                        <input
                          type="email"
                          className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                          placeholder="Email"
                          id="email"
                          name="email"
                          onChange={login_formik.handleChange}
                          value={login_formik.values.email}
                        />
                        <div className="text-danger pt-1">
                          {login_formik.touched.email &&
                          login_formik.errors.email ? (
                            <div>{login_formik.errors.email}</div>
                          ) : null}
                        </div>
                      </div>
                      <div className="form-group last mb-4">
                        <input
                          type="password"
                          className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                          placeholder="Password"
                          id="password"
                          name="password"
                          onChange={login_formik.handleChange}
                          value={login_formik.values.password}
                        />
                        <div className="text-danger pt-1">
                          {login_formik.touched.password &&
                          login_formik.errors.password ? (
                            <div>{login_formik.errors.password}</div>
                          ) : null}
                        </div>
                      </div>
                      <button
                        type="submit"
                        disabled={isloading}
                        //   className="btn text-white btn-block "
                        className="no-icon default-btn btn-block mb-2"
                      >
                        {isloading ? "Logging In.." : "Login"}
                        <span></span>
                      </button>
                    </form>
                  )}
                  <div className="col-lg-12 text-center mb-2 mt-2">
                    By singing up, you agree to{" "}
                    <a
                      onClick={() => {
                        props.loginToggle();
                        Router.push("/privacy-policy");
                      }}
                      href="#"
                    >
                      our terms and privacy policy.
                    </a>
                  </div>
                  <div className="otpDiv">
                    <button
                      type="button"
                      onClick={() => {
                        setLoginState(!loginState);
                      }}
                      className="no-icon default-btn btn-block"
                    >
                      {loginState ? "Partner Sign Up" : "Login"}{" "}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const SIGNUP_YUP = Yup.object({
  firstName: Yup.string().required("Required"),
  lastName: Yup.string().required("Required"),
  email: Yup.string().email("Invalid email address").required("Required"),
  phone: Yup.string()
    .min(10, "Must 10 characters long")
    // .max(15, "Must be equal to 10 characters ")
    .required("Required"),
  address: Yup.string().required("Required"),
  company_name: Yup.string().required("Required"),
});
const LOGIN_YUP = Yup.object({
  email: Yup.string().email("Invalid email address").required("Required"),
  password: Yup.string().max(255).required("Required"),
});
