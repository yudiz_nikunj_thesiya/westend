import React, { useState } from "react";

import { useFormik } from "formik";
import * as Yup from "yup";
import baseUrl from "../utils/baseUrl";
import { useForm } from "react-hook-form";
import axios from "axios";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import AutoComplete from "../components/AutoComplete/Index.js";
import DatePicker from "../components/DatePicker";
import TimePicker from "../components/TimePicker";
import PhoneInput from "../components/CustomPhoneInput.js";

import {
  countries,
  decision,
  universities,
  educations,
  departments,
  StatusList,
} from "../utils/Globals";
const MySwal = withReactContent(Swal);
// Form initial state
const alertContent = () => {
  MySwal.fire({
    title: "Congratulations!",
    text: "Your request was successfully sent. We will get back to you soon",
    icon: "success",
    timer: 5000,
    timerProgressBar: true,
    showConfirmButton: false,
  });
};
export default function Index(props) {
  const [loading, setLoading] = useState(false);
  const formik = useFormik({
    initialValues: {
      name: "",
      phone: "",
      email: "",
      time: "00:00",
      date: "",
      country: "",
      amount: "",
    },
    validationSchema: Detail_YUP,
    onSubmit: async (values) => {
      setLoading(true);
      try {
        const url = `${baseUrl}/auth/requestCalllback`;
        await axios.post(url, values);
        setLoading(false);
        formik.handleReset();
        props.toggle();
        alertContent();
      } catch (error) {
        setLoading(false);

        console.log(error);
      }
    },
  });
  const handleChange = (name, value) => {
    formik.setFieldValue(name, value);
  };

  const handleNumeric = (e) => {
    const { name, value } = e.target;
    formik.setFieldValue(name, value.replace(/\D/g, ""));
  };
  const onNumberChange = (value) => {
    formik.setFieldValue("phone", value);
  };
  return (
    <div>
      <div className="loginContent">
        <div className="container">
          <div className="row">
            <div className="col-lg-5 order-md-2 imgSection3">
              <div></div>
            </div>
            <div className="col-md-12 col-lg-7 col-sm-12 col-xs-12 loginContents2">
              <div className="row justify-loginContent-center">
                <div className="col-md-12">
                  <div className="col-lg-12 text-center pb-2">
                    <div className="mb-4">
                      <h3>Request Callback</h3>
                      <h6>
                        Please fill in your Information. We will contact you soon.
                      </h6>
                    </div>
                  </div>
                  <>
                    <form>
                      <div className="flex  -mx-3 mb-3">
                        <div className="w-full md:w-1/2  pl-3 mb-6 md:mb-0">
                          {/* <label htmlFor="username">Username</label> */}
                          <input
                            type="text"
                            className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-0 leading-tight focus:outline-none focus:bg-white"
                            placeholder="Your Name"
                            onChange={formik.handleChange}
                            value={formik.values.name}
                            name="name"
                          />
                          <div className="text-danger pt-1">
                            {formik.touched.name && formik.errors.name ? (
                              <div>{formik.errors.name}</div>
                            ) : null}
                          </div>
                        </div>
                        <div className="w-full md:w-1/2  px-3 mb-6 md:mb-0">
                          {/* <label htmlFor="password">Password</label> */}
                          <input
                            type="email"
                            className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-0 leading-tight focus:outline-none focus:bg-white"
                            placeholder="Your email address"
                            onChange={formik.handleChange}
                            name="email"
                            value={formik.values.email}
                          />
                          <div className="text-danger pt-1">
                            {formik.touched.email && formik.errors.email ? (
                              <div>{formik.errors.email}</div>
                            ) : null}
                          </div>
                        </div>
                      </div>

                      <div className="flex  -mx-3 mb-3">
                        <div className="w-full md:w-1/2  pl-3 mb-6 md:mb-0">
                          {/* <label htmlFor="username">Username</label> */}
                          <PhoneInput
                            value={formik.values.phone}
                            onChange={onNumberChange}
                          />
                          <div className="text-danger pt-1">
                            {formik.touched.phone && formik.errors.phone ? (
                              <div>{formik.errors.phone}</div>
                            ) : null}
                          </div>
                        </div>
                        <div className="w-full md:w-1/2  px-3 mb-6 md:mb-0">
                          {/* <label htmlFor="password">Password</label> */}
                          <input
                            type="text"
                            className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-0 leading-tight focus:outline-none focus:bg-white"
                            placeholder="Loan Amount(INR)"
                            onChange={handleNumeric}
                            name="amount"
                            value={formik.values.amount}
                          />
                          <div className="text-danger pt-1">
                            {formik.touched.amount && formik.errors.amount ? (
                              <div>{formik.errors.amount}</div>
                            ) : null}
                          </div>
                        </div>
                      </div>
                      <div className="flex  -mx-3 mb-3">
                        <div className="w-full  px-3 mb-6 md:mb-0">
                          <AutoComplete
                            formik={formik}
                            data={countries}
                            name="country"
                            label="Choose a country"
                            country={true}
                            onChange={handleChange}
                            value={formik.values.country}
                          />
                          <div className="text-danger pt-1">
                            {formik.touched.country && formik.errors.country ? (
                              <div>{formik.errors.country}</div>
                            ) : null}
                          </div>
                        </div>
                      </div>
                      <div className="flex  -mx-3 mb-3">
                        <div className="w-full md:w-1/2  pl-3 mb-6 md:mb-0">
                          <DatePicker
                            label="Appointment Date"
                            name="date"
                            onChange={formik.handleChange}
                            value={formik.values.date}
                          />
                          <div className="text-danger pt-1">
                            {formik.touched.date && formik.errors.date ? (
                              <div>{formik.errors.date}</div>
                            ) : null}
                          </div>
                        </div>
                        <div className="w-full md:w-1/2  px-3 mb-6 md:mb-0">
                          <TimePicker
                            label="Appointment Time"
                            name="time"
                            onChange={formik.handleChange}
                            value={formik.values.time}
                            ampm={true}
                          />
                          <div className="text-danger pt-1">
                            {formik.touched.time && formik.errors.time ? (
                              <div>{formik.errors.time}</div>
                            ) : null}
                          </div>
                        </div>
                      </div>

                      <div className="mt-2">
                        <div className="col-lg-12 pb-1 pt-1 text-center">
                          <button
                            type="submit"
                            disabled={loading}
                            onClick={formik.handleSubmit}
                            //   className="btn text-white btn-block "
                            className="default-btn btn-block"
                          >
                            {loading ? "SUBMITTING.." : "SUBMIT"}
                            <span></span>
                          </button>
                        </div>
                      </div>
                    </form>
                  </>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
const Detail_YUP = Yup.object({
  name: Yup.string()
    .max(100, "Must be less than 100 characters")
    .required("Required"),
  country: Yup.string()
    .max(100, "Must be less than 100 characters")
    .required("Required"),
  date: Yup.string().required("Required"),
  time: Yup.string().required("Required"),
  phone: Yup.string()
    .min(10, "Must 10 characters long")
    // .max(15, "Must be equal to 10 characters ")
    .required("Required"),
  amount: Yup.number().required("Required"),

  email: Yup.string().email("Invalid email address").required("Required"),
});
