import React, { useState } from "react";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemPanel,
  AccordionItemButton,
} from "react-accessible-accordion";
import { resetIdCounter, Tab, Tabs, TabList, TabPanel } from "react-tabs";
import SubscribeForm from "../../components/Common/SubscribeForm";
import ImageCard from "../../components/ImageCard";
import { useRouter } from "next/router";

resetIdCounter();

const Faq = (props) => {
  const router = useRouter();
  const [tabIndex, setTabIndex] = useState(1);
  React.useEffect(() => {
    if (props.param) {
      let param = Number(props.param)
      setTabIndex(param)
    }
  }, [props])
  return (
    <React.Fragment>
      <div className="faq-area bg-fff8f8 ">
        <div className="container">
          <div className="section-title">
            <span className="sub-title">Study Anywhere</span>
            <h2>Countries we support</h2>
          </div>
          <div className="tab faq-accordion-tab">
            <Tabs selectedIndex={tabIndex} onSelect={index => setTabIndex(index)}>
              <TabList>
                <Tab>
                  <ImageCard src="/images/partner/australia.png" alt="image" />
                  <div>Australia</div>
                </Tab>
                <Tab>
                  <ImageCard src="/images/partner/united-states.png" alt="image" />
                  <div>United States</div>
                </Tab>
                <Tab>
                  <ImageCard src="/images/partner/uk.png" alt="image" />
                  <div>UK</div>
                </Tab>
                <Tab>
                  <ImageCard src="/images/partner/canada.png" alt="image" />
                  <div>Canada</div>
                </Tab>
                <Tab>
                  <ImageCard src="/images/partner/new-zealand.png" alt="image" />
                  <div>New Zealand</div>
                </Tab>
                <Tab>
                  <ImageCard src="/images/partner/germany.png" alt="image" />
                  <div>Germany</div>
                </Tab>
              </TabList>
              <TabPanel>
                <div className="faq-accordion">
                  <Accordion allowZeroExpanded preExpanded={["a"]}>
                    <AccordionItem uuid="a">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          What is the requirement of Finance for an Australian Visa?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          Aspirants or applicants required to have adequate amount of money to pay for fees of course, and living and travel costs for student and accompanying family members while a student studying in Australia. Student can get visa only if they have required money to show in their Account. If student failed to show the required financial capacity with the application of Australian student Visa or while the officer asks then it is certain that student visa application would be refused.
                        </p>
                        <p>
                          Applicant can show this by providing evidence of any one of the following:
                        </p>
                        <p>
                          <ul class="features-list">
                            <li><i class="bx bx-badge-check"></i> Option 1: Evidence of money to cover travel to Australia and 12 months livelihood expenses, course fee of 12 months and (for school aged students) schooling expenses for student and accompanying members of family.</li>
                            <li><i class="bx bx-badge-check"></i> Option 2: Evidence that you can accomplish the requirement of annual income.</li>
                          </ul>


                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="b">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Funds option for 12 Months
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          Many students show their financial capacity by providing evidence of funds for 12 months of course. You are required to have funds to cover travel to Australia and 12 months livelihood expenses, course fee of 12 months and (for school aged students) schooling expenses for student and accompanying members of family.
                          <br />
                          <br />
                          Expense for 12 months is: <br />
                          <ul class="features-list">
                            <li> <i class="bx bx-badge-check"></i>Student: AUD 20,209</li>
                            <li> <i class="bx bx-badge-check"></i>Partner/Spouse: AUD 7,100</li>
                            <li> <i class="bx bx-badge-check"></i>Child: AUD 3,040</li>
                          </ul>
                          <b>Travel cost:</b> Approximately 2,000 A$
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="c">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Cost of Schooling for your children
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          Your school-aged children are added in your student visa application, the cost of schooling is at least AUD 8,000 per year for individual child will be needed to add in the amount of funds required. It is your responsibility to research for school cost which vary between states territories and schools in Australia.
                          <br />
                          <b>Note:</b> Eliminate the amount of prepaid tuition fees to get the real amounts of fund required for the Australian student Visa.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="d">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Authenticate Access to funds
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You along with your family members must be able to access the amounts demonstrated while you are in Australia. To consider the authenticity of funds, DIBP will take following notes into account:
                          <br />
                          <ul className="features-list">
                            <li> <i class="bx bx-badge-check"></i>The kind of relation between you and the fund provider, wherever it is applicable.</li>
                            <li> <i class="bx bx-badge-check"></i>Your income, property and employment or of the person who is providing funds.</li>
                          </ul>
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="e">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          What are the evidence acceptable to accomplish the financial requirements for Australian student visa?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          <ul className="features-list">
                            <li><i class="bx bx-badge-check"></i>Money deposited to the financial institution i.e. FD, Bank Balance etc.</li>
                            <li><i class="bx bx-badge-check"></i>Loan taken from Financial Institution.</li>
                            <li><i class="bx bx-badge-check"></i>If your money is in Post office, PF/PPF, Co-operative bank etc then that fund should be transferred to the authenticated bank and keep evidence of source of fund (Post office, PF/PPF, Co-operative Bank etc…)</li>
                            <li><i class="bx bx-badge-check"></i>Loan taken from Government</li>
                            <li><i class="bx bx-badge-check"></i>Financial support or scholarship</li>
                            <li><i class="bx bx-badge-check"></i>Your visa history and of the person who is providing you funds.</li>
                          </ul>
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="f">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Cost of Education in Australia
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          <ul className="features-list">
                            <li><i class="bx bx-badge-check"></i>To study in Australia cost depends upon the level of study and the institution you choose.</li>
                            <li><i class="bx bx-badge-check"></i>As you are going to be an international student for that country therefore your tuition fees are paid before you start your study.</li>
                            <li><i class="bx bx-badge-check"></i>You might have to pay additional charges for your course which includes course materials and access to facilities of institution.</li>
                            <br />
                            <li><i class="bx bx-badge-check"></i>Undergraduate bachelor degree varies between AUD$15,000 to AUD$33,000*</li>
                            <li><i class="bx bx-badge-check"></i>Postgraduate Master’s Degree varies between AUD$20,000 to AUD$40,000*</li>
                            <li><i class="bx bx-badge-check"></i>Approximately Cost may vary from Rs.20 lacs to Rs.40 lacs as per Indian currency.</li>
                          </ul>
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="g">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          More Information for student loan
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          Student struggles while getting education loans. Major issues face by students include:
                          <br />
                          <ul className="features-list">
                            <li><i class="bx bx-badge-check"></i>For students who aspire to visit the USA, UK, Canada, Australia etc there are plenty of activities and formalities which take away the precious time. Going from one branch to another to secure education loan become a challenging task.</li>
                            <li><i class="bx bx-badge-check"></i>In terms of loan there should be a flexibility that means loans need to be customized as per the requirements of students and their respective courses.</li>
                            <li><i class="bx bx-badge-check"></i>Accessibility to get the required margin money (part of fees paid by students or their parents which is excluded in loan.)</li>
                            <li><i class="bx bx-badge-check"></i>Loans required to pay is more than the collateral security</li>
                            <li><i class="bx bx-badge-check"></i>Authenticity of the institution for which student has applied. (this is quite challenging in today’s world of changing education as decision lead to achieve the goal.)</li>
                            <li><i class="bx bx-badge-check"></i>Ability to get digital access of loan account to enable transaction processing inclusive of electronic payments.</li>
                          </ul>
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>


                  </Accordion>
                </div>
              </TabPanel>

              <TabPanel>
                <div className="faq-accordion">
                  <Accordion allowZeroExpanded preExpanded={["a"]}>
                    <AccordionItem uuid="a">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Where should I study abroad?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="b">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How can I contact a school directly?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="c">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a study abroad program on Westend?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="d">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a school where I want to study?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="e">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Am I eligible for admission?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>
                  </Accordion>
           
                </div>
              </TabPanel>

              <TabPanel>
                <div className="faq-accordion">
                  <Accordion allowZeroExpanded preExpanded={["a"]}>
                    <AccordionItem uuid="a">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Money need to demonstrate
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          For UK Visas and Immigration, you need to show the following level of funds:
                        </p>
                        <h5> Financial Requirements:</h5>
                        <ul className="features-list">
                          <li><span><b>
                            Tuition Fees:</b> If you have chosen a course which is of less than a year then you need to show 1st year fees or full course fees.</span></li>
                          <li><span><b>Living Expenses:</b> t is required for all the students to show the 1050 Euros per month for the duration of their course or 9 months, whichever is shorter in comparison towards the Tier4 visa maintenance requirement.</span></li>
                          <li><span>Approximately cost may vary from Rs.15 lacs to Rs.30 lacs as per Indian currency.</span></li>

                        </ul>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="b">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Dependent
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          “If you are living with your dependents in UK then you need to demonstrate a minimum of 680 Euros per month for each dependent. You will actually need an evidence to prove that you have 6,120 Euros for individual dependents for courses of the duration of the nine months.
                        </p>

                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="c">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Financial Evidence
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          Immigration is very stern about the financial documents. You will have to show that you have adequate money for your visa. The money required to show can be in your bank account, in parent’s account or in a joint account with your name. If you are depending on the funds in your parent’s bank account, then you will mandatorily have to show following documents:
                        </p>
                        <ul class="features-list">
                          <li><i class="bx bx-badge-check"></i> Parent’s letter confirming that you are their child and that they will be paying all of your expenses</li>
                          <li><i class="bx bx-badge-check"></i> Your original birth certificates</li>
                          <li><i class="bx bx-badge-check"></i> Financial documents of your parents.</li>

                        </ul>
                        <p>
                          If you are depending on the funds in family member’s account other than your parents then you will have to transfer that amount in your bank account or joint account with your name.
                        </p>

                        <p>Financial proof from any other family member’s bank account other than your parents is not accepted by the Home Office except from an official financial or government sponsors. The UKVI define an official sponsor as follows:</p>

                        <ul class="features-list">
                          <li><i class="bx bx-badge-check"></i> Government of your country or UK government</li>
                          <li><i class="bx bx-badge-check"></i> British Council</li>
                          <li><i class="bx bx-badge-check"></i> Any international Organizations</li>
                          <li><i class="bx bx-badge-check"></i> Any international company</li>

                          <li><i class="bx bx-badge-check"></i> Any university</li>


                        </ul>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="d">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Required Documents
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          The home office is very strict in terms of requirements and these documents must be produced when requested:
                        </p>
                        <ul class="features-list">
                          <li><i class="bx bx-badge-check"></i> Bank statements</li>
                          <li><i class="bx bx-badge-check"></i> Building society passbooks</li>
                          <li><i class="bx bx-badge-check"></i> Letter from the bank</li>
                          <li><i class="bx bx-badge-check"></i> Overseas account statements/letters</li>

                          <li><i class="bx bx-badge-check"></i> Letter giving confirmation of loan. The loan is required to be procured by national governments, their regional government or state government, a company of government sponsored student loan or portion of an academic or education loan scheme.</li>


                        </ul>
                        <p>If any of your documents are not in English then include translations that meet the specific requirements for translations.</p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="e">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          The 28-Day Rule
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          The funds you are using for the maintenance must be in your account for a consecutive period of 28 days. Your application might be refused if you are not accomplishing the Home Office requirements or if this amount drop below the required amount even for a single day. The Bank statement should not be older than 30 days on the date of your application.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>
                  </Accordion>
                </div>
              </TabPanel>

              <TabPanel>
                <div className="faq-accordion">
                  <Accordion allowZeroExpanded preExpanded={["a"]}>
                    <AccordionItem uuid="a">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Student Visa Requirements
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          Canadian student visa requirements include evident of funds to support your expenses.
                          In present scenario the cost is CA$10,000(~US$7,550) for each year of your stay. (CA$11,000/~US$8,300 in case you are applying to study in Quebec).
                        </p>
                        <h5>Different means to prove funding are as follows:</h5>
                        <ul class="features-list">
                          <li><i class="bx bx-badge-check"></i> Evidence of your Canadian Bank Account demonstrating that you have transferred some amount.</li>
                          <li><i class="bx bx-badge-check"></i> Guaranteed Investment Certificate from a Canadian bank.</li>
                          <li><i class="bx bx-badge-check"></i>Evidence of student or education loan from bank.</li>
                          <li><i class="bx bx-badge-check"></i> Your bank statements for the previous four months.</li>

                          <li><i class="bx bx-badge-check"></i> A bank draft which can be converted to Canadian dollars.</li>
                          <li><i class="bx bx-badge-check"></i> A letter from a person school providing you funds.</li>
                          <li><i class="bx bx-badge-check"></i> Evidence of funding paid from, within Canada, in case you have scholarship or you are in a Canada funded educational program.</li>


                        </ul>
                        <p>Fee of tuition is around Rs15 lakh to Rs30 lakh as per Indian currency which may vary between institutions and study programs.</p>
                      </AccordionItemPanel>
                    </AccordionItem>

                  </Accordion>
                </div>
              </TabPanel>

              <TabPanel>
                <div className="faq-accordion">
                  <Accordion allowZeroExpanded preExpanded={["a"]}>
                    <AccordionItem uuid="a">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Documents required
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <ul class="features-list">
                          <li><i class="bx bx-badge-check"></i> Aspirants who belong to nations such as India, Sri Lanka, Bhutan, Nepal and Bangladesh must demonstrate proof that they have enough money for their livelihood expenses during studying in New Zealand, as per the immigration law of New Zealand.</li>
                          <li><i class="bx bx-badge-check"></i>
                            Proof of only certain funds that is the funds including wages slip and corresponding bank statements of previous months which is showing the transfer in to student’s or his/her sponsor’s or bank account of financial sponsor. In order to attain regular income tax returns of last three years should be provided.
                          </li>
                          <li><i class="bx bx-badge-check"></i>Fixed term deposit which is minimum of six months old or a GPOF (General provident fund) or EPF (Employer provident fund) statement held with Reserve Bank of India (RBI).</li>
                          <li><i class="bx bx-badge-check"></i>
                            Funds from different sources such as Kisan credit card loans, gold loans, agriculture income or property sales are acceptable only if the students or their sponsors have held required funds for six months or the source of such funds must be authenticated.</li>

                          <li><i class="bx bx-badge-check"></i> In case a person is willing to study for over a year then they will have to show the proof or payment plan that how they are going to pay for their tuition fees and living expenses for the entire period spent in New Zealand. The evidence of saving and income supporting student’s payment plan must cover the last three years.</li>
                          <li><i class="bx bx-badge-check"></i>Approximately cost may vary from Rs. 15 lacs to Rs. 30 lacs as per Indian currency.</li>
                          <li><i class="bx bx-badge-check"></i>If you are wishing to go to New Zealand to pursue studies, then come to Westend and get the best advice and guidance to file for Visa at any one of our 19 offices located in major cities of India.</li>


                        </ul>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="b">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Where should I study abroad?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="c">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a study abroad program on Westend?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="d">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a school where I want to study?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="e">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Am I eligible for admission?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>
                  </Accordion>
                </div>
              </TabPanel>
              <TabPanel>
                <div className="faq-accordion">
                  <Accordion allowZeroExpanded preExpanded={["a"]}>
                    <AccordionItem uuid="a">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Where should I study abroad?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="b">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How can I contact a school directly?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="c">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a study abroad program on Westend?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="d">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a school where I want to study?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="e">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Am I eligible for admission?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>
                  </Accordion>
           
                </div>
              </TabPanel>
            </Tabs>
          </div>
        </div>
      </div>

      <SubscribeForm />

      {/* <Footer /> */}
    </React.Fragment>
  );
};

Faq.getInitialProps = async ({ Component, query }) => {

  return { param: query.country };
}
export default Faq;
