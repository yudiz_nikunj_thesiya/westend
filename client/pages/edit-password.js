import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import Axios from "axios";
import Notifier from "../utils/Notifier";
import baseUrl from "../utils/baseUrl";
import { getUser, getToken } from "../utils/auth";
const token = getToken();
const loggedInUser = getUser();
const EditPassword = () => {
  const [isloading, setisloading] = React.useState(false);

  const formik = useFormik({
    initialValues: {
      oldPassword: "",
      newPassword: "",
    },
    validationSchema: PASSWORD_YUP,
    onSubmit: async (values) => {
      setisloading(true);
      try {
        let response = await Axios({
          method: "put",
          url: `${baseUrl}/auth/password/${loggedInUser.id}`,
          data: values,
          headers: {
            Authorization: token ? `Bearer ${token}` : "",
          },
        });
        Notifier(response.data.message, "success");
        setisloading(false);
        formik.handleReset();
      } catch (err) {
        Notifier(err.response.data.message, "error");
        setisloading(false);
      }
    },
  });
  return (
    <React.Fragment>
      <div>
        <div className="container">
          <div className="row">
            <div className="col-lg-8 sm:justify-center">
              <h2>Change Password</h2>
            </div>
          </div>
          <div className="col-lg-8 ">
            <div className="border-box">
              <form onSubmit={formik.handleSubmit}>
                <div className="form-group">
                  <label>Old Password</label>
                  <input
                    type="password"
                    name="oldPassword"
                    onChange={formik.handleChange}
                    value={formik.values.oldPassword}
                    className="form-control"
                  />
                  <div className="text-danger pt-1">
                    {formik.touched.oldPassword && formik.errors.oldPassword ? (
                      <div className="formikError">
                        {formik.errors.oldPassword}
                      </div>
                    ) : null}
                  </div>
                </div>

                <div className="form-group">
                  <label>New Password</label>
                  <input
                    type="password"
                    name="newPassword"
                    onChange={formik.handleChange}
                    value={formik.values.newPassword}
                    className="form-control"
                  />
                  <div className="text-danger pt-1">
                    {formik.touched.newPassword && formik.errors.newPassword ? (
                      <div className="formikError">
                        {formik.errors.newPassword}
                      </div>
                    ) : null}
                  </div>
                </div>

                <button type="submit" className="default-btn mt-10">
                  {!isloading ? "Update" : "Updating..."}
                  <span></span>
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>

      {/* <Footer /> */}
    </React.Fragment>
  );
};

export default EditPassword;
const PASSWORD_YUP = Yup.object({
  oldPassword: Yup.string().max(255).required("Required"),
  newPassword: Yup.string().min(6).max(255).required("Required"),
});
