import React from "react";
import Axios from "axios";
import Notifier from "../utils/Notifier";
import baseUrl from "../utils/baseUrl";
import AutoComplete from "../components/AutoComplete/Index.js";
import Button from "../components/Button/index";
import PhoneInput from "../components/CustomPhoneInput.js";

import { useRouter } from "next/router";

import { countries } from "../utils/Globals";
import { useFormik } from "formik";
import * as Yup from "yup";
const Index2 = () => {
  const router = useRouter();

  const [loading, setloading] = React.useState(false);

  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      phone: "",
      email: "",
      university: "",
      country: "",
    },
    validationSchema: Detail_YUP,
    onSubmit: async (values) => {
      try {
        setloading(true);
        let response = await Axios({
          method: "post",
          url: `${baseUrl}/auth/applyScholarship`,
          data: values,
        });
        setloading(false);
        Notifier(response.data.message, "success");

        router.push("/");
      } catch (err) {
        setloading(false);

        if (err.response) {
          Notifier(err.response.data.message, "error");
        }
      }
    },
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "phone") {
      formik.setFieldValue(name, value.replace(/\D/g, ""));
    } else {
      formik.setFieldValue(name, value);
    }
  };
  const onhandleChange = (name, value) => {
    formik.setFieldValue(name, value);
  };
  const onNumberChange = (value) => {
    formik.setFieldValue("phone", value);
  };
  return (
    <>
      <div className="about-area-two ptb-100">
        <div className="container">
          <div className="row ">
            <div className="col-lg-5 col-md-12">
              <div className="about-content-box">
                <h2>Apply For Scholarship</h2>
                <p>
                  In publishing and graphic design, Lorem ipsum is a placeholder
                  text commonly used to demonstrate the visual form of a
                  document or a typeface without relying on meaningful content.
                  Lorem ipsum may be used as a placeholder before final copy is
                  available.
                </p>
                <p>
                  <strong></strong>
                </p>
              </div>
            </div>

            <div className="col-lg-7 col-md-12">
              <div className="about-video-box ">
                <h5>Scholarship Request</h5>
                <form className="w-full ">
                  <div className="flex no-wrap -mx-3 mb-0">
                    <div className="w-full pl-3 mb-6 md:mb-0">
                      <input
                        className="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 mb-2 mt-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        type="text"
                        placeholder="First Name"
                        name="firstName"
                        onChange={handleChange}
                        value={formik.values.firstName}
                      />
                      <div className="text-danger pt-1">
                        {formik.touched.firstName && formik.errors.firstName ? (
                          <div>{formik.errors.firstName}</div>
                        ) : null}
                      </div>
                    </div>
                    <div className="w-full px-3">
                      <input
                        className="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 mb-2 mt-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        type="text"
                        placeholder="Last Name"
                        name="lastName"
                        onChange={handleChange}
                        value={formik.values.lastName}
                      />
                      <div className="text-danger pt-1">
                        {formik.touched.lastName && formik.errors.lastName ? (
                          <div>{formik.errors.lastName}</div>
                        ) : null}
                      </div>
                    </div>
                  </div>
                  <div className="flex no-wrap -mx-3 mb-0">
                    <div className="w-full pl-3 mb-6 mt-1 md:mb-0">
                      <PhoneInput
                        value={formik.values.phone}
                        onChange={onNumberChange}
                      />
                      <div className="text-danger pt-1">
                        {formik.touched.phone && formik.errors.phone ? (
                          <div>{formik.errors.phone}</div>
                        ) : null}
                      </div>
                    </div>
                    <div className="w-full  px-3">
                      <input
                        className="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 mb-2 mt-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="grid-email"
                        type="text"
                        placeholder="Email"
                        name="email"
                        onChange={formik.handleChange}
                        value={formik.values.email}
                      />
                      <div className="text-danger pt-1">
                        {formik.touched.email && formik.errors.email ? (
                          <div>{formik.errors.email}</div>
                        ) : null}
                      </div>
                    </div>
                  </div>

                  <div className="flex no-wrap -mx-3 mb-0">
                    <div className="w-full px-3">
                      <input
                        className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-2 leading-tight focus:outline-none focus:bg-white"
                        type="text"
                        placeholder="University Name"
                        name="university"
                        onChange={handleChange}
                        value={formik.values.university}
                      />

                      <div className="text-danger pt-1">
                        {formik.touched.university &&
                        formik.errors.university ? (
                          <div>{formik.errors.university}</div>
                        ) : null}
                      </div>
                    </div>
                  </div>

                  <div className="flex no-wrap -mx-3 mb-2">
                    <div className="w-full  px-3 mb-6 md:mb-0">
                      <AutoComplete
                        formik={formik}
                        data={countries}
                        name="country"
                        label="Choose a country"
                        country={true}
                        onChange={onhandleChange}
                        value={formik.values.country}
                      />
                      <div className="text-danger pt-1">
                        {formik.touched.country && formik.errors.country ? (
                          <div>{formik.errors.country}</div>
                        ) : null}
                      </div>
                    </div>
                  </div>

                  <div className="flex no-wrap px-3 -mx-3 mb-0">
                    <div className="row">
                      <div className="col-lg-12">
                        <div className="d-flex  pt-2">
                          <Button
                            onClick={formik.handleSubmit}
                            className="no-icon default-btn  "
                            label={loading ? "SUBMITTING" : "Submit"}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
const Detail_YUP = Yup.object({
  firstName: Yup.string()
    .max(100, "Must be less than 100 characters")
    .required("Required"),
  lastName: Yup.string()
    .max(100, "Must be less than 100 characters")
    .required("Required"),
  phone: Yup.string()
    .min(10, "Must 10 characters long")
    // .max(15, "Must be equal to 10 characters ")
    .required("Required"),
  email: Yup.string().email("Invalid email address").required("Required"),

  country: Yup.string().required("Required"),
  university: Yup.string().required("Required"),
});

export default Index2;
