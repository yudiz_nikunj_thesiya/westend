import React from "react";
import PageBanner from "../components/Common/PageBanner";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemPanel,
  AccordionItemButton,
} from "react-accessible-accordion";
import { resetIdCounter, Tab, Tabs, TabList, TabPanel } from "react-tabs";
import SubscribeForm from "../components/Common/SubscribeForm";
import ImageCard from "../components/ImageCard";

resetIdCounter();

const Faq = () => {
  return (
    <React.Fragment>
      <div className="faq-area bg-fff8f8">
        <div className="container">
          <div className="section-title">
            <span className="sub-title">Study anywhere</span>
            <h2>Countries</h2>
          </div>
          <div className="tab faq-accordion-tab">
            <Tabs>
              <TabList>
                <Tab>
                <ImageCard src="/images/partner/united-states.png" alt="image" />
                </Tab>
                <Tab>
                <ImageCard src="/images/partner/australia.png" alt="image" />
                </Tab>
                <Tab>
                <ImageCard src="/images/partner/uk.png" alt="image" />
                </Tab>
                <Tab>
                <ImageCard src="/images/partner/canada.png" alt="image" />
                </Tab>
                <Tab>
                <ImageCard src="/images/partner/new-zealand.png" alt="image" />
                </Tab>
                <Tab>
                <ImageCard src="/images/partner/germany.png" alt="image" />
                </Tab>
              </TabList>

              <TabPanel>
                <div className="faq-accordion">
                  <Accordion allowZeroExpanded preExpanded={["a"]}>
                    <AccordionItem uuid="a">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How can I contact a school directly?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="b">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Where should I study abroad?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="c">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a study abroad program on eDemy.com?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="d">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a school where I want to study?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="e">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Am I eligible for admission?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>
                  </Accordion>
                </div>
              </TabPanel>

              <TabPanel>
                <div className="faq-accordion">
                  <Accordion allowZeroExpanded preExpanded={["a"]}>
                    <AccordionItem uuid="a">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Where should I study abroad?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="b">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How can I contact a school directly?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="c">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a study abroad program on eDemy.com?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="d">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a school where I want to study?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="e">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Am I eligible for admission?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>
                  </Accordion>
                </div>
              </TabPanel>

              <TabPanel>
                <div className="faq-accordion">
                  <Accordion allowZeroExpanded preExpanded={["a"]}>
                    <AccordionItem uuid="a">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a study abroad program on eDemy.com?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="b">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How can I contact a school directly?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="c">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Where should I study abroad?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="d">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a school where I want to study?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="e">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Am I eligible for admission?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>
                  </Accordion>
                </div>
              </TabPanel>

              <TabPanel>
                <div className="faq-accordion">
                  <Accordion allowZeroExpanded preExpanded={["a"]}>
                    <AccordionItem uuid="a">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Am I eligible for admission?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="b">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How can I contact a school directly?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="c">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Where should I study abroad?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="d">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a study abroad program on eDemy.com?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="e">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a school where I want to study?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>
                  </Accordion>
                </div>
              </TabPanel>

              <TabPanel>
                <div className="faq-accordion">
                  <Accordion allowZeroExpanded preExpanded={["a"]}>
                    <AccordionItem uuid="a">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How can I contact a school directly?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="b">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Where should I study abroad?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="c">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a study abroad program on eDemy.com?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="d">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a school where I want to study?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="e">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Am I eligible for admission?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>
                  </Accordion>
                </div>
              </TabPanel>
            </Tabs>
          </div>
        </div>
      </div>

      <SubscribeForm />

      {/* <Footer /> */}
    </React.Fragment>
  );
};

export default Faq;
