import React from "react";
import Navbar from "../components/_App/Navbar";
import PageBanner from "../components/Common/PageBanner";
// import Footer from '../components/_App/Footer';
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemPanel,
  AccordionItemButton,
} from "react-accessible-accordion";
import { resetIdCounter, Tab, Tabs, TabList, TabPanel } from "react-tabs";
import SubscribeForm from "../components/Common/SubscribeForm";
import Link from "@/utils/ActiveLink";

resetIdCounter();

const Faq = () => {
  return (
    <React.Fragment>
      {/* <Navbar /> */}
      <div className="courses-area  bg-f5f7fa">
        <div className="container">
          <div className="section-title">
            <span className="sub-title">Go at your own pace</span>
            <h2>The World’s Largest Selection Of Courses</h2>
          </div>
          <div className="col-lg-10 offset-md-2">
            <div className="courseDiv">
              <div className="row ">
                {[1, 2, 3, 4].map((index) => (
                  <div key={index} className="col-lg-5 countryCard col-md-12">
                    <div className="single-courses-item">
                      <div className="row align-items-center">
                        <div className="col-lg-4 col-md-4 ">
                          <div className="courses-image text-center">
                            <img
                              src="/images/courses-small/courses-small1.jpg"
                              alt="image"
                            />
                          </div>
                        </div>

                        <div className="col-lg-8 col-md-8">
                          <div className="courses-content">
                            <div className="d-flex">
                              <div className="div1">
                                <p>Processing Fee</p>
                              </div>
                              <div className="div2">
                                <p>1.1% - 1.5%</p>
                              </div>
                            </div>
                            <div className="d-flex ">
                              <div className="div1">
                                <p>Processing Fee</p>
                              </div>
                              <div className="div2">
                                <p>1.1% - 1.5%</p>
                              </div>
                            </div>
                            <div className="d-flex ">
                              <div className="div1">
                                <p>Processing Fee</p>
                              </div>
                              <div className="div2">
                                <p>1.1% - 1.5%</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>

        <div className="shape16">
          <img src="/images/shape15.png" alt="image" />
        </div>
      </div>
      <div className="faq-area ptb-100">
        <div className="container">
          <div className="tab faq-accordion-tab">
            <Tabs>
              <TabList>
                <Tab>
                  <i className="bx bx-flag"></i> <span>Getting Started</span>
                </Tab>
                <Tab>
                  <i className="bx bxs-badge-dollar"></i>{" "}
                  <span>Pricing & Planes</span>
                </Tab>
                <Tab>
                  <i className="bx bx-shopping-bag"></i>{" "}
                  <span>Sales Question</span>
                </Tab>
                <Tab>
                  <i className="bx bx-book-open"></i> <span>Usage Guides</span>
                </Tab>
                <Tab>
                  <i className="bx bx-info-circle"></i>{" "}
                  <span>General Guide</span>
                </Tab>
              </TabList>

              <TabPanel>
                <div className="faq-accordion">
                  <Accordion allowZeroExpanded preExpanded={["a"]}>
                    <AccordionItem uuid="a">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How can I contact a school directly?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="b">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Where should I study abroad?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="c">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a study abroad program on eDemy.com?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="d">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a school where I want to study?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="e">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Am I eligible for admission?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>
                  </Accordion>
                </div>
              </TabPanel>

              <TabPanel>
                <div className="faq-accordion">
                  <Accordion allowZeroExpanded preExpanded={["a"]}>
                    <AccordionItem uuid="a">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Where should I study abroad?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="b">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How can I contact a school directly?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="c">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a study abroad program on eDemy.com?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="d">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a school where I want to study?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="e">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Am I eligible for admission?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>
                  </Accordion>
                </div>
              </TabPanel>

              <TabPanel>
                <div className="faq-accordion">
                  <Accordion allowZeroExpanded preExpanded={["a"]}>
                    <AccordionItem uuid="a">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a study abroad program on eDemy.com?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="b">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How can I contact a school directly?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="c">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Where should I study abroad?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="d">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a school where I want to study?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="e">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Am I eligible for admission?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>
                  </Accordion>
                </div>
              </TabPanel>

              <TabPanel>
                <div className="faq-accordion">
                  <Accordion allowZeroExpanded preExpanded={["a"]}>
                    <AccordionItem uuid="a">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Am I eligible for admission?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="b">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How can I contact a school directly?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="c">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Where should I study abroad?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="d">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a study abroad program on eDemy.com?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="e">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a school where I want to study?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>
                  </Accordion>
                </div>
              </TabPanel>

              <TabPanel>
                <div className="faq-accordion">
                  <Accordion allowZeroExpanded preExpanded={["a"]}>
                    <AccordionItem uuid="a">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How can I contact a school directly?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="b">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Where should I study abroad?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="c">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a study abroad program on eDemy.com?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="d">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          How do I find a school where I want to study?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>

                    <AccordionItem uuid="e">
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          Am I eligible for admission?
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel>
                        <p>
                          You can contact a school by filling out a{" "}
                          <a href="contact.html">“Contact Us”</a> form. This
                          form can be found to the right of both the institute
                          and education program profiles and also at the bottom
                          of these profiles.
                        </p>
                      </AccordionItemPanel>
                    </AccordionItem>
                  </Accordion>
                </div>
              </TabPanel>
            </Tabs>
          </div>
        </div>
      </div>

      <SubscribeForm />
    </React.Fragment>
  );
};

export default Faq;
