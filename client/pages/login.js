import React, { useState } from "react";
import PhoneInput from "../components/CustomPhoneInput.js";
import { useFormik } from "formik";
import * as Yup from "yup";
import baseUrl from "../utils/baseUrl";
import { useRouter } from "next/router";
import Axios from "axios";
import Notifier from "../utils/Notifier";
import { handleLogin } from "../utils/auth";
import Signup from "./signup";
import { signIn } from "next-auth/client";
import Link from "@/utils/ActiveLink";
import Modal from "../components/Modals/index";
import OtpLogin from "./otp-login";
import VerifyOtpLogin from "./verifyOtp";
import ForgotPassword from "./forgot-password";
import SuccessMessage from "./successMessage";
import cookie from "js-cookie";
import {
  faPhoneAlt,
  faHome,
  faCheckCircle,
} from "@fortawesome/free-solid-svg-icons";
export default function Index(props) {
  const Router = useRouter();
  let cooky = cookie.get("user");
  let loggedInUser = cooky && JSON.parse(cooky);
  const [loginState, setLoginState] = useState(true);
  const [forgotModal, setforgotModal] = React.useState(false);
  const [successmodal, setsuccessModal] = React.useState(false);
  const [otpModal, setotpModal] = React.useState(false);
  const [verifyotpModal, setverifyotpModal] = React.useState(false);
  const [tokenState, settokenState] = React.useState("");
  const [userState, setuserState] = React.useState("");
  const [isNumber, setisNumber] = React.useState(false);

  const [loading, setloading] = React.useState(false);
  const verifyotpToggle = () => {
    setverifyotpModal(!verifyotpModal);
    setotpModal(false);
  };
  const otpToggle = () => {
    setotpModal(!otpModal);
  };
  const successToggle = () => {
    setsuccessModal(!successmodal);
  };
  const GoDashboard = () => {
    let token = tokenState;
    let user = userState;

    cookie.set("token", token);
    cookie.set("user", JSON.stringify(user));
    cookie.set("path", path);

    let path;
    if (user.role === "student") {
      path = "/student/dashboard";
    } else if (user.role === "admin") {
      path = "/admin/dashboard";
    } else {
      path = "/partner/dashboard";
    }

    successToggle();
    Router.push(path);
  };

  const forgotToggle = () => setforgotModal(!forgotModal);

  const login_formik = useFormik({
    initialValues: {
      email: "",
    },
    validationSchema: isNumber == true ? NUMBER_YUP : LOGIN_YUP,
    onSubmit: async (values) => {
      setloading(true);
      if (isNumber) {
        values.phone = values.email;
      } else {
        values.phone = "";
      }

      try {
        let response = await Axios({
          method: "post",
          url: `${baseUrl}/auth/otpLogin`,
          data: values,
        });
        Notifier(response.data.message, "success");
        setloading(false);

        verifyotpToggle();
        formik.handleReset();
      } catch (err) {
        Notifier(err.response.data.message, "error");
        setloading(false);
      }
    },
  });
  const handleChange = (e) => {
    const { value, name } = e.target;
    if (value !== "" && isNaN(value) == false) {
      setisNumber(true);
      return true;
    }
    login_formik.setFieldValue(name, value);
  };
  const onNumberChange = (value) => {
    if (value === "") {
      setisNumber(false);
      login_formik.setFieldValue("email", "");
    } else {
      login_formik.setFieldValue("email", value);
    }
  };

  return (
    <>
      <Modal
        size="lg"
        modal={successmodal}
        toggle={successToggle}
        content={
          <SuccessMessage
            toggle={successToggle}
            onClick={GoDashboard}
            icon={faCheckCircle}
            title="Welcome to Westend !” You are just one step away from your dream
          education. Please complete your registration."
            buttonText="Go To Dashboard"
          />
        }
      />
      <Modal
        size="md"
        modal={otpModal}
        toggle={otpToggle}
        content={
          <OtpLogin toggle={otpToggle} verifyotpToggle={verifyotpToggle} />
        }
      />
      <Modal
        size="md"
        modal={verifyotpModal}
        toggle={verifyotpToggle}
        content={
          <VerifyOtpLogin
            toggle={verifyotpToggle}
            successToggle={successToggle}
            setuserState={setuserState}
            settokenState={settokenState}
            forgotToggle={forgotToggle}
            otpToggle={otpToggle}
          />
        }
      />
      <Modal
        size="md"
        modal={forgotModal}
        toggle={forgotToggle}
        content={<ForgotPassword forgotToggle={forgotToggle} />}
      />
      <div className="about-area-two ptb-70">
        <div className="container">
          <div className="row justify-content-center">
            {/* <div className="col-lg-6 order-md-2 imgSection"></div> */}
            <div className="col-md-12 col-lg-6 col-sm-12 col-xs-12 ">
              <div className="col-lg-12 text-center pb-2">
                <div className="mb-4">
                  <h3>Login</h3>
                </div>
                <div className="social-login">
                  <a
                    href="#"
                    onClick={() => signIn("facebook")}
                    className="facebook"
                  >
                    <span className="icon-facebook ">
                      <img src="/icons/facebok.png" />
                    </span>
                  </a>
                  <a
                    href="#"
                    onClick={() => alert("hello")}
                    className="twitter"
                  >
                    <span className="icon-twitter ">
                      <img src="/icons/linkedin.png" />
                    </span>
                  </a>
                  <a
                    href="#"
                    onClick={() => signIn("google")}
                    className="google"
                  >
                    <span className="icon-google ">
                      <img src="/icons/google.png" />
                    </span>
                  </a>
                </div>
                <span>or use your email account</span>
              </div>

              <form onSubmit={login_formik.handleSubmit}>
                <div className="form-group first">
                  {/* <label htmlFor="username">Username</label> */}
                  {isNumber == true ? (
                    <PhoneInput
                      country={"in"}
                      className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                      disableDropdown={true}
                      value={login_formik.values.email}
                      onChange={onNumberChange}
                    />
                  ) : (
                    <input
                      type="email"
                      className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                      placeholder="Email/Phone"
                      id="email"
                      name="email"
                      onChange={handleChange}
                      value={login_formik.values.email}
                    />
                  )}
                  <div className="text-danger pt-1">
                    {login_formik.touched.email && login_formik.errors.email ? (
                      <div>{login_formik.errors.email}</div>
                    ) : null}
                  </div>
                </div>
                <button
                  type="submit"
                  disabled={loading}
                  className="no-icon default-btn btn-block"
                >
                  {loading ? "Logging In.." : "Login With OTP"}
                </button>
              </form>

              <div className="col-lg-12 pb-1 pt-1 text-center">
                <span className="text-center"></span>
              </div>
              <button
                onClick={() => Router.push("/signup")}
                className="no-icon default-btn btn-block"
              >
                Create New Account <span></span>
              </button>
              <div
                id="forgotPassword"
                className="col-lg-12 col-md-12 col-sm-12  mt-2"
              >
                <a onClick={forgotToggle}>Forgot Your Password?</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
const LOGIN_YUP = Yup.object({
  email: Yup.string().email("Invalid email address").required("Required"),
});
const NUMBER_YUP = Yup.object({
  email: Yup.string()
    .min(10, "Must 10 characters long")
    // .max(15, "Must be equal to 10 characters ")
    .required("Required"),
});
