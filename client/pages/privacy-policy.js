import React from "react";
// import Navbar from '../components/_App/Navbar';
import PageBanner from "../components/Common/PageBanner";
import Sidebar from "../components/TermsOfService/Sidebar";
// import Footer from '../components/_App/Footer';

const PrivacyPolicy = () => {
  return (
    <React.Fragment>
      {/* <Navbar /> */}

      <div className="privacy-policy-area ptb-100">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 ">
              <div className="privacy-policy-content">
                <h3> Westend Privacy Policy Vrs.01</h3>

                <blockquote className="blockquote">
                  <p>
                    We know that you care how information about you is used and
                    shared, and we appreciate your trust. This Privacy Policy
                    describes our policies and procedures on the collection, use
                    and disclosure of your information when you use the Service
                    and tells you about your privacy rights and how the law
                    protects you. We value your trust & respect your privacy.
                    Unless otherwise defined in this Privacy Policy, terms used
                    in this Privacy Policy have the same meanings as in Our
                    Terms and Conditions.
                  </p>
                </blockquote>
                <h2>Collection of Information</h2>

                <h3>1. Personal Data</h3>
                <p>
                  While using Our Service, We may ask you to provide us with
                  certain personally identifiable information that can be used
                  to contact or identify you. Personally identifiable
                  information may include, but it’s not limited to:
                </p>
                <ul class="features-list">
                  <li>
                    <i class="bx bx-badge-check"></i>
                    First name and last name
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    Phone number
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    Address, State, Province, ZIP/Postal code, City
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    Education History
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    Email address
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    Credit history
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    Income proof
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    Interested area of further Education
                  </li>
                </ul>
                <h3>2. Usage Data</h3>
                <ul class="features-list">
                  <li>
                    <i class="bx bx-badge-check"></i>
                    Usage Data is collected automatically when using the
                    Service.
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    Usage Data may include information such as Your Device's
                    Internet Protocol address (e.g. IP address), browser type,
                    browser version, the pages of our Service that you visit,
                    the time and date of your visit, the time spent on those
                    pages, unique device identifiers and other diagnostic data.
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    When You access the Service by or through a mobile device,
                    We may collect certain information automatically, including,
                    but not limited to, the type of mobile device You use, Your
                    mobile device unique ID, the IP address of Your mobile
                    device, Your mobile operating system, the type of mobile
                    Internet browser You use, unique device identifiers and
                    other diagnostic data.
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    We may also collect information that your browser sends
                    whenever you visit our Service or when you access the
                    Service by or through a mobile device.
                  </li>
                </ul>
                <h3>3. Cookies</h3>
                <ul class="features-list">
                  <li>
                    <i class="bx bx-badge-check"></i>
                    When you use Our Services, We may send one or more cookies.
                    We use cookies to improve the quality of Our Service by
                    storing User preferences and tracking User information.
                    Certain of Our products and services allow you to download
                    and/or personalize the content you receive from us. For
                    these products and services, we will record information
                    about your preferences, along with any information you
                    provide yourself.
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    We may allow third-party service providers, like
                    advertisers, to place and read their own Cookies, web
                    beacons, and similar technologies to collect Data and/or
                    Personal Information through Services. This data and/or
                    personal information are collected directly and
                    automatically by these third parties, and we do not
                    participate in these data transmissions and these
                    third-party cookies are not covered under this Privacy
                    Policy.
                  </li>
                </ul>
                <h2>
                  For What Purposes Does WESTEND Use Your Personal Information?
                </h2>
                <ul class="features-list">
                  <li>
                    <i class="bx bx-badge-check"></i>
                    The Personal Information We collect allows us to keep you
                    posted on our latest announcements, upcoming events,
                    including confirmations, security alerts and support and
                    administrative messages. It also helps us to improve Our
                    Services. If you do not want to be on our mailing list, you
                    can opt out at any time by updating your preferences on the
                    Website. We may also use Personal Information for internal
                    purposes such as data analysis and research to improve Our
                    Services, products and customer communication.
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    We may also use Personal Information for internal purposes
                    such as data analysis and research to improve Our Services,
                    products and customer communication.
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    We collect Non-Personally Identifiable Information about Our
                    users generally to improve features and functionality of the
                    Website. We may analyse trends through aggregated
                    demographic and psychographic offers to users. We may also
                    share this Non-Personally Identifiable Information with Our
                    partners to allow them to assist us in delivering tailored
                    advertisements, promotions, deals, discounts, and other
                    offers to you when you use the Services. We collect
                    information from you when You participate in sweepstakes,
                    games or other promotions offered on Our Website.
                  </li>
                </ul>
                <h3>4. Single Sign-On</h3>
                <ul class="features-list">
                  <li>
                    <i class="bx bx-badge-check"></i>
                    You can log in to our site using sign-in service such as
                    Facebook, Google, or an Open ID provider. There services
                    will authenticate your identity and provide you the option
                    to share certain personal information with us such as your
                    sign-in information, name and email address to link between
                    the sites.
                  </li>
                </ul>
                <h2>
                  We only share personal information with other companies or
                  individuals in the following limited circumstances:
                </h2>

                <ul class="features-list">
                  <li>
                    <i class="bx bx-badge-check"></i>
                    We may share with third parties the location of your device
                    in order to improve and personalize your experience of the
                    Services.
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    We may share with third parties certain pieces of
                    non-personal information, such as the number of Users who
                    used a particular service, users who clicked on a particular
                    advertisement or who skipped a particular advertisement, as
                    well as similar aggregated information. Such information
                    does not identify you individually.
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    We provide such information to trusted businesses or persons
                    for the purpose of processing personal information on our
                    behalf. We require that these parties agree to process such
                    information based on our instructions and in compliance with
                    this Privacy Policy and any other appropriate
                    confidentiality and security measures.
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    Advertisers: We allow advertisers and/or merchant partners
                    (“Advertisers”) to choose the demographic information of
                    users who will see their advertisements and/or promotional
                    offers and You agree that We may provide any of the
                    information We have collected from You in non-personally
                    identifiable form to an Advertiser, in order for that
                    Advertiser to select the appropriate audience for those
                    advertisements and/or offers.
                  </li>
                </ul>
                <h3>5. Retention of Your Personal Data</h3>
                <ul class="features-list">
                  <li>
                    <i class="bx bx-badge-check"></i>
                    The Company will retain Your Personal Data only for as long
                    as is necessary for the purposes set out in this Privacy
                    Policy. We will retain and use Your Personal Data to the
                    extent necessary to comply with our legal obligations (for
                    example, if we are required to retain your data to comply
                    with applicable laws), resolve disputes, and enforce our
                    legal agreements and policies.
                  </li>
                </ul>
                <h4>Law enforcement</h4>
                <ul class="features-list">
                  <li>
                    <i class="bx bx-badge-check"></i>
                    Under certain circumstances, the Company may be required to
                    disclose Your Personal Data if required to do so by law or
                    in response to valid requests by public authorities (e.g. a
                    court or a government agency).
                  </li>
                </ul>
                <h4>Other legal requirements</h4>
                <p>
                  The Company may disclose Your Personal Data in the good faith
                  belief that such action is necessary to:
                </p>
                <ul class="features-list">
                  <li>
                    <i class="bx bx-badge-check"></i>
                    Comply with a legal obligation
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    Protect and defend the rights or property of the Company
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    Protect the personal safety of Users of the Service or the
                    public
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    Protect against legal liability
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    Prevent or investigate possible wrongdoing in connection
                    with the Service
                  </li>
                </ul>

                <h3>6. Links to Other Websites</h3>
                <ul class="features-list">
                  <li>
                    <i class="bx bx-badge-check"></i>
                    Our Service may contain links to other websites that are not
                    operated by us. If you click on a third party link, you will
                    be directed to that third party's site. We strongly advise
                    you to review the Privacy Policy of every site you visit.
                  </li>
                  <li>
                    <i class="bx bx-badge-check"></i>
                    We have no control over and assume no responsibility for the
                    content, privacy policies or practices of any third party
                    sites or services.
                  </li>
                </ul>
                <h3>7. Contact Us</h3>

                <p>
                  If you have any questions, comments or feedback regarding this
                  Privacy and Security Policy or any other privacy or security
                  concern, send an e-mail to inquiry@westend.co.in
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* <Footer /> */}
    </React.Fragment>
  );
};

export default PrivacyPolicy;
