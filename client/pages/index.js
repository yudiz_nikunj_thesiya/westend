import React from "react";
import MainBanner from "@/components/Index/MainBanner";
import Features from "@/components/Index/Features";
import Testimonials2 from "@/components/Common/Testimonials2";
import Testimonials from "@/components/Index/Testimonials";
import Instance from "@/components/Index/Instance";
import Partners from "@/components/Index/Partners";
import Ad from "@/components/Index/Ad";
import Funfacts from "@/components/Index/Funfacts";
import Blog from "@/components/Index/Blog";
import EdemyPremium from "@/components/Index/EdemyPremium";
import Countries from "../components/Common/Partner";
import { useRouter } from "next/router";
import { handleLogin } from "../utils/auth";
import Notifier from "../utils/Notifier";

import { useSession } from "next-auth/client";

const Index = () => {
  const [session, loading] = useSession();
  const router = useRouter();
  React.useEffect(() => {
    // If no session exists, redirect to login
    console.log(session, "session");

    if (!session) {
      router.push("/");
    } else if (session && session.user) {
      const { token, user } = session;
      let path;
      if (user.role === "student") {
        path = "/student/dashboard";
      } else if (user.role === "admin") {
        path = "/admin/dashboard";
      } else {
        path = "/partner/dashboard";
      }
      handleLogin(token, user, path);
    } else if (session && session.successMessage) {
      Notifier(session.successMessage, "success");
    } else if (session && session.errorMessage) {
      // Notifier(session.errorMessage, "error");
    }
  }, [session]);
  return (
    <React.Fragment>
      <MainBanner />
      <Features />
      <Countries />
      <Partners />
      <EdemyPremium />
      <Instance />
      <Testimonials2 />
    </React.Fragment>
  );
};

export default Index;
