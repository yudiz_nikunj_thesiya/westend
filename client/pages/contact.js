import React from "react"
// import Navbar from '../components/_App/Navbar';
import PageBanner from "../components/Common/PageBanner"
import ContactForm from "../components/Contact/ContactForm"
// import Footer from '../components/_App/Footer';
import GoogleMap from "../components/Contact/GoogleMap"

const Contact = () => {
  return (
    <React.Fragment>
      {/* <Navbar /> */}

      <div className="contact-area ptb-100">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-6 col-md-12">
              <div className="contact-info">
                <span className="sub-title">Contact Details</span>
                <h2>Get in Touch</h2>
                <h4>
                  Whether you have a comment to share, we look forward to hearing from you
                </h4>

                <ul>
                  <li>
                    <div className="icon">
                      <i className="bx bx-map"></i>
                    </div>
                    <h3>Our Address</h3>
                    <p>
                      Westend Educorp Pvt Ltd<br></br> 1005, B- Square -2, Near to Hotel
                      Double Tree by hilton, Iscon-Ambali Road, Ahmedabad-380054
                    </p>
                  </li>
                  <li>
                    <div className="icon">
                      <i className="bx bx-phone-call"></i>
                    </div>
                    <h3>Contact</h3>
                    <p>
                      Mobile: <a href="tel:+919023726795">+91 90237 26795</a>
                    </p>
                    <p>
                      Office: <a href="tel:+919313924262">+91 93139 24262</a>
                    </p>
                    <p>
                      Mail:{" "}
                      <a href="mailto:inquiry@westend.co.in">inquiry@westend.co.in</a>
                    </p>
                  </li>
                </ul>
              </div>
            </div>

            <div className="col-lg-6 col-md-12">
              <ContactForm />
            </div>
          </div>
        </div>
      </div>

      <GoogleMap />

      {/* <Footer /> */}
    </React.Fragment>
  )
}

export default Contact
