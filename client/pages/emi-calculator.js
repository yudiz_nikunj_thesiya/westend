import React,{useState,useEffect} from "react";
import Table from "../components/Table";
import Data from "../utils/data.json";
import { PieChart } from "react-minimal-pie-chart";
import { faEdit, faTrash,faCircle } from "@fortawesome/free-solid-svg-icons";
import FontAwesomeIcon from "../components/FontAwesomeIcon/index.js";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemPanel,
  AccordionItemButton,
} from "react-accessible-accordion";
const EmiCalculator = (props) => {
 
  const [formData, setformData] = useState({
    amount: 100000,
    interest: 10,
    duration:5,
  });

  // state to storage the results of the calculation
  const [results, setResults] = useState({
    monthlyPayment: 0,
    totalPayment: 0,
    totalInterest: 0,
    loanAmount:100000,
    isResult: false,
  });

  const handleChange=(e)=>{
    const {name,value}=e.target;
    setformData({...formData,[name]:value})
    let amount=100000;
    let interest=10;
    let years=5;


    if(name=='amount'){
      amount=value;
    }else if(name=='interest'){
      interest=value;

    }else if(name=='duration'){
      years=value;
    }
    const userAmount = Number(amount);
    const calculatedInterest = Number(interest) / 100 / 12;
    const calculatedPayments = Number(years) * 12;
    const x = Math.pow(1 + calculatedInterest, calculatedPayments);
    const monthly = (userAmount * x * calculatedInterest) / (x - 1);

    if (isFinite(monthly)) {
      const monthlyPaymentCalculated = monthly.toFixed(2);
      const totalPaymentCalculated = (monthly * calculatedPayments).toFixed(2);
      const totalInterestCalculated = (
        monthly * calculatedPayments -
        userAmount
      ).toFixed(2);
      // Set up results to the state to be displayed to the user
      setResults({
        monthlyPayment: monthlyPaymentCalculated,
        totalPayment: totalPaymentCalculated,
        totalInterest: totalInterestCalculated,
        loanAmount: userAmount,
        isResult: true,
      });
    }
    return;
  }
  useEffect(()=>{
if(props){
 const { monthlyPaymentCalculated: monthlyPaymentCalculated,
    totalPaymentCalculated:totalPaymentCalculated,
    totalInterestCalculated:totalInterestCalculated,
    totalPrincipleInterestCalculated:totalPrincipleInterestCalculated,
    loanAmount:loanAmount,
  }=props
  setResults({
    monthlyPayment: monthlyPaymentCalculated,
    totalPayment: totalPaymentCalculated,
    totalInterest: totalInterestCalculated,
    totalPrincipleInterest:totalPrincipleInterestCalculated,
    loanAmount:loanAmount,
    isResult: true,
  });
}
  },[props])

  return (
    <React.Fragment>
      {/* <Navbar /> */}

      <div className="privacy-policy-area pb-100">
        <div className="container">
          <div className="section-title">
            <span className="sub-title">Student Loan</span>
            <h2>EMI Calculator</h2>
          </div>

          <div className="row">
            <div className="col-lg-12 col-md-12">
              <div className="privacy-policy-content">
                <div className="row ">
                  <div className="col-lg-4 col-sm-12 col-xs-12 col-md-12">
                    <div className="rangeSlider">
                      <div className="card">
                        <div className="slide">
                          <form className="range-field  w-full">
                            <div className="d-flex justify-content-between">
                              <label>Loan Amount</label>
                              <label>₹{formData.amount}</label>
                            </div>
                            <input
                              type="range"
                              name="amount"
                              onChange={handleChange}
                              value={formData.amount}
                              min={100000}
                              max={10000000}
                            />
                          </form>
                        </div>

                        <div className="slide">
                          <form className="range-field  w-full">
                            <div className="d-flex justify-content-between">
                              <label>Interest (p.a)</label>
                              <label>{formData.interest}%</label>
                            </div>
                            <input
                              onChange={handleChange}
                              value={formData.interest}
                              name="interest"
                              min="0"
                              type="range"
                              max={100}
                            />
                          </form>
                        </div>
                        <div className="slide">
                          <form className="range-field  w-full">
                            <div className="d-flex justify-content-between">
                              <label>Loan Duration</label>
                              <label>{formData.duration} Years</label>
                            </div>
                            <input
                              onChange={handleChange}
                              value={formData.duration}
                              min="0"
                              name="duration"
                              type="range"
                              max={10}
                            />
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-4  col-sm-12 col-xs-12 col-md-12">
                    <div className="card2">
                      <div className="card">
                        <div className="slide">
                          <form className="range-field  w-full">
                            <div>
                              <span>
                                <b>Monthly EMI</b>
                              </span>
                            </div>
                            <div className="pt-1">
                              <span>₹{results.monthlyPayment}</span>
                            </div>
                          </form>
                        </div>
                      </div>
                    
                      <div className="card mt-2">
                        <div className="slide">
                          <form className="range-field  w-full">
                            <div>
                              <span>
                                <b>Total Interest</b>
                              </span>
                            </div>
                            <div className="pt-1">
                            <span>₹{results.totalInterest}</span>
                            </div>
                          </form>
                        </div>
                      </div>
                      <div className="card mt-2">
                        <div className="slide">
                          <form className="range-field  w-full">
                            <div>
                              <span>
                                <b>Total Amount</b>
                              </span>
                            </div>
                            <div className="pt-1">
                            <span>₹{results.totalPayment}</span>

                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-4 col-sm-12  col-xs-12 col-md-12">
                    <div className="rangeSlider">
                      <div className="card">
                        <div className="slide">
                          <div className="d-flex">
                          <PieChart
       label={({ dataEntry }) => dataEntry.value}
       labelStyle={
         {
          fontSize: '8px',
          fill: '#fff',
         color:"white"
         }
       }
       
       labelPosition={70}
       style={{height:"170px"}}
       data={[
          {
          
            label: "TotalPayment",
            value: Math.round(results.loanAmount),
            color: "#152f68",
          },
          {
          label: "Interest",
          value: Math.round(results.totalInterest),
           color: "#eb4034" }
        ]}
      />
                            </div>

                            <div className="d-flex p-3 justify-content-center">
                          <div className="d-flex justify-content-between mr-4">
                          <FontAwesomeIcon
                            icon={faCircle}
                         color="#eb4034"
                            onClick={() => props.handleEdit(row.id)}
                          />
                            <small className="ml-2">Interest</small>
                          </div>
                          <div className="d-flex justify-content-between">
                          <FontAwesomeIcon
                            icon={faCircle}
                         color="#152f68"
                            onClick={() => props.handleEdit(row.id)}
                          />
                            
                            <small className="ml-2">Loan Amount</small>
                          </div>
                              </div>                           
                          <div className="d-flex justify-content-between">
                            <label>Disbursal Duration</label>
                            <label>6 months</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* <div className="emiTable">
                  <div className="card">
                    <div>
                      <h5>Loan Amortization Schedule</h5>
                      <p>
                        Following table show the year wise payment schedule.
                      </p>
                    </div>
                    <Table
                      headers={Data.EMI_TABLE.headings}
                      rows={Data.EMI_TABLE.items}
                    />
                  </div>
                </div> */}
        <div className="faq-accordion p-2">
        <Accordion allowZeroExpanded preExpanded={["a"]}>
          <AccordionItem uuid="a">
            <AccordionItemHeading>
              <AccordionItemButton>
              Calculate EMI for Education Loan
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
                <ul class="features-list">
                <li><i class="bx bx-badge-check"></i> The Education Loan EMI calculator from Westend is
                  an automated tool that will quickly let you know about your
                  EMI. Note, the interest rate and your loan tenure are the
                  critical influencing factors that decide the EMI loan. </li>
                <li><i class="bx bx-badge-check"></i>Note, the interest rate and your loan tenure are the
                  critical influencing factors that decide the EMI loan.</li>
                <li><i class="bx bx-badge-check"></i>The
                  principal portion and the interest represent the EMI of your
                  loan. The EMI generally stays set for the entire term of your
                  loan and is to be repaid every month over the loan tenure.</li>
                <li><i class="bx bx-badge-check"></i>You
                  pay more interest during the first few years of your loan
                  term, and eventually, as you repay the loan, a higher portion
                  is modified towards the central part.</li>

              </ul>

              </AccordionItemPanel>
          </AccordionItem>
          </Accordion>
          </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* <Footer /> */}
    </React.Fragment>
  );
};
EmiCalculator.getInitialProps = async ({ Component, query }) => {
  let amount=100000;
  let interest=10;
  let years=5;
  const userAmount = Number(amount);
  const calculatedInterest = Number(interest) / 100 / 12;
  const calculatedPayments = Number(years) * 12;
  const x = Math.pow(1 + calculatedInterest, calculatedPayments);
  const monthly = (userAmount * x * calculatedInterest) / (x - 1);

  if (isFinite(monthly)) {
    var monthlyPaymentCalculated = monthly.toFixed(2);
    var totalPaymentCalculated = (monthly * calculatedPayments).toFixed(2);
    var totalInterestCalculated = (
      monthly * calculatedPayments -
      userAmount
    ).toFixed(2);
    

  }
  return { monthlyPaymentCalculated: monthlyPaymentCalculated,
    totalPaymentCalculated:totalPaymentCalculated,
    totalInterestCalculated:totalInterestCalculated,
    loanAmount:amount
  };
}
export default EmiCalculator;
