import React from "react";
import {
  faPhoneAlt,
  faHome,
  faCheckCircle,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
export default function successMessage(props) {
  const { toggle, onClick, title, buttonText, icon } = props;
  return (
    <div className="container">
      <div className="row">
        <div className="col-lg-12 text-center ">
          <h1>
            <FontAwesomeIcon icon={icon} color="green" />
          </h1>

          <h5>{title}</h5>
          <button onClick={onClick} className="default-btn2">
            {buttonText}
            <span></span>
          </button>
          <hr />
        </div>
      </div>
    </div>
  );
}
