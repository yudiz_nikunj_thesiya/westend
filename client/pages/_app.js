import "../public/css/bootstrap.min.css";
import "../public/css/animate.min.css";
import "../public/css/boxicons.min.css";
import "../public/css/meanmenu.min.css";
import "../public/css/flaticon.css";
import "../node_modules/react-modal-video/css/modal-video.min.css";
import "react-accessible-accordion/dist/fancy-example.css";
import "react-tabs/style/react-tabs.css";
import "react-image-lightbox/style.css";
import "../public/css/style.css";
import "../public/css/responsive.css";
import "bootstrap-daterangepicker/daterangepicker.css";
import React from "react";

import Layout from "../components/_App/Layout";
import { Toaster } from "react-hot-toast";
import { StoreProvider } from "../Context";
import { parseCookies } from "nookies";
import { redirectUser } from "../utils/auth";
import baseUrl from "../utils/baseUrl";
import { Provider as AuthProvider } from "next-auth/client";
import { values } from "next-pwa/cache";

const MyApp = ({ Component, pageProps: { session, ...pageProps } }) => {
  return (
    <AuthProvider session={session}>
      <StoreProvider>
        <Layout {...pageProps}>
          <Component {...pageProps} />
          <Toaster />
        </Layout>
      </StoreProvider>
    </AuthProvider>
  );
};

export default MyApp;

MyApp.getInitialProps = async ({ req, Component, ctx, ...values }) => {
  // console.log("ctx: ", ctx);
  // console.log("values: ", values);
  const { token, user, path } = parseCookies(ctx);

  let pageProps = {};
  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx);
  }
  if (!token) {
    // if a user not logged in then user can't access those pages
    const isProtectedRoute =
      ctx.pathname === "/student/dashboard" ||
      ctx.pathname === "/partner/dashboard" ||
      ctx.pathname === "/admin/dashboard";

    if (isProtectedRoute) {
      redirectUser(ctx, `/`);
    }
  } else {
    let { role } = JSON.parse(user);

    // if a user logged in then user can't access those pages
    // role == student
    if (role === "student") {
      const ifLoggedIn =
        ctx.pathname === "/admin/dashboard" ||
        ctx.pathname === "/partner/dashboard";

      if (ifLoggedIn) {
        redirectUser(ctx, "/student/dashboard");
      }
    } else if (role === "partner") {
      const ifLoggedIn =
        ctx.pathname === "/admin/dashboard" ||
        ctx.pathname === "/student/dashboard";

      if (ifLoggedIn) {
        redirectUser(ctx, "/partner/dashboard");
      }
    } else if (role === "admin") {
      const ifLoggedIn =
        ctx.pathname === "/partner/dashboard" ||
        ctx.pathname === "/student/dashboard";

      if (ifLoggedIn) {
        redirectUser(ctx, "/admin/dashboard");
      }
    }
  }

  // By returning { props: posts }, the Blog component
  // will receive `posts` as a prop at build time
  return {
    pageProps,
  };
};
