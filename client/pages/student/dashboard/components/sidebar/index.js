import React from "react";
import Link from "@/utils/ActiveLink";
import Button from "../../../../../components/Button/index";
import { faLongArrowAltLeft } from "@fortawesome/free-solid-svg-icons";
import { getUser, getToken } from "../../../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
export default function index(props) {
  return (
    <div className="sidebar col-lg-3 ">
      <div className="td-sidebar">
        <ul>
          <li>
            <a
              onClick={() => props.toggle(1)}
              className={props.isActive === 1 ? "active" : "album"}
            >
              Application
            </a>
          </li>
          <li>
            <a
              className={props.isActive === 2 ? "active" : "album"}
              onClick={() => props.toggle(2)}
            >
              Active Loans
            </a>
          </li>
          <li>
            <a
              className={props.isActive === 3 ? "active" : "album"}
              onClick={() => props.toggle(3)}
            >
              Tools
            </a>
          </li>
          <li>
            <a
              className={props.isActive === 4 ? "active" : "album"}
              onClick={() => props.toggle(4)}
            >
              FAQ's
            </a>
          </li>
          {loggedInUser &&
          loggedInUser.role == "admin" &&
          props.studentTab &&
          props.studentTab === true ? (
            ""
          ) : (
            <>
              <li>
                <a
                  className={props.isActive === 5 ? "active" : "album"}
                  onClick={() => props.toggle(5)}
                >
                  Edit Profile
                </a>
              </li>
              <li>
                <a
                  className={props.isActive === 6 ? "active" : "album"}
                  onClick={() => props.toggle(6)}
                >
                  Change Password
                </a>
              </li>
            </>
          )}
        </ul>
      </div>
      {loggedInUser && loggedInUser.role == "student" ? (
        ""
      ) : (
        <div className="d-flex  stepBackButton justify-content-center">
          <Button
            onClick={props.handleBack}
            className="no-icon default-btn"
            icon={faLongArrowAltLeft}
            label="Back"
          />
        </div>
      )}
    </div>
  );
}
