import React, { useEffect } from "react";
import Axios from "axios";
import Notifier from "../../../../../../../utils/Notifier";
import Button from "../../../../../../../components/Button/index";
import PhoneInput from "../../../../../../../components/CustomPhoneInput.js";
import TextInput from "../../../../../../../components/TextInput";

import baseUrl from "../../../../../../../utils/baseUrl";
import { useFormik } from "formik";
import * as Yup from "yup";
import { getUser, getToken } from "../../../../../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
function BasicDetails(props) {
  const [loading, setloading] = React.useState(false);
  const { handleNext, activeStep, steps } = props;
  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      phone: "",
      email: "",
      date: "",
      state: "",
      city: "",
      pinCode: "",
      address: "",
    },
    validationSchema: Detail_YUP,
    onSubmit: async (values) => {
      setloading(true);
      if (loggedInUser) {
        let _id = loggedInUser.id;
        if (props.studentID) {
          _id = props.studentID;
        }
        try {
          let response = await Axios({
            method: "post",
            url: `${baseUrl}/student/createBasicDetail/${_id}`,
            data: values,
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          handleNext();

          setloading(false);

          props.setData(response.data.data);

          // router.push("/profile");
        } catch (err) {
          setloading(false);
          if (err.response) {
            Notifier(err.response.data.message, "error");
          }
        }
      }
    },
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "phone" || name === "pinCode") {
      if (value.length <= 10) {
        formik.setFieldValue(name, value.replace(/\D/g, ""));
      }
    } else {
      formik.setFieldValue(name, value);
    }
  };
  const getCourseDetails = async () => {
    if (props.data.data) {
      let data = props.data.data;
      Object.keys(data).map((dat, i) => {
        formik.setFieldValue(dat, data[dat]);
      });
    } else {
      if (loggedInUser) {
        let _id = loggedInUser.id;
        if (props.studentID) {
          _id = props.studentID;
        }
        try {
          let response = await Axios({
            method: "get",
            url: `${baseUrl}/student/getApplication/${_id}`,
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          let data = response.data.data.data;

          Object.keys(data).map((dat, i) => {
            formik.setFieldValue(dat, data[dat]);
            formik.setFieldValue(dat, data[dat]);
            if (dat == "date") {
              const date = new Date(data[dat]);
              formik.setFieldValue(dat, date.toISOString().slice(0, 10));
            }
          });
        } catch (error) {
          if (error.response)
            console.log(error.response.data.message, "error ");
        }
      }
    }
  };
  useEffect(() => {
    getCourseDetails();
  }, [props.data, activeStep]);
  const onNumberChange = (value) => {
    formik.setFieldValue("phone", value);
  };
  return (
    <div>
      <form className="w-full  ">
        <div className="2xl:flex  2xl:flex-row xl:flex xl:flex-row lg:flex lg:flex-row md:flex md:flex-row no-wrap sm:flex sm:flex-col -mx-3  ">
          <div className="w-full mb-3 lg:mb-2 md:w-1/2 px-3  ">
            <TextInput
              type="text"
              placeholder="First Name"
              label="First Name"
              name="firstName"
              onChange={handleChange}
              value={formik.values.firstName}
            />
            <div className="text-danger pt-1">
              {formik.touched.firstName && formik.errors.firstName ? (
                <div>{formik.errors.firstName}</div>
              ) : null}
            </div>
          </div>
          <div className="w-full mb-3 lg:mb-2 md:w-1/2 px-3">
            <TextInput
              type="text"
              placeholder="Last Name"
              label="Last Name"
              name="lastName"
              onChange={handleChange}
              value={formik.values.lastName}
            />
            <div className="text-danger pt-1">
              {formik.touched.lastName && formik.errors.lastName ? (
                <div>{formik.errors.lastName}</div>
              ) : null}
            </div>
          </div>
          <div className="w-full mb-3 lg:mb-2 md:w-1/2 px-3 mb-2 md:mb-3 lg:mb-2">
            <label
              className="inputLabel block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-first-name"
            >
              Phone Number
            </label>
            <PhoneInput value={formik.values.phone} onChange={onNumberChange} />
            <div className="text-danger pt-1">
              {formik.touched.phone && formik.errors.phone ? (
                <div>{formik.errors.phone}</div>
              ) : null}
            </div>
          </div>
        </div>

        <div className="2xl:flex  2xl:flex-row xl:flex xl:flex-row lg:flex lg:flex-row md:flex md:flex-row no-wrap sm:flex sm:flex-col -mx-3 ">
          <div className="w-full  md:w-1/2  mb-3 lg:mb-2 px-3">
            <TextInput
              id="grid-last-name"
              type="text"
              placeholder="Email"
              label="Email"
              name="email"
              onChange={formik.handleChange}
              value={formik.values.email}
            />
            <div className="text-danger pt-1">
              {formik.touched.email && formik.errors.email ? (
                <div>{formik.errors.email}</div>
              ) : null}
            </div>
          </div>

          <div className="w-full  md:w-1/2  mb-3 lg:mb-2 px-3">
            <TextInput
              type="date"
              placeholder="Date of Birth"
              label="Date of Birth"
              name="date"
              onChange={handleChange}
              value={formik.values.date}
            />
            <div className="text-danger pt-1">
              {formik.touched.date && formik.errors.date ? (
                <div>{formik.errors.date}</div>
              ) : null}
            </div>
          </div>
        </div>

        <div className="flex no-wrap mb-2 md:mb-3  -mx-3 ">
          <div className="w-full mb-3 lg:mb-2  px-3">
            <TextInput
              type="text"
              placeholder="Address Line 1"
              label="Address Line 1"
              name="address"
              onChange={handleChange}
              value={formik.values.address}
            />
            <div className="text-danger pt-1">
              {formik.touched.address && formik.errors.address ? (
                <div>{formik.errors.address}</div>
              ) : null}
            </div>
          </div>
        </div>
        <div className="2xl:flex  2xl:flex-row xl:flex xl:flex-row lg:flex lg:flex-row md:flex md:flex-row no-wrap sm:flex sm:flex-col -mx-3 ">
          <div className="w-full mb-3 lg:mb-2 md:w-1/2 px-3  ">
            <TextInput
              type="text"
              placeholder="State"
              label="State"
              name="state"
              onChange={handleChange}
              value={formik.values.state}
            />
            <div className="text-danger pt-1">
              {formik.touched.state && formik.errors.state ? (
                <div>{formik.errors.state}</div>
              ) : null}
            </div>
          </div>
          <div className="w-full mb-3 lg:mb-2  md:w-1/2 px-3">
            <TextInput
              id="grid-last-name"
              type="text"
              placeholder="City"
              label="City"
              name="city"
              onChange={handleChange}
              value={formik.values.city}
            />
            <div className="text-danger pt-1">
              {formik.touched.city && formik.errors.city ? (
                <div>{formik.errors.city}</div>
              ) : null}
            </div>
          </div>
          <div className="w-full mb-3 lg:mb-2  md:w-1/2 px-3">
            <TextInput
              id="grid-last-name"
              type="text"
              placeholder="PinCode"
              label="PinCode"
              name="pinCode"
              onChange={handleChange}
              value={formik.values.pinCode}
            />
            <div className="text-danger pt-1">
              {formik.touched.pinCode && formik.errors.pinCode ? (
                <div>{formik.errors.pinCode}</div>
              ) : null}
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-12">
            <div className="d-flex text-right">
              {activeStep !== steps.length && (
                <Button
                  onClick={formik.handleSubmit}
                  className="no-icon default-btn  "
                  label={loading ? "SUBMITTING" : "Next"}
                />
              )}
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}

export default BasicDetails;

const Detail_YUP = Yup.object({
  firstName: Yup.string()
    .max(100, "Must be less than 100 characters")
    .required("Required"),
  lastName: Yup.string()
    .max(100, "Must be less than 100 characters")
    .required("Required"),
  phone: Yup.string()
    .min(10, "Must 10 characters long")
    // .max(15, "Must be equal to 10 characters ")
    .required("Required"),
  email: Yup.string().email("Invalid email address").required("Required"),

  date: Yup.string().required("Required"),
  address: Yup.string()
    .max(100, "Must be less than 100 characters")
    .required("Required"),
  state: Yup.string()
    .max(100, "Must be less than 100 characters")
    .required("Required"),
  city: Yup.string()
    .max(100, "Must be less than 100 characters")
    .required("Required"),
  pinCode: Yup.number().required("Required"),
});
