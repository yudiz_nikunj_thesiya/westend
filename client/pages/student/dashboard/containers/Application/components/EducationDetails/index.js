import React, { useEffect } from "react";
import AutoComplete from "../../../../../../../components/AutoComplete/Index.js";
import RadioButton from "../../../../../../../components/RadioButton/Index.js";
import Button from "../../../../../../../components/Button/index";
import { parseCookies } from "nookies";
import cookie from "js-cookie";
import TextInput from "../../../../../../../components/TextInput";

import { useFormik } from "formik";
import * as Yup from "yup";
import {
  decision,
  Bachelors,
  universities,
  educations,
  departments,
  degrees,
} from "../../../../../../../utils/Globals";
import { Store } from "../../../../../../../Context";
import baseUrl from "../../../../../../../utils/baseUrl";
import Axios from "axios";
import Notifier from "../../../../../../../utils/Notifier";
import { getUser, getToken } from "../../../../../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
export default function Education(props) {
  const {
    handleBack,
    handleNext,
    completedSteps,
    totalSteps,
    activeStep,
    steps,
    handleSkip,
  } = props;
  const [loading, setloading] = React.useState(false);

  const [checked, setChecked] = React.useState("");
  let cooky = cookie.get("user");
  let loggedInUser = cooky && JSON.parse(cooky);
  const getCourseDetails = async () => {
    // if (props.data.data) {
    //   let data = props.data.data;
    //   Object.keys(data).map((dat, i) => {
    //     formik.setFieldValue(dat, data[dat]);
    //   });
    //   setChecked(data.apply);
    // } else {
    if (loggedInUser) {
      let _id = loggedInUser.id;
      if (props.studentID) {
        _id = props.studentID;
      }
      try {
        let response = await Axios({
          method: "get",
          url: `${baseUrl}/student/getApplication/${_id}`,

          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        let data = response.data.data.data;
        Object.keys(data).map((dat, i) => {
          if (data[dat]) {
            formik.setFieldValue(dat, data[dat]);
          }
        });
        setChecked(data.apply);
      } catch (error) {
        if (error.response) console.log(error.response.data.message, "error ");
      }
    }
    //}
  };
  useEffect(() => {
    getCourseDetails();
  }, [props.data, props.setData, loading, setloading, activeStep]);

  const formik = useFormik({
    initialValues: {
      degree: "",
      course: "",
      college: "",
      tenPercentage: "",
      twelvePercentage: "",
      cgpa: "",
      backlogs: "No",
      test: "No",
      mortgage: "No",
      backlogsNumber: "0",
    },
    validationSchema: Education_YUP,

    onSubmit: async (values) => {
      setloading(true);
      if (loggedInUser) {
        let _id = loggedInUser.id;
        if (props.studentID) {
          _id = props.studentID;
        }

        if (checked == "UG") {
          values.degree = "";
          values.course = "";
          values.college = "";
          values.cgpa = "";
        }
        try {
          let response = await Axios({
            method: "post",
            url: `${baseUrl}/student/createEducationDetail/${_id}`,
            data: values,
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          props.setData(response.data.data);

          setloading(false);

          if (values.test === "No") {
            handleSkip();
          } else {
            handleNext();
          }

          // router.push("/profile");
        } catch (err) {
          setloading(false);
          if (err.response) {
            Notifier(err.response.data.message, "error");
          }
        }
      }
    },
  });
  const handleChange = (name, value) => {
    formik.setFieldValue(name, value);
  };
  const handleTest = (e) => {
    formik.setFieldValue(e.target.name, e.target.checked);
  };
  return (
    <div>
      <form className="w-full ">
        {checked == "PG" ? (
          <>
            <div className="2xl:flex 2xl:flex-row xl:flex xl:flex-row lg:flex lg:flex-row md:flex md:flex-row no-wrap sm:flex sm:flex-col -mx-3 mb-0">
              <div className="w-full md:w-1/2 px-3 mb-3">
                <label className="inputLabel block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                  Current Degree
                </label>
                <AutoComplete
                  data={departments}
                  name="degree"
                  label="Choose your current degree"
                  onChange={handleChange}
                  value={formik.values.degree}
                />
                <div className="text-danger pt-1">
                  {formik.touched.degree && formik.errors.degree ? (
                    <div>{formik.errors.degree}</div>
                  ) : null}
                </div>
              </div>
              <div className="w-full md:w-1/2 px-3 mb-3">
                <label className="inputLabel block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                  Current Area of Study
                </label>
                <AutoComplete
                  formik={formik}
                  data={educations}
                  label="Choose your current area of study"
                  name="course"
                  onChange={handleChange}
                  value={formik.values.course}
                />
                <div className="text-danger pt-1">
                  {formik.touched.course && formik.errors.course ? (
                    <div>{formik.errors.course}</div>
                  ) : null}
                </div>
              </div>
            </div>
            <div className="2xl:flex 2xl:flex-row xl:flex xl:flex-row lg:flex lg:flex-row md:flex md:flex-row no-wrap sm:flex sm:flex-col -mx-3 mb-0">
              <div className="w-full md:w-1/2 px-3 mb-3">
                <label className="inputLabel block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                  Current University/College/School
                </label>
                <input
                  className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4  leading-tight focus:outline-none focus:bg-white"
                  type="text"
                  placeholder="Type University/College Name"
                  name="college"
                  onChange={formik.handleChange}
                  value={formik.values.college}
                />
                <div className="text-danger pt-1">
                  {formik.touched.college && formik.errors.college ? (
                    <div>{formik.errors.college}</div>
                  ) : null}
                </div>
              </div>
              <div className="w-full md:w-1/2 px-3 mb-3">
                <label className="inputLabel block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                  CGPA/Percentage
                </label>
                <input
                  className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4  leading-tight focus:outline-none focus:bg-white"
                  type="text"
                  placeholder="CGPA"
                  name="cgpa"
                  onChange={formik.handleChange}
                  value={formik.values.cgpa}
                />
                <div className="text-danger pt-1">
                  {formik.touched.cgpa && formik.errors.cgpa ? (
                    <div>{formik.errors.cgpa}</div>
                  ) : null}
                </div>
              </div>
            </div>
          </>
        ) : (
          ""
        )}
        <div className="2xl:flex 2xl:flex-row xl:flex xl:flex-row lg:flex lg:flex-row md:flex md:flex-row no-wrap sm:flex sm:flex-col -mx-3 mb-0">
          <div className="w-full md:w-1/2 px-3 mb-3">
            <label className="inputLabel block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
              10th Percentage
            </label>
            <input
              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-0 leading-tight focus:outline-none focus:bg-white"
              type="number"
              min="0"
              max="100"
              placeholder="10th Percentage"
              name="tenPercentage"
              onChange={formik.handleChange}
              value={formik.values.tenPercentage}
            />
            <div className="text-danger pt-1">
              {formik.touched.tenPercentage && formik.errors.tenPercentage ? (
                <div>{formik.errors.tenPercentage}</div>
              ) : null}
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3 mb-3">
            <label className="inputLabel block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
              12th Percentage
            </label>
            <input
              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-0 leading-tight focus:outline-none focus:bg-white"
              type="number"
              min="0"
              max="100"
              placeholder="12th Percentage"
              name="twelvePercentage"
              onChange={formik.handleChange}
              value={formik.values.twelvePercentage}
            />
            <div className="text-danger pt-1">
              {formik.touched.twelvePercentage &&
              formik.errors.twelvePercentage ? (
                <div>{formik.errors.twelvePercentage}</div>
              ) : null}
            </div>
          </div>
        </div>
        <div className="2xl:flex 2xl:flex-row xl:flex xl:flex-row lg:flex lg:flex-row md:flex md:flex-row no-wrap sm:flex sm:flex-col -mx-3 mb-0">
          <div className="w-full md:w-1/2 px-3 mb-3">
            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
              Do you have backlogs?
            </label>
            <RadioButton
              name="backlogs"
              onChange={formik.handleChange}
              value={formik.values.backlogs}
              options={decision}
            />
          </div>

          <div className="w-full md:w-1/2 px-3 mb-3">
            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
              Have you appear IELTS/PTE/TOEFL/GRE/GMAT/SAT?
            </label>

            <RadioButton
              name="test"
              onChange={formik.handleChange}
              value={formik.values.test}
              options={decision}
            />
          </div>
        </div>
        {formik.values.backlogs == "Yes" ? (
          <div className="2xl:flex 2xl:flex-row xl:flex xl:flex-row lg:flex lg:flex-row md:flex md:flex-row no-wrap sm:flex sm:flex-col -mx-3 mb-0">
            <div className="w-full md:w-1/2 px-3 mb-3">
              <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                No. Of backlogs?
              </label>
              <input
                className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-0 leading-tight focus:outline-none focus:bg-white"
                type="number"
                min="0"
                max="4"
                placeholder="No. of backlogs"
                name="backlogsNumber"
                onChange={formik.handleChange}
                value={formik.values.backlogsNumber}
              />
              <div className="text-danger pt-1">
                {formik.touched.backlogsNumber &&
                formik.errors.backlogsNumber ? (
                  <div>{formik.errors.backlogsNumber}</div>
                ) : null}
              </div>
            </div>
          </div>
        ) : (
          ""
        )}
        <div className="row">
          <div className="col-lg-12">
            <div className="d-flex text-right">
              <Button
                onClick={handleBack}
                className="no-icon default-btn mr-3 "
                label="Back"
              />
              {activeStep !== steps.length && (
                <Button
                  onClick={formik.handleSubmit}
                  className="no-icon ml-3 default-btn  "
                  label={loading ? "SUBMITTING" : "Next"}
                />
              )}
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}

const Education_YUP = Yup.object({
  tenPercentage: Yup.number()
    .min(0, "Percentage should be greater than 0")
    .max(100, "Percentage should be less than or equal to 100")
    .required("Required"),
  twelvePercentage: Yup.number()
    .min(0, "Percentage should be greater than 0")
    .max(100, "Percentage should be less than or equal to 100")
    .required("Required"),
});
