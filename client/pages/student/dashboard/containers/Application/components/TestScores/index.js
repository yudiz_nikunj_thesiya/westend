import React, { useState, useEffect } from "react";
import MemoizedIndex from "./MemoScore";

import Notifier from "../../../../../../../utils/Notifier";
import { Store } from "../../../../../../../Context";
import baseUrl from "../../../../../../../utils/baseUrl";
import Axios from "axios";
import { parseCookies } from "nookies";
import cookie from "js-cookie";
import { getUser, getToken } from "../../../../../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
export default function Index(props) {
  const {
    handleBack,
    handleNext,
    completedSteps,
    totalSteps,
    activeStep,
    steps,
  } = props;

  const [scoresArray, setscoresArray] = useState([]);
  const [data, setData] = useState("");
  const getCourseDetails = async () => {
    if (props.data) {
      let data = props.data.testScore;
      setData(data);
    } else {
      if (loggedInUser) {
        let _id = loggedInUser.id;
        if (props.studentID) {
          _id = props.studentID;
        }
        try {
          let response = await Axios({
            method: "get",
            url: `${baseUrl}/student/getApplication/${_id}`,

            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          let data = response.data.data.testScore;

          setData(data);
        } catch (error) {
          if (error.response)
            console.log(error.response.data.message, "error ");
        }
      }
    }
  };
  useEffect(() => {
    getCourseDetails();
  }, [props.data, activeStep]);

  const handleAdd = (scores, sethide) => {
    onAdd(scores);
    if(sethide)
      sethide(true);
    setData([...data, scores]);
  };
  const onAdd = async (scores) => {
    if (loggedInUser) {
      let _id = loggedInUser.id;
      if (props.studentID) {
        _id = props.studentID;
      }

      try {
        let response = await Axios({
          method: "post",
          url: `${baseUrl}/student/createTestScore/${_id}`,
          data: scores,
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        console.log(response.data, "response.data");

        props.setData(response.data.data);

        // router.push("/profile");
      } catch (err) {
        if (err.response) {
          Notifier(err.response.data.message, "error");
        }
      }
    }
  };
  const handleSubmit = async (e) => {
    //handleAdd();
    handleNext();
  };
  const handleDelete = async (id) => {
    let fill = data.filter((dt) => dt.id !== id);
    setData(fill);
    try {
      let response = await Axios({
        method: "delete",
        url: `${baseUrl}/student/deleteScore/${id}`,

        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      let data = response.data.data;

      props.setData(data);
      Notifier(response.data.message, "success");
      // router.reload();
    } catch (err) {
      if (err.response) {
        Notifier(err.response.data.message, "error");
      }
    }
  };
  const onHandleDelete = (score) => {
    let fill = scoresArray.filter((dt) => dt.score !== score.score);

    setData(fill);
  };
  return (
    <div>
      <form className="w-full ">
        {data ? (
          data &&
          data.map((score) => (
            <div key={score} className="mt-2">
              <MemoizedIndexs
                testName={score.testName}
                Score={score.score}
                onDelete={() => handleDelete(score.id)}
              />
            </div>
          ))
        ) : (
          <h1 className="text-center"></h1>
        )}
        <MemoizedIndex onAdd={handleAdd} />

        <div className="row  ">
          <div className="col-lg-12">
            <div className="flex lg:justify-start  my-6 ">
              <button
                // disabled={activeStep === 0}
                onClick={handleBack}
                className="no-icon default-btn2  mr-3 "
              >
                Back<span></span>
              </button>

              {activeStep !== steps.length && (
                <button
                  onClick={handleSubmit}
                  type="button"
                  className="no-icon default-btn  "
                >
                  {completedSteps() === totalSteps() - 1 ? "Submit" : "Next"}
                  <span></span>
                </button>
              )}
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}
