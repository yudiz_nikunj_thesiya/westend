import React, { useState } from "react";
import AutoComplete from "../../../../../../../components/AutoComplete/Index.js";
import { TestScores } from "../../../../../../../utils/Globals";
import Notifier from "../../../../../../../utils/Notifier";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import FontAwesomeIcon from "../../../../../../../components/FontAwesomeIcon/index.js";

function Index(props) {
  const [testName, setTestName] = useState("");
  const [Score, setScore] = useState("");
  const [testErrorMessage, settestErrorMessage] = useState("");
  const [scoreErrorMessage, setscoreErrorMessage] = useState("");

  const [show, sethide] = useState(false);

  const handleTestName = (name, value) => {
    setTestName(value);
  };
  const handleScore = (e) => {
    const { value } = e.target;
    setScore(value);
  };
  const ScoreValidator = (value) => {
    if (testName && testName === "TOEFL") {
      if (value <= 120 && value > 0) {
        return true;
      } else {
        Notifier(
          "Score must be less than equal to 120 and greater than 0",
          "error"
        );
      }
    } else if (testName && testName === "GRE") {
      if (value > 0 && value <= 340) {
        return true;
      } else {
        Notifier(
          "Score must be less than equal to 170 and greater than 140",
          "error"
        );
      }
    } else if (testName && testName === "SAT") {
      if (value <= 800 && value > 200) {
        return true;
      } else {
        Notifier(
          "Score must be less than equal to 800 and greater than 200",
          "error"
        );
      }
    } else if (testName && testName === "GMAT") {
      if (value <= 800 && value > 200) {
        return true;
      } else {
        Notifier(
          "Score must be less than equal to 800 and greater than 200",
          "error"
        );
      }
    } else if (testName && testName === "PTE") {
      if (value <= 90 && value > 10) {
        return true;
      } else {
        Notifier(
          "Score must be less than equal to 90 and greater than 10",
          "error"
        );
      }
    } else if (testName && testName === "IELTS") {
      if (value <= 9 && value > 0) {
        return true;
      } else {
        Notifier(
          "Score must be less than equal to 9 and greater than 0",
          "error"
        );
      }
    } else if (testName && testName === "Duo Lingo") {
      if (value <= 160 && value > 10) {
        return true;
      } else {
        Notifier(
          "Score must be less than equal to 160 and greater than 10",
          "error"
        );
      }
    }
  };
  const handleAdd = () => {
    if (!testName) {
      settestErrorMessage("Required");
      return;
    } else {
      settestErrorMessage("");
    }
    if (!Score) {
      setscoreErrorMessage("Required");
      return;
    } else {
      setscoreErrorMessage("");
    }
    let check = ScoreValidator(Score);
    if (check === true) {
      let scores = {};
      scores.score = Score;
      scores.testName = testName;
      props.onAdd(scores, sethide);
      setTestName("");
      setScore("");
    }
  };

  return (
    <div>
      <form className="w-full ">
        <div className="2xl:flex 2xl:flex-row xl:flex xl:flex-row lg:flex lg:flex-row md:flex md:flex-row no-wrap sm:flex sm:flex-col -mx-3 mb-0">
          <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
            <label
              className="inputLabel block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-first-name"
            >
              Test
            </label>
            <AutoComplete
              data={TestScores}
              name="testName"
              label="Select Test"
              onChange={handleTestName}
              value={testName}
            />
            {testErrorMessage ? (
              <small className="text-danger">{testErrorMessage}</small>
            ) : (
              ""
            )}
          </div>
          <div className="w-full md:w-1/2 px-3">
            <label
              className="inputLabel block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-last-name"
            >
              Score
            </label>
            <input
              className="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
              type="number"
              min="0"
              step="0.1"
              type="number"
              name="score"
              onChange={handleScore}
              value={Score}
              placeholder="Enter score"
            />
            {scoreErrorMessage ? (
              <small className="text-danger">{scoreErrorMessage}</small>
            ) : (
              ""
            )}
          </div>
          <div className="w-full md:w-1/2 px-3  lg:py-6 md:py-0  ">
            <button
              // disabled={activeStep === 0}
              onClick={handleAdd}
              type="button"
              className="no-icon default-btn AddButton lg:mt-1 md:mt-1 sm:mt-3 sm:py-6"
            >
              Add<span></span>
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}
export const MemoizedIndex = React.memo(Index);
export default MemoizedIndex;
