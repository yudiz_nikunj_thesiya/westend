import React, { useState, useEffect } from "react";
import MemoizedIndexs from "./MemoDocuments";
import { DocumentList } from "../../../../../../../utils/Globals";
import Notifier from "../../../../../../../utils/Notifier";
import baseUrl from "../../../../../../../utils/baseUrl";
import Axios from "axios";

import { getUser, getToken } from "../../../../../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
export default function Index(props) {
  const {
    handleBack,
    handleNext,
    completedSteps,
    totalSteps,
    activeStep,
    steps,
  } = props;

  const [loading, setloading] = useState(false);

  const [data, setData] = useState("");
  const [documentList, setDocumentList] = useState(DocumentList);

  const getCourseDetails = async () => {
    if (props.data && props.data.appDocument) {
      let data = props.data.appDocument;
      if (data && data.length > 0) {
        let newArray = mergeArrayObjects(documentList, data);
        if (newArray && newArray.length > 0) {
          setDocumentList(newArray);
        }
      }
      setData(data);
    } else {
      if (loggedInUser) {
        let _id = loggedInUser.id;
        if (props.studentID) {
          _id = props.studentID;
        }
        try {
          let response = await Axios({
            method: "get",
            url: `${baseUrl}/student/getApplication/${_id}`,

            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          let data = response.data.data.appDocument;
          let newArray = mergeArrayObjects(documentList, data);
          if (newArray && newArray.length > 0) {
            setDocumentList(newArray);
          }
          console.log(newArray, "newArray");

          setData(data);
        } catch (error) {
          if (error.response)
            console.log(error.response.data.message, "error ");
        }
      }
    }
  };
  useEffect(() => {
    getCourseDetails();
  }, [props.data, activeStep]);
  const handleAdd = (formData) => {
    onAdd(formData);
  };
  const onAdd = async (scores) => {
    if (loggedInUser) {
      setloading(true);
      let _id = loggedInUser.id;
      if (props.studentID) {
        _id = props.studentID;
      }
      const form_data = new FormData();
      form_data.append("testName", scores.testName);
      form_data.append("file", scores.file);

      try {
        let response = await Axios({
          method: "post",
          url: `${baseUrl}/student/createDocument/${_id}`,
          data: form_data,
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        getCourseDetails();
        setloading(false);

        let data = response.data.data.appDocument;
        let newArray = mergeArrayObjects(documentList, data);
        if (newArray && newArray.length > 0) {
          setDocumentList((newArray) => newArray);
        }
        console.log(newArray, "newArray");
        setData(data);
        // props.setData(response.data.data);
        Notifier(response.data.message, "success");

        // router.push("/profile");
      } catch (err) {
        setloading(false);

        if (err.response) {
          Notifier(err.response.data.message, "error");
        }
      }
    } else {
      setloading(false);

      Notifier("you are not loggedIn", "error");
    }
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      if (loggedInUser) {
        let _id = loggedInUser.id;
        if (props.studentID) {
          _id = props.studentID;
        }
        let response = await Axios({
          method: "put",
          url: `${baseUrl}/student/submitApplication/${_id}`,
          data: { app_status: "Application Submitted" },
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        Notifier(response.data.message, "success");
        handleNext();
      }
    } catch (err) {
      if (err.response) {
        Notifier(err.response.data.message, "error");
      }
    }
  };

  function mergeArrayObjects(arr1, arr2) {
    var newArray = arr1.map(
      (obj) =>
        arr2.find((item2) => item2.name.trim() === obj.name.trim()) || obj
    );
    return newArray;
  }
  return (
    <div>
      <div className="col-lg-12 text-center pb-2">
        <small className="text-danger">
          Submit the documents to get your application approved faster. You can
          upload jpeg, png or pdf file as a document.
        </small>
      </div>
      <form className="w-full ">
        {documentList &&
          documentList.length > 0 &&
          documentList.map((score, index) => (
            <div key={index} className="mt-2">
              <MemoizedIndexs onAdd={handleAdd} score={score} />
            </div>
          ))}

        <div className="row pt-2">
          <div className="col-lg-12">
            <div className="d-flex text-right">
              <button
                // disabled={activeStep === 0}
                onClick={handleBack}
                className="default-btn2  mr-3 "
              >
                Back<span></span>
              </button>

              {activeStep !== steps.length && (
                <button
                  onClick={handleSubmit}
                  className="no-icon default-btn  "
                >
                  Submit
                  <span></span>
                </button>
              )}
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}
