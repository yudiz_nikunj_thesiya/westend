import React, { useState } from "react";
import cookie from "js-cookie";
import FileSaver from "file-saver";
import imageUrl from "../../../../../../../utils/imageUrl";
import { Alert, Spinner } from "reactstrap";
import {
  faPhoneAlt,
  faHome,
  faCheckCircle,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Index(props) {
  const [testName, setTestName] = useState(props.score.name);
  const [File, setFile] = useState("");

  let cooky = cookie.get("user");
  let loggedInUser = cooky && JSON.parse(cooky);
  const handleTestName = (name, value) => {
    setTestName(value);
  };
  const handleFile = (e) => {
    let file = e.target.files[0];
    if (file.size > 5000000) {
      Notifier("Document size should be less than equal to 5mb", "error");
    } else {
      let values = {
        file: file,
        testName: testName,
      };
      setFile(file);
      props.onAdd(values);
    }
  };

  return (
    <div>
      <form className="w-full ">
        <div className="row">
          <div className="col-lg-4">
            <div className="w-full px-3 mb-6 md:mb-0">
              <input
                className="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                type="text"
                name="testName"
                label="Select Document"
                onChange={handleTestName}
                value={testName}
                disabled={true}
              />
            </div>
          </div>
          <div className="col-lg-4">
            <div className="w-full  px-3">
              <>
                <input
                  className="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                  type="file"
                  name="file"
                  onChange={handleFile}
                  accept="image/jpeg,image/png,application/pdf"
                />
                <small>
                  {File
                    ? JSON.stringify(File.name)
                    : props.score && props.score.fileName
                    ? JSON.stringify(props.score.fileName)
                    : null}
                </small>
              </>
            </div>
          </div>
          <div className="col-lg-4">
            <div className="d-flex justify-content-between">
              <div className="w-full md:w-1/2 px-3 py-6">
                {loggedInUser &&
                loggedInUser.role == "admin" &&
                props.score &&
                props.score.file ? (
                  <a
                    // disabled={activeStep === 0}
                    href={props.score.file}
                    disabled={props.score.file ? false : true}
                    download
                    className="no-icon default-btn  mt-1"
                  >
                    Download<span></span>
                  </a>
                ) : (
                  ""
                )}
              </div>
              <div className="w-full md:w-1/3 px-3 py-6">
                {props.score && props.score.fileName ? (
                  <h4 className="pt-2">
                    <FontAwesomeIcon icon={faCheckCircle} color="green" />
                  </h4>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}

export default Index;
