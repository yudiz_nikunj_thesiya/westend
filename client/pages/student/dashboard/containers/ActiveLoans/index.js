import React, { useEffect, useState } from "react";
import baseUrl from "../../../../../utils/baseUrl";
import Notifier from "../../../../../utils/Notifier";
import Table2 from "../../../../../components/Table/activeLoans.js";
import { ActiveLoansheadCells } from "../../../../../utils/Globals/index.js";
import Axios from "axios";

import { Store } from "../../../../../Context";
import cookie from "js-cookie";
import { getUser, getToken } from "../../../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
export default function Index(props) {
  const [data, setData] = useState([]);

  const getCourseDetails = async () => {
    if (loggedInUser) {
      let _id = loggedInUser.id;
      if (props.studentID) {
        _id = props.studentID;
      }
      try {
        let response = await Axios({
          method: "get",
          url: `${baseUrl}/student/getApplication/${_id}`,
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        let dataObj = response.data.data.data;
        let array = [];
        array.push(dataObj);
        setData(array);
      } catch (error) {
        if (error.response) console.log(error.response.data.message, "error ");
      }
    }
  };
  console.log("data");

  useEffect(() => {
    getCourseDetails();
  }, [props.setActiveTab]);
  return (
    <div className=" text-center">
      {data && data.length > 0 ? (
        <Table2
          data={data}
          // handleEdit={editToggle}
          // handleDelete={handleDelete}
          headCells={ActiveLoansheadCells}
        />
      ) : (
        <h6 className="text-center">No Active Loans Yet</h6>
      )}
    </div>
  );
}
