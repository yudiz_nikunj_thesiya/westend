import React from "react";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemPanel,
  AccordionItemButton,
} from "react-accessible-accordion";
import { resetIdCounter, Tab, Tabs, TabList, TabPanel } from "react-tabs";
export default function index() {
  return (
    <div>
      <div className="faq-accordion p-2">
        <Accordion allowZeroExpanded preExpanded={["a"]}>
          <AccordionItem uuid="a">
            <AccordionItemHeading>
              <AccordionItemButton>
                Who is eligible for an education loan?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
                Every bank has its own eligibility criteria for dispensing education loans, but a few common parameters are:

              </p>
              <ul class="features-list">
                <li><i class="bx bx-badge-check"></i> Applicant should be an Indian national.</li>
                <li><i class="bx bx-badge-check"></i>Applicant should have confirmed admission in a college/educational institution at the time the loan application is made.</li>
                <li><i class="bx bx-badge-check"></i>Applicant should be in the age bracket 16–35 years.</li>
                <li><i class="bx bx-badge-check"></i>The Applicant should have a co-borrower such as a parent who acts as guarantor for the loan</li>

              </ul>
            </AccordionItemPanel>
          </AccordionItem>

          <AccordionItem uuid="b">
            <AccordionItemHeading>
              <AccordionItemButton>
                What documents does the lending bank ask for?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
                Documents that are to be submitted with the education loan application include:
              </p>
              <ul class="features-list">
                <li><i class="bx bx-badge-check"></i> Admission confirmation letters.</li>
                <li><i class="bx bx-badge-check"></i>Schedule of expenses for the course including tuition fees and other expenses.</li>
                <li><i class="bx bx-badge-check"></i>Score sheet of qualifying test(s).</li>
                <li><i class="bx bx-badge-check"></i>Copies of foreign exchange permit/student visa for overseas studies.</li>

                <li><i class="bx bx-badge-check"></i>Bank account statements for last 6 months. (Can be a joint account with parent/guardian)</li>
                <li><i class="bx bx-badge-check"></i>Statement of assets and liabilities of borrower.</li>
                <li><i class="bx bx-badge-check"></i>Proof of age</li>
                <li><i class="bx bx-badge-check"></i>Proof of residence</li>
                <li><i class="bx bx-badge-check"></i>2 passport size photographs</li>
                <li><i class="bx bx-badge-check"></i>Documents related to collateral required for education loans exceeding Rs. 4 lakhs.</li>

              </ul>
            </AccordionItemPanel>
          </AccordionItem>

          <AccordionItem uuid="c">
            <AccordionItemHeading>
              <AccordionItemButton>
                What are the courses for which an education loan can be availed?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
                Education loan can be availed for all courses recognized by the UGC in case of India and all regular courses abroad. Each lender has its own list of educational institutions and courses that they would provide a loan for whether overseas or within India.
              </p>

            </AccordionItemPanel>
          </AccordionItem>

          <AccordionItem uuid="d">
            <AccordionItemHeading>
              <AccordionItemButton>
                What are the expenses covered by education loans?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
                Education loans can be used for paying:
              </p>
              <ul class="features-list">
                <li><i class="bx bx-badge-check"></i> Tuition fees and hostel expenses</li>
                <li><i class="bx bx-badge-check"></i>Exam, library, and lab fees if applicable</li>
                <li><i class="bx bx-badge-check"></i>Any refundable caution deposits paid to the educational institute</li>
                <li><i class="bx bx-badge-check"></i>Cost of books, uniforms, and other essentials for completion of course</li>

                <li><i class="bx bx-badge-check"></i>Travel expenses (return fare for international flights.</li>
                <li><i class="bx bx-badge-check"></i>Cost of books, uniforms, and other essentials for completion of course</li>
                <li><i class="bx bx-badge-check"></i>Travel expenses (return fare for international flights.</li>

              </ul>
            </AccordionItemPanel>
          </AccordionItem>

          <AccordionItem uuid="e">
            <AccordionItemHeading>
              <AccordionItemButton>
                Is a co-applicant needed for Indian Banks?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
                Yes, for all full-time courses a co applicant is required. The co-applicant can be Parent/Guardian or Spouse (if-married).
              </p>
            </AccordionItemPanel>
          </AccordionItem>

          <AccordionItem uuid="e">
            <AccordionItemHeading>
              <AccordionItemButton>
                Is a co-applicant needed for Indian Banks?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
                Yes, for all full-time courses a co applicant is required. The co-applicant can be Parent/Guardian or Spouse (if-married).
              </p>
            </AccordionItemPanel>
          </AccordionItem>

          <AccordionItem uuid="e">
            <AccordionItemHeading>
              <AccordionItemButton>
                Is a co-applicant needed for International Banks?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
              No, Co-Applicant is not required for International banks .
              </p>
            </AccordionItemPanel>
          </AccordionItem>
          <AccordionItem uuid="e">
            <AccordionItemHeading>
              <AccordionItemButton>
              Who can be my co-borrowers?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
              Your parents/ guardian can be a co-borrower as well as spouse in case of a married applicant. The co-borrower should have a steady source of income and a good credit history in order to improve the applicant’s chances of a successful loan application.
              </p>
            </AccordionItemPanel>
          </AccordionItem>


{/*  */}
<AccordionItem uuid="e">
            <AccordionItemHeading>
              <AccordionItemButton>
              Is there a maximum limit on an education loan that a lender can disburse?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
              The maximum limit on education loans for overseas studies is between Rs 20 lakhs to Rs50 lakhs. In case of some lenders, the applicant can get a higher loan amount sanctioned provided bank-accepted collateral is provided.
              </p>
            </AccordionItemPanel>
          </AccordionItem>



          <AccordionItem uuid="e">
            <AccordionItemHeading>
              <AccordionItemButton>
              What is the tenure of an education loan?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
              The average tenure for an education loan is between 5-7 years in case of most lenders. However, some lenders can offer a longer tenure of up to 15 years in case of higher loan amounts.
              </p>
            </AccordionItemPanel>
          </AccordionItem>



          <AccordionItem uuid="e">
            <AccordionItemHeading>
              <AccordionItemButton>
              What is the maximum loan amount one can get for overseas studies?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
              Banks generally fund up to 90% of the education expenses as loan and some even provide loans for 100% of education expense. However, the exact loan amount an applicant is eligible for depends on a range of factors including the monthly earning of the parent/legal guardian, the value of collateral provided, the academic record of the applicant and other factors.
              </p>
            </AccordionItemPanel>
          </AccordionItem>



          <AccordionItem uuid="e">
            <AccordionItemHeading>
              <AccordionItemButton>
              Is there a margin on education loans?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
              Most education loan providers do not charge a margin on education loans equal to or less than Rs. 4 lakhs. For higher loan amounts of up to 7.5 lakhs, banks and NBFCs have a margin of around 5% i.e. they provide loan amount equal to 95% of the course cost and the rest must be borne by the applicant. In case of higher loan amounts, the lender may decide to set margins on a case by case basis according to internal policies.
              </p>
            </AccordionItemPanel>
          </AccordionItem>

          <AccordionItem uuid="e">
            <AccordionItemHeading>
              <AccordionItemButton>
              Is it possible to get a loan without collateral OR Co-Signer?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
              Yes, Student can get a loan collateral OR Co-Signer also from International banks but there are some criteria that student has to fulfil.
              </p>
            </AccordionItemPanel>
          </AccordionItem>


          <AccordionItem uuid="e">
            <AccordionItemHeading>
              <AccordionItemButton>
               Is the loan disbursed in favour of the borrower or the institute/college/university?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
              The tuition and hostel fees are disbursed directly to the institute as per their schedule and fee structure usually in the form of a draft. Other components of the loan such as course-related expenses for lab equipment, laptop computer, uniform, travel expenses etc. may be claimed by and provided to the loan applicant.
              </p>
            </AccordionItemPanel>
          </AccordionItem>

          <AccordionItem uuid="e">
            <AccordionItemHeading>
              <AccordionItemButton>
              What is a holiday/interest free/moratorium period? What if there is a break in the study?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
              It is usually either 6 months to 1 year after the successful completion of the course, or the time it takes the borrower to start working at a job, whichever is earlier.
              </p>
            </AccordionItemPanel>
          </AccordionItem>
          <AccordionItem uuid="e">
            <AccordionItemHeading>
              <AccordionItemButton>
              Do Education loans have any Income Tax benefits?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
              Loan borrowers can avail tax benefits on interest paid on education loan under Sec 80E of the Income Tax Act. This benefit is available over and above the Rs. 150,000 deduction allowed under Section 80C. Tax benefits can be availed once the borrower starts paying the interest on the education loan. Further, the deduction is available until the borrower pays off the full interest amount on the loan or for a maximum period of 8 years, whichever is earlier.
              </p>
            </AccordionItemPanel>
          </AccordionItem>
          <AccordionItem uuid="e">
            <AccordionItemHeading>
              <AccordionItemButton>
              Is there a prepayment penalty on education loan?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
              Most banks do not levy prepayment penalty on education loan. However, it is best to check with the bank before signing the dotted line.
              </p>
            </AccordionItemPanel>
          </AccordionItem>
          <AccordionItem uuid="e">
            <AccordionItemHeading>
              <AccordionItemButton>
              Is it necessary to have an account with the bank to avail of an educational loan? 
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
              Now it is no longer a mandatory requirement. If you have an account with the particular bank, it usually becomes easier to get the loan sanctioned. This is because of your prior relationship with the bank, your past financial records and transactions can be analysed faster to make a decision.
              </p>
            </AccordionItemPanel>
          </AccordionItem>
          <AccordionItem uuid="e">
            <AccordionItemHeading>
              <AccordionItemButton>
              Do banks offer an education loan in foreign exchange? I need to pay in dollars for my studies, but the bank is in India.
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
              The loan availed by the student is paid to the educational institution directly in the local currency (Dollar/Pounds/Euro etc.). The lender may charge an additional currency conversion fee as per RBI regulations on the loan amount disbursed.
              </p>
            </AccordionItemPanel>
          </AccordionItem>
          <AccordionItem uuid="e">
            <AccordionItemHeading>
              <AccordionItemButton>
              Is any incentive available for a girl student applying for education loan?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
              Banks generally provide a 0.5% concession on the applicable education loan interest rate to girl students pursuing higher education in India and abroad.
              </p>
            </AccordionItemPanel>
          </AccordionItem>
          <AccordionItem uuid="e">
            <AccordionItemHeading>
              <AccordionItemButton>
              Is credit score important in case of education loan?
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>
              A majority of students applying for an education loan do not have previous credit history such as other loans or credit cards. Thus, education loan specialized lenders have a specialized credit scoring model that scores education loan application based on the University, College and the Course of admission. They factor the academic background of the student as well as the credit history of the co-borrower into their decision.
              </p>
            </AccordionItemPanel>
          </AccordionItem>
     

        </Accordion>
      </div>
    </div>
  );
}
