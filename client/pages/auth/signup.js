import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import baseUrl from "../../utils/baseUrl";
import PhoneInput from "../../components/CustomPhoneInput.js";

import Axios from "axios";
import Notifier from "../../utils/Notifier";

export default function Signup(props) {
  const [isloading, setisloading] = React.useState(false);

  const signup_formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",

      email: "",
      phone: "",
    },
    validationSchema: SIGNUP_YUP,
    onSubmit: async (values) => {
      setisloading(true);
      try {
        let response = await Axios.post(`${baseUrl}/auth/signup`, values);
        setisloading(false);
        props.loginToggle();
        props.verifyotpToggle();
        Notifier(response.data.message, "success");
      } catch (err) {
        setisloading(false);

        Notifier(err.response.data.message, "error");
      }
    },
  });

  const onNumberChange = (value) => {
    signup_formik.setFieldValue("phone", value);
  };
  return (
    <div>
      <form onSubmit={signup_formik.handleSubmit}>
        <div className="flex  -mx-3 mb-2">
          <div className="w-full px-3 mb-6 md:mb-0">
            {/* <label htmlFor="username">Username</label> */}
            <input
              type="text"
              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
              placeholder="First Name"
              id="firstName"
              name="firstName"
              onChange={signup_formik.handleChange}
              value={signup_formik.values.firstName}
            />
            <div className="text-danger pt-1">
              {signup_formik.touched.firstName &&
              signup_formik.errors.firstName ? (
                <div className="formikError">
                  {signup_formik.errors.firstName}
                </div>
              ) : null}
            </div>
          </div>
        </div>
        <div className="flex no-wrap -mx-3 mb-2">
          <div className="w-full  px-3 mb-6 md:mb-0">
            {/* <label htmlFor="username">Username</label> */}
            <input
              type="text"
              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
              placeholder="Last Name"
              id="lastName"
              name="lastName"
              onChange={signup_formik.handleChange}
              value={signup_formik.values.lastName}
            />
            <div className="text-danger pt-1">
              {signup_formik.touched.lastName &&
              signup_formik.errors.lastName ? (
                <div className="formikError">
                  {signup_formik.errors.lastName}
                </div>
              ) : null}
            </div>
          </div>
        </div>
        <div className="flex no-wrap -mx-3 mb-2">
          <div className="w-full px-3 mb-6 md:mb-0">
            {/* <label htmlFor="password">Password</label> */}
            <input
              type="email"
              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
              placeholder="Email"
              id="emails"
              name="email"
              onChange={signup_formik.handleChange}
              value={signup_formik.values.email}
            />
            <div className="text-danger pt-1">
              {signup_formik.touched.email && signup_formik.errors.email ? (
                <div className="formikError">{signup_formik.errors.email}</div>
              ) : null}
            </div>
          </div>
        </div>
        <div className="flex no-wrap -mx-3 mb-2">
          <div className="w-full  px-3 mb-6 md:mb-0">
            {/* <label htmlFor="password">Password</label> */}

            <PhoneInput
              value={signup_formik.values.phone}
              onChange={onNumberChange}
            />
            <div className="text-danger pt-1">
              {signup_formik.touched.phone && signup_formik.errors.phone ? (
                <div className="formikError">{signup_formik.errors.phone}</div>
              ) : null}
            </div>
          </div>
        </div>
        <button type="submit" className="default-btn btn-block">
          {isloading ? "Signing Up..." : "Sign Up"} <span></span>
        </button>
      </form>

      <div className="col-lg-12 pb-1 pt-1 text-center">
        <span className="text-center"></span>
      </div>
      <button
        type="submit"
        onClick={() => {
          props.setLoginState(!props.loginState);
        }}
        className="no-icon default-btn btn-block"
      >
        {props.loginState ? "New Student Signup" : "Login"} <span></span>
      </button>
      <div className="col-lg-12 text-center">
        <small>By singing up, you agree to our terms and privacy policy.</small>
      </div>
    </div>
  );
}

const SIGNUP_YUP = Yup.object({
  firstName: Yup.string().required("Required"),
  lastName: Yup.string().required("Required"),
  email: Yup.string().email("Invalid email address").required("Required"),

  phone: Yup.string()
    .min(10, "Must 10 characters long")
    // .max(15, "Must be equal to 10 characters ")
    .required("Required"),
});
