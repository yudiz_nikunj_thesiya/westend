import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import baseUrl from "../utils/baseUrl";
import { useRouter } from "next/router";
import Axios from "axios";
import Notifier from "../utils/Notifier";
import { handleLogin } from "../utils/auth";

export default function Index(props) {
  const [isloading, setisloading] = React.useState(false);

  const formik = useFormik({
    initialValues: {
      otp: "",
    },
    validationSchema: LOGIN_YUP,
    onSubmit: async (values) => {
      setisloading(true);

      try {
        let response = await Axios({
          method: "post",
          url: `${baseUrl}/auth/verifyOtp`,
          data: { otp: values.otp },
        });
        Notifier(response.data.message, "success");
        setisloading(false);
        props.toggle();

        const { token, user } = response.data.data;
        let path;
        if (user.role === "student") {
          path = "/student/dashboard";
        } else if (user.role === "admin") {
          path = "/admin/dashboard";
        } else {
          path = "/partner/dashboard";
        }
        handleLogin(token, user, path);

        formik.handleReset();
      } catch (err) {
        Notifier(err.response.data.message, "error");
        setisloading(false);
      }
    },
  });
  const handleChange = (e) => {
    let { name, value } = e.target;
    formik.setFieldValue(name, value.replace(/\D/g, ""));
  };
  return (
    <div className="otpLogin">
      <div className="loginContent">
        <div className="container">
          <div className="row">
            <div className="col-md-12 loginContents">
              <div className="row justify-loginContent-center">
                <div className="col-lg-8 offset-md-2">
                  <div className="col-lg-12 text-center pb-2">
                    <div className="mb-4">
                      <h3>Verify OTP</h3>
                    </div>
                  </div>

                  <form onSubmit={formik.handleSubmit}>
                    <>
                      <div className="form-group first">
                        {/* <label htmlFor="username">Username</label> */}
                        <input
                          type="text"
                          className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                          placeholder="Enter received OTP"
                          id="otp"
                          name="otp"
                          onChange={handleChange}
                          value={formik.values.otp}
                        />
                        <div className="text-danger pt-1">
                          {formik.touched.otp && formik.errors.otp ? (
                            <div className="formikError">
                              {formik.errors.otp}
                            </div>
                          ) : null}
                        </div>
                      </div>
                    </>

                    <button type="submit" className="default-btn btn-block">
                      {isloading ? "SUBMITTING..." : "SUBMIT"}
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
const LOGIN_YUP = Yup.object({
  otp: Yup.number().required("Required"),
});
