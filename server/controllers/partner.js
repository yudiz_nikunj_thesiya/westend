//Login User
const STRINGS = require("../utils/texts");
const CustomError = require("../utils/customError");
const response = require("../utils/response");
const bcrypt = require("bcryptjs");
const JWT = require("jsonwebtoken");
const prisma = require("../prisma/");
const MailService = require("../services/mail.service");
const crypto = require("crypto");
const passwordExpires = Date.now() + 48 * (60 * 60 * 1000) + "";
const moment = require("moment");

class PartnerContoller {
  async signup(req, res) {
    try {
      let data = req.body;
      data.role = "partner";
      let Emailuser = await prisma.user.findFirst({
        where: {
          email: data.email,
        },
      });
      let Phoneuser = await prisma.user.findFirst({
        where: {
          phone: data.phone,
        },
      });
      if (Emailuser) {
        throw new CustomError(res, STRINGS.ERRORS.userAlreadyExists, 404);
      } else if (Phoneuser) {
        throw new CustomError(res, STRINGS.ERRORS.phoneAlreadyExists, 404);
      } else {
        // let tempNum = Math.floor(100000 + Math.random() * 90000000);
        // let num = tempNum.toString();
        // const salt = await bcrypt.genSalt(15);
        // const hash = await bcrypt.hash(num, salt);
        // data.password = hash;
        const buf = await crypto.randomBytes(20);
        var passwordToken = buf.toString("hex");

        data["passwordToken"] = passwordToken;
        data["passwordExpires"] = passwordExpires;
        let user = await prisma.user.create({ data: data });
        const token = await JWT.sign(
          { id: user.id },
          "SOMEThingReallyTRicky1345"
        );
        const result = {
          user,
          token,
        };

        let emailService = new MailService();
        await emailService.sendEmail(user, passwordToken);
        res
          .status(200)
          .send(response(STRINGS.TEXTS.partnerCreated, result, 200));
      }
    } catch (err) {
      res.send(response(err.message, 400));
    }
  }
  // get all partner students
  async getAllStudents(req, res) {
    try {
      let userId = Number(req.params.id);
      let user = await prisma.user.findFirst({
        where: {
          id: userId,
        },
      });
      if (user) {
        let users = await prisma.user.findMany({
          where: {
            AND: [
              {
                role: "student",
              },
              {
                partnerId: userId,
              },
            ],
          },
        });
        let app = await prisma.application.findMany({
          where: {
            userId: userId,
          },
        });

        res.status(200).send(response(STRINGS.TEXTS.allStudents, users));
      } else {
        res.status(400).send(response(STRINGS.TEXTS.userNotFound));
      }
    } catch (err) {
      res.status(400).send(response(err.message));
    }
  }
  //   search student by date
  async searchByDate(req, res) {
    try {
      let date = req.query.date;
      let userId = Number(req.query.id);

      let result = await prisma.user.findMany({
        where: {
          AND: [
            {
              role: "student",
            },
            {
              partnerId: userId,
            },
          ],
        },
      });
      let split = date.split(" ");
      let date1 = split[0];
      let date2 = split[2];
      let array = [];
      var startDate = moment(date1, "DD/MM/YYYY");
      var endDate = moment(date2, "DD/MM/YYYY");

      result.map((dt) => {
        var date = moment(new Date(dt.createdAt), "DD/MM/YYYY");

        if (
          date.isSameOrAfter(startDate, "day") &&
          date.isSameOrBefore(endDate, "day")
        ) {
          array.push(dt);
        }
      });

      res.status(200).send(response(STRINGS.TEXTS.getUser, array));
    } catch (err) {
      console.log("Error--->", err.message);
      res.status(400).send(response(err.message));
    }
  }
  //   create partner student
  async createStudent(req, res) {
    try {
      let data = req.body;
      let updatedUser;
      let Emailuser = await prisma.user.findFirst({
        where: {
          email: data.email,
        },
      });
      let Phoneuser = await prisma.user.findFirst({
        where: {
          phone: data.phone,
        },
      });
      if (Emailuser) {
        throw new CustomError(res, STRINGS.ERRORS.userAlreadyExists, 404);
      } else if (Phoneuser) {
        throw new CustomError(res, STRINGS.ERRORS.phoneAlreadyExists, 404);
      } else {
        let _id = Number(req.params.id);
        let tempuser = await prisma.user.findFirst({
          where: {
            id: _id,
          },
        });
        data.role = "student";

        data["partner_name"] = tempuser.company_name;
        data["partnerId"] = tempuser.id;
        const buf = await crypto.randomBytes(20);
        var resetPasswordToken = buf.toString("hex");
        data["passwordToken"] = resetPasswordToken;
        updatedUser = await prisma.user.create({ data: data });
        let newId = Number(updatedUser.id);
        await prisma.application.create({
          data: {
            userId: newId,
            //   basic
            firstName: updatedUser.firstName,
            lastName: updatedUser.lastName,
            phone: updatedUser.phone,
            date: updatedUser.createdAt,
          },
        });
        let users = await prisma.user.findMany({
          where: {
            AND: [
              {
                role: "student",
              },
              {
                partnerId: _id,
              },
            ],
          },
        });

        res
          .status(200)
          .send(response(STRINGS.TEXTS.StudentCreatedByAdmin, users));

        let emailService = new MailService();
        await emailService.sendEmail(updatedUser, updatedUser.passwordToken);
      }
      // let emailService = new MailService();
      // await emailService.sendEmail(email, updatedUser);
    } catch (err) {
      console.log(err.message);
      res.status(400).send(response(err.message));
    }
  }
  // *****

  // update Student

  async updateStudent(req, res) {
    try {
      let _id = Number(req.params.id);
      let { firstName, lastName, email, phone } = req.body;

      const user = await prisma.user.findFirst({
        where: {
          id: _id,
        },
      });
      if (user) {
        let updatedUser = await prisma.user.update({
          where: {
            id: _id,
          },
          data: {
            firstName: firstName,
            lastName: lastName,
            email: email,
            phone: phone,
          },
        });
        let partnerId = Number(req.body.partnerId);
        let users = await prisma.user.findMany({
          where: {
            AND: [
              {
                role: "student",
              },
              {
                partnerId: partnerId,
              },
            ],
          },
        });
        res.status(200).send(response(STRINGS.TEXTS.partnerUpdated, users));
      } else {
        res.status(404).send(response(STRINGS.ERRORS.userNotFound));
      }
    } catch (err) {
      res.status(400).send(response(err.message));
    }
  }
  // *************
  // delete student
  async deleteStudent(req, res) {
    try {
      let _id = JSON.parse(req.params.id);
      let user = await prisma.user.findFirst({
        where: {
          id: _id,
        },
      });
      if (!user) {
        res.status(404).send(response(STRINGS.ERRORS.userNotFound));
      }
      await prisma.user.delete({
        where: {
          id: _id,
        },
      });
      let userId = user.id;
      let users = await prisma.user.findMany({
        where: [
          {
            role: "student",
          },
          {
            partnerId: userId,
          },
        ],
      });
      res.status(200).send(response(STRINGS.TEXTS.studentDeleted, users));
    } catch (err) {
      console.log(err.message);
      res.status(400).send(response(err.message));
    }
  }

  // *****

  //   search student
  async getsearchStudent(req, res) {
    try {
      let partnerId = Number(req.query.id);
      const { q, date } = req.query;
      let email = req.query.email == "true" ? "true" : "false";
      let name = req.query.name == "true" ? "true" : "false";
      let phone = req.query.phone == "true" ? "true" : "false";
      let result = [];
      // **********************
      if (email == "true" && phone == "true" && name == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                email: {
                  contains: q,
                  mode: "insensitive",
                },
              },
              {
                phone: {
                  contains: q,
                  mode: "insensitive",
                },
              },
              {
                firstName: {
                  contains: q,
                  mode: "insensitive",
                  //
                },
              },
            ],
            AND: [
              {
                role: "student",
              },
              {
                partnerId: partnerId,
              },
            ],
          },
        });
      } else if (phone == "true" && email == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                email: {
                  contains: q,
                  mode: "insensitive",
                },
              },
              {
                phone: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
            AND: [
              {
                role: "student",
              },
              {
                partnerId: partnerId,
              },
            ],
          },
        });
      } else if (name == "true" && email == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                email: {
                  contains: q,
                  mode: "insensitive",
                },
              },
              {
                firstName: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
            AND: [
              {
                role: "student",
              },
              {
                partnerId: partnerId,
              },
            ],
          },
        });
      } else if (name == "true" && email == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                firstName: {
                  contains: q,
                  mode: "insensitive",
                },
              },
              {
                phone: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
            AND: [
              {
                role: "student",
              },
              {
                partnerId: partnerId,
              },
            ],
          },
        });
      } else if (name == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                firstName: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
            AND: [
              {
                role: "student",
              },
              {
                partnerId: partnerId,
              },
            ],
          },
        });
      } else if (email == "true") {
        result = await prisma.user.findMany({
          where: {
            AND: [
              {
                role: "student",
              },
              {
                email: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
          },
        });
      } else if (phone == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                phone: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
            AND: [
              {
                role: "student",
              },
              {
                partnerId: partnerId,
              },
            ],
          },
        });
      }

      if (date) {
        let split = date.split(" ");
        let date1 = split[0];
        let date2 = split[2];
        let array = [];
        var startDate = moment(date1, "DD/MM/YYYY");
        var endDate = moment(date2, "DD/MM/YYYY");
        result.map((dt) => {
          var date = moment(new Date(dt.createdAt), "DD/MM/YYYY");

          if (
            date.isSameOrAfter(startDate, "day") &&
            date.isSameOrBefore(endDate, "day")
          ) {
            array.push(dt);
          }
        });

        return res.status(200).send(response(STRINGS.TEXTS.getUser, array));
      }

      res.status(200).send(response(STRINGS.TEXTS.getUser, result));
    } catch (err) {
      res.status(400).send(response(err.message));
    }
  }

  //  createPartnerLead
  async createPartnerLead(req, res) {
    try {
      let data = req.body;
      let token = data.verifyPartnerToken;
      token = token.split('&&')[0];
      let updatedUser = await prisma.user.findFirst({
        where: {
          AND: [
            {
              verifyPartnerToken: token,
            },
            {
              role: "partner",
            },
          ],
        },
      });
      if (!updatedUser)
        return res.status(400).send(response(STRINGS.ERRORS.tokenInvalid));
      let Emailuser = await prisma.user.findFirst({
        where: {
          email: data.email,
        },
      });
      let Phoneuser = await prisma.user.findFirst({
        where: {
          phone: data.phone,
        },
      });
      if (Emailuser) {
        throw new CustomError(res, STRINGS.ERRORS.userAlreadyExists, 404);
      } else if (Phoneuser) {
        throw new CustomError(res, STRINGS.ERRORS.phoneAlreadyExists, 404);
      } else {
        const buf = await crypto.randomBytes(20);
        var passwordToken = buf.toString("hex");
        data["passwordToken"] = passwordToken;
        data["partner_name"] = updatedUser.company_name;
        data["partnerId"] = updatedUser.id;
        updatedUser = await prisma.user.create({ data: data });
        let newId = Number(updatedUser.id);
        let users = await prisma.user.findMany({
          where: {
            role: "student",
          },
        });
        await prisma.application.create({
          data: {
            userId: newId,
            firstName: updatedUser.firstName,
            lastName: updatedUser.lastName,
            phone: updatedUser.phone,
            date: updatedUser.createdAt,
          },
        });
        let emailService = new MailService();
        await emailService.sendEmail(updatedUser, updatedUser.passwordToken);

        res
          .status(200)
          .send(response(STRINGS.TEXTS.StudentCreatedByAdmin, users));
      }
    } catch (err) {
      console.log(err.message);
      res.status(400).send(response(err.message));
    }
  }
  // ***
  // get partner by partnerToken

  async getPartner(req, res) {
    try {
      let check = await prisma.user.findFirst({
        where: {
          AND: [
            {
              verifyPartnerToken: req.query.token,
            },
            {
              role: "partner",
            },
          ],
        },
      });
      if (!check)
        return res.status(404).send(response(STRINGS.ERRORS.tokenInvalid));
      res.status(200).send(response(STRINGS.TEXTS.getUser, check));
    } catch (error) {
      console.log("err->", error.message);
      res.status(500).send(response(error.message));
    }
  }
}

module.exports = new PartnerContoller();
