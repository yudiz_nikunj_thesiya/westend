//Login User
const STRINGS = require("../utils/texts");
const CustomError = require("../utils/customError");
const response = require("../utils/response");
const { GeneratePdf } = require("../utils/generatePdf");
const handlebars = require("handlebars");
const fs = require("fs");
const path = require("path");
const prisma = require("../prisma/");
const MailService = require("../services/mail.service");
var admin = require("firebase-admin");

const bcrypt = require("bcryptjs");
const JWT = require("jsonwebtoken");
const crypto = require("crypto");
const puppeteer = require("puppeteer");
const moment = require("moment");
const passwordExpires = Date.now() + 48 * (60 * 60 * 1000) + "";

class AdminContoller {
  async getAllUsers(req, res) {
    try {
      let role = req.params.role;
      let users = await prisma.user.findMany({
        where: {
          role: role,
        },
      });

      res.status(200).send(response(STRINGS.TEXTS.allStudents, users));
    } catch (err) {
      res.status(200).send(response(err.message));
    }
  }
  //get all Applicayions
  async getAllApplications(req, res) {
    try {
      let applications = await prisma.application.findMany({});
      res.status(200).send(response(STRINGS.TEXTS.allStudents, applications));
    } catch (err) {
      console.log(err.message, "err message");
      res.status(400).send(response(err.message));
    }
  }

  //   create partner
  async createPartner(req, res) {
    try {
      let data = req.body;
      data.role = "partner";
      let Emailuser = await prisma.user.findFirst({
        where: {
          email: data.email,
        },
      });

      let Phoneuser = await prisma.user.findFirst({
        where: {
          phone: data.phone,
        },
      });
      if (Emailuser) {
        throw new CustomError(res, STRINGS.ERRORS.userAlreadyExists, 404);
      } else if (Phoneuser) {
        throw new CustomError(res, STRINGS.ERRORS.phoneAlreadyExists, 404);
      } else {
        const buf = await crypto.randomBytes(20);
        var passwordToken = buf.toString("hex");
        const salt = bcrypt.genSaltSync(15);
        const hash = await bcrypt.hash(data.password, salt);

        const bufs = await crypto.randomBytes(10);
        var verifyPartnerToken = bufs.toString("hex");
        data["verifyPartnerToken"] = verifyPartnerToken;
        data["passwordToken"] = passwordToken;
        data["passwordExpires"] = passwordExpires;
        data["password"] = hash;
        // data["approved"] = "approved";
        data["isVerify"] = true;

        let user = await prisma.user.create({ data: data });
        let partners = await prisma.user.findMany({
          where: {
            role: "partner",
          },
        });

        let emailService = new MailService();
        await emailService.sendEmailToPartner(user);
        res
          .status(200)
          .send(response(STRINGS.TEXTS.partnerCreatedByAdmin, partners));
      }
    } catch (err) {
      console.log(err.message, "err.message");
      res.send(response(err.message, 400));
    }
  }
  // *****

  //   create student
  async createStudent(req, res) {
    try {
      let data = req.body;
      let updatedUser;
      let Emailuser = await prisma.user.findFirst({
        where: {
          email: data.email,
        },
      });
      let Phoneuser = await prisma.user.findFirst({
        where: {
          phone: data.phone,
        },
      });
      if (Emailuser) {
        throw new CustomError(res, STRINGS.ERRORS.userAlreadyExists, 404);
      } else if (Phoneuser) {
        throw new CustomError(res, STRINGS.ERRORS.phoneAlreadyExists, 404);
      } else {
        const buf = await crypto.randomBytes(20);
        var passwordToken = buf.toString("hex");
        data["passwordToken"] = passwordToken;
        data["app_status"] = "Lead Created";
        updatedUser = await prisma.user.create({ data: data });
        let newId = Number(updatedUser.id);
        let users = await prisma.user.findMany({
          where: {
            role: "student",
          },
        });

        await prisma.application.create({
          data: {
            userId: newId,
            //   basic
            firstName: updatedUser.firstName,
            lastName: updatedUser.lastName,
            phone: updatedUser.phone,
            email: updatedUser.email,
            date: updatedUser.createdAt,
          },
        });
        // let emailService = new MailService();
        // await emailService.sendEmail(updatedUser, updatedUser.passwordToken);

        res
          .status(200)
          .send(response(STRINGS.TEXTS.StudentCreatedByAdmin, users));
      }
    } catch (err) {
      console.log(err.message);
      res.status(400).send(response(err.message));
    }
  }
  // *****

  // update Student

  async updateStudent(req, res) {
    try {
      let _id = JSON.parse(req.params.id);
      let { firstName, lastName, email, phone, app_status } = req.body;
      let comment = req.body.comment;
      const user = await prisma.user.findFirst({
        where: {
          id: _id,
        },
      });
      if (!user) {
        return res.status(404).send(response(STRINGS.ERRORS.userNotFound));
      }
      await prisma.user.update({
        where: {
          id: _id,
        },
        data: {
          firstName: firstName,
          lastName: lastName,
          email: email,
          phone: phone,
          app_status: app_status,
          comment: comment,
        },
      });
      let users = await prisma.user.findMany({
        where: {
          role: "student",
        },
      });

      res.status(200).send(response(STRINGS.TEXTS.StudentUpdated, users));
    } catch (err) {
      res.status(400).send(response(err.message));
    }
  }
  // *************

  // update Partner

  async updatePartner(req, res) {
    try {
      let _id = Number(req.params.id);
      let {
        firstName,
        lastName,
        email,
        phone,
        address,
        website,
        reason,
        company_name,
        password,
      } = req.body;
      const user = await prisma.user.findFirst({
        where: {
          id: _id,
        },
      });
      if (!user) {
        return res.status(404).send(response(STRINGS.ERRORS.userNotFound));
      }
      const salt = bcrypt.genSaltSync(15);

      const hash = await bcrypt.hash(password, salt);

      await prisma.user.update({
        where: {
          id: _id,
        },
        data: {
          firstName: firstName,
          lastName: lastName,
          email: email,
          phone: phone,
          address: address,
          website: website,
          reason: reason,
          company_name: company_name,
          password: hash,
        },
      });

      let partners = await prisma.user.findMany({
        where: {
          role: "partner",
        },
      });

      res.status(200).send(response(STRINGS.TEXTS.partnerUpdated, partners));
    } catch (err) {
      console.log(err.message);
      res.status(400).send(response(err.message));
    }
  }

  // *************

  //   delete partner
  async deletePartner(req, res) {
    try {
      let _id = Number(req.params.id);
      let user = await prisma.user.findFirst({
        where: {
          id: _id,
        },
      });
      if (!user) {
        return res.status(404).send(response(STRINGS.ERRORS.userNotFound));
      }
      await prisma.user.delete({
        where: {
          id: _id,
        },
      });

      let partners = await prisma.user.findMany({
        where: {
          role: "partner",
        },
      });
      res.status(200).send(response(STRINGS.TEXTS.partnerDeleted, partners));
    } catch (err) {
      console.log(err.message);
      res.status(400).send(response(err.message));
    }
  }
  async deleteStudent(req, res) {
    try {
      let _id = Number(req.params.id);
      let user = await prisma.user.findFirst({
        where: {
          id: _id,
        },
      });
      if (!user) {
        return res.status(404).send(response(STRINGS.ERRORS.userNotFound));
      }

      let userId = user.id;
      let application = await prisma.application.findFirst({
        where: {
          userId: userId,
        },
      });
      let OTP = await prisma.otp.findFirst({
        where: {
          userId: userId,
        },
      });
      if (application) {
        let appId = application.id;
        await prisma.application.delete({
          where: {
            id: appId,
          },
        });
      }
      if (OTP) {
        let appId = OTP.id;
        await prisma.otp.delete({
          where: {
            id: appId,
          },
        });
      }
      await prisma.user.delete({
        where: {
          id: _id,
        },
      });
      let users = await prisma.user.findMany({
        where: {
          role: "student",
        },
      });
      res.status(200).send(response(STRINGS.TEXTS.studentDeleted, users));
    } catch (err) {
      res.status(400).send(response(err.message));
    }
  }

  // approve partner

  async approvePartner(req, res) {
    try {
      let data = req.body;
      await Promise.all(
        data.map(async (value) => {
          let _id = Number(value);
          await prisma.user.update({
            where: {
              id: _id,
            },
            data: {
              approved: "approved",
            },
          });
        })
      );

      let users = await prisma.user.findMany({
        where: {
          role: "partner",
        },
      });

      res.status(200).send(response(STRINGS.TEXTS.approvePartner, users));
    } catch (err) {
      res.status(400).send(response(err.message));
    }
  }

  //   search partner
  async getsearchPartner(req, res) {
    try {
      const { q, date } = req.query;
      let email = req.query.email == "true" ? "true" : "false";
      let name = req.query.name == "true" ? "true" : "false";
      let phone = req.query.phone == "true" ? "true" : "false";
      let result = [];
      // **********************
      if (email == "true" && phone == "true" && name == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                email: {
                  contains: q,
                  mode: "insensitive",
                },
              },
              {
                phone: {
                  contains: q,
                  mode: "insensitive",
                },
              },
              {
                firstName: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
            AND: {
              role: "partner",
            },
          },
          select: {
            firstName: true,
            lastName: true,
            email: true,
            phone: true,
            company_name: true,
            createdAt: true,
          },
        });
      } else if (phone == "true" && email == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                email: {
                  contains: q,
                  mode: "insensitive",
                },
              },
              {
                phone: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
            AND: {
              role: "partner",
            },
          },
          select: {
            firstName: true,
            lastName: true,
            email: true,
            phone: true,
            company_name: true,
            createdAt: true,
          },
        });
      } else if (name == "true" && email == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                email: {
                  contains: q,
                  mode: "insensitive",
                },
              },
              {
                firstName: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
            AND: {
              role: "partner",
            },
          },
          select: {
            firstName: true,
            lastName: true,
            email: true,
            phone: true,
            company_name: true,
            createdAt: true,
          },
        });
      } else if (name == "true" && email == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                firstName: {
                  contains: q,
                  mode: "insensitive",
                },
              },
              {
                phone: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
            AND: {
              role: "partner",
            },
          },
          select: {
            firstName: true,
            lastName: true,
            email: true,
            phone: true,
            company_name: true,
            createdAt: true,
          },
        });
      } else if (name == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                firstName: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
            AND: {
              role: "partner",
            },
          },
          select: {
            firstName: true,
            lastName: true,
            email: true,
            phone: true,
            company_name: true,
            createdAt: true,
          },
        });
      } else if (email == "true") {
        result = await prisma.user.findMany({
          where: {
            AND: [
              {
                role: "partner",
              },
              {
                email: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
          },
          select: {
            firstName: true,
            lastName: true,
            email: true,
            phone: true,
            company_name: true,
            createdAt: true,
          },
        });
      } else if (phone == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                phone: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
            AND: {
              role: "partner",
            },
          },
          select: {
            firstName: true,
            lastName: true,
            email: true,
            phone: true,
            company_name: true,
            createdAt: true,
          },
        });
      }

      if (date) {
        let split = date.split(" ");
        let date1 = split[0];
        let date2 = split[2];
        let array = [];
        var startDate = moment(date1, "DD/MM/YYYY");
        var endDate = moment(date2, "DD/MM/YYYY");
        result.map((dt) => {
          var date = moment(new Date(dt.createdAt), "DD/MM/YYYY");

          if (
            date.isSameOrAfter(startDate, "day") &&
            date.isSameOrBefore(endDate, "day")
          ) {
            array.push(dt);
          }
        });

        return res.status(200).send(response(STRINGS.TEXTS.getUser, array));
      }

      res.status(200).send(response(STRINGS.TEXTS.getUser, result));
    } catch (err) {
      res.status(400).send(response(err.message));
    }
  }

  //   search student
  async getsearchStudent(req, res) {
    try {
      const { q } = req.query;
      let email = req.query.email == "true" ? "true" : "false";
      let name = req.query.name == "true" ? "true" : "false";
      let partner_name = req.query.partner_name == "true" ? "true" : "false";

      let result = await prisma.user.findMany({
        where: {
          role: "student",
        },
      });
      // **********************
      if (email == "true" && partner_name == "true" && name == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                email: {
                  contains: q,
                  mode: "insensitive",
                },
              },
              {
                partner_name: {
                  contains: q,
                  mode: "insensitive",
                },
              },
              {
                firstName: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
            AND: {
              role: "student",
            },
          },
        });
      } else if (partner_name == "true" && email == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                email: {
                  contains: q,
                  mode: "insensitive",
                },
              },
              {
                partner_name: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
            AND: {
              role: "student",
            },
          },
        });
      } else if (name == "true" && email == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                email: {
                  contains: q,
                  mode: "insensitive",
                },
              },
              {
                firstName: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
            AND: {
              role: "student",
            },
          },
        });
      } else if (name == "true" && email == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                firstName: {
                  contains: q,
                  mode: "insensitive",
                },
              },
              {
                partner_name: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
            AND: {
              role: "student",
            },
          },
        });
      } else if (name == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                firstName: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
            AND: {
              role: "student",
            },
          },
        });
      } else if (email == "true") {
        result = await prisma.user.findMany({
          where: {
            AND: [
              {
                role: "student",
              },
              {
                email: {
                  contains: q,
                  mode: "insensitive",
                },
              },
            ],
          },
        });
      } else if (partner_name == "true") {
        result = await prisma.user.findMany({
          where: {
            OR: [
              {
                partner_name: {
                  contains: q,
                  mode: "insegnsitive",
                },
              },
            ],
            AND: {
              role: "student",
            },
          },
        });
      }
      res.status(200).send(response(STRINGS.TEXTS.getUser, result));
    } catch (err) {
      res.status(400).send(response(err.message));
    }
  }
  //   search student by country,status and date
  async searchFilterByStudent(req, res) {
    try {
      let country = req.query.country;
      let role = req.query.role;

      let date = req.query.date;
      let status = req.query.status;
      let result = await prisma.user.findMany({
        where: {
          role: role,
        },
      });
      if (country && status && date) {
        let split = date.split(" ");
        let date1 = split[0];
        let date2 = split[2];
        let array = [];
        var startDate = moment(date1, "DD/MM/YYYY");
        var endDate = moment(date2, "DD/MM/YYYY");

        result.map((dt) => {
          var date = moment(new Date(dt.createdAt), "DD/MM/YYYY");

          if (
            date.isSameOrAfter(startDate, "day") &&
            date.isSameOrBefore(endDate, "day") &&
            dt.country.toLocaleLowerCase() == country.toLocaleLowerCase() &&
            dt.app_status.toLocaleLowerCase() == status.toLocaleLowerCase()
          ) {
            array.push(dt);
          }
        });
        return res.status(200).send(response(STRINGS.TEXTS.getUser, array));
      }
      if (country && status) {
        let fills = await prisma.user.findMany({
          where: {
            AND: [
              {
                country: {
                  contains: country,
                },
              },
              {
                app_status: {
                  contains: status,
                },
              },

              {
                role: role,
              },
            ],
          },
        });
        return res.status(200).send(response(STRINGS.TEXTS.getUser, fills));
      }
      if (country && date) {
        let split = date.split(" ");
        let date1 = split[0];
        let date2 = split[2];
        let array = [];
        var startDate = moment(date1, "DD/MM/YYYY");
        var endDate = moment(date2, "DD/MM/YYYY");

        result.map((dt) => {
          var date = moment(new Date(dt.createdAt), "DD/MM/YYYY");

          if (
            date.isSameOrAfter(startDate, "day") &&
            date.isSameOrBefore(endDate, "day") &&
            dt.country.toLocaleLowerCase() == country.toLocaleLowerCase()
          ) {
            array.push(dt);
          }
        });
        return res.status(200).send(response(STRINGS.TEXTS.getUser, array));
      }
      if (status && date) {
        let split = date.split(" ");
        let date1 = split[0];
        let date2 = split[2];
        let array = [];
        var startDate = moment(date1, "DD/MM/YYYY");
        var endDate = moment(date2, "DD/MM/YYYY");

        result.map((dt) => {
          var date = moment(new Date(dt.createdAt), "DD/MM/YYYY");

          if (
            date.isSameOrAfter(startDate, "day") &&
            date.isSameOrBefore(endDate, "day") &&
            dt.app_status.toLocaleLowerCase() == status.toLocaleLowerCase()
          ) {
            array.push(dt);
          }
        });
        return res.status(200).send(response(STRINGS.TEXTS.getUser, array));
      }
      if (date) {
        let result = await prisma.user.findMany({
          where: {
            role: role,
          },
        });
        let split = date.split(" ");
        let date1 = split[0];
        let date2 = split[2];
        let array = [];
        var startDate = moment(date1, "DD/MM/YYYY");
        var endDate = moment(date2, "DD/MM/YYYY");

        result.map((dt) => {
          var date = moment(new Date(dt.createdAt), "DD/MM/YYYY");

          if (
            date.isSameOrAfter(startDate, "day") &&
            date.isSameOrBefore(endDate, "day")
          ) {
            array.push(dt);
          }
        });

        return res.status(200).send(response(STRINGS.TEXTS.getUser, array));
      }
      if (country) {
        let fills = await prisma.user.findMany({
          where: {
            AND: [
              {
                country: {
                  contains: country,
                },
              },

              {
                role: role,
              },
            ],
          },
        });
        return res.status(200).send(response(STRINGS.TEXTS.getUser, fills));
      }
      if (status) {
        let fills = await prisma.user.findMany({
          where: {
            AND: [
              {
                app_status: {
                  contains: status,
                },
              },

              {
                role: role,
              },
            ],
          },
        });
        return res.status(200).send(response(STRINGS.TEXTS.getUser, fills));
      }
      res.status(200).send(response(STRINGS.TEXTS.getUser, result));
    } catch (err) {
      console.log("Error--->", err.message);
      res.status(400).send(response(err.message));
    }
  }

  //   generate pdf
  async generatePdf(req, res) {
    try {
      const _id = JSON.parse(req.params.id);

      // Check if user exist
      const app = await prisma.application.findFirst({
        where: {
          userId: _id,
        },
      });
      if (!app) {
        return res.status(400).send(response(STRINGS.ERRORS.appNotFound));
      }
      let appId = Number(app.id);
      const applicantDetail = await prisma.applicantsdetail.findMany({
        where: {
          appId: appId,
        },
      });
      const testScores = await prisma.testscore.findMany({
        where: {
          appId: appId,
        },
      });
      const appDocuments = await prisma.appdocument.findMany({
        where: {
          appId: appId,
        },
      });
      let applicantDetailOne;
      let applicantDetailTwo;
      if (applicantDetail && applicantDetail.length == 1) {
        applicantDetailOne = applicantDetail[0];
      } else if (applicantDetail && applicantDetail.length == 2) {
        applicantDetailTwo = applicantDetail[1];
        applicantDetailOne = applicantDetail[0];
      }
      let date = Date.now();
      const fileName = `${date}.pdf`;
      const File_Path = path.join(
        __dirname,
        "../html_templates/application.html"
      );
      const source = fs.readFileSync(File_Path, "utf-8").toString();
      const template = handlebars.compile(source);

      const replacements = {
        firstName: app.firstName,
        lastName: app.lastName,
        email: app.email,
        phone: app.phone,
        dateOfBirth: app.date,
        address: app.address,
        state: app.state,
        city: app.city,
        pinCode: app.pinCode,
        apply: app.apply,
        country: app.country,
        university: app.university,
        degree: app.degree,
        education: app.education,
        status: app.status,
        livingExpense: app.livingExpense,
        collateral: app.collateral,
        loanAmount: app.loanAmount,
        department: app.department,
        course: app.course,
        course: app.course,
        college: app.college,
        cgpa: app.cgpa,
        tenPercentage: app.tenPercentage,
        twelvePercentage: app.twelvePercentage,
        backlogs: app.backlogs,
        backlogsNumber: app.backlogsNumber,
        test: app.test,
        applicantDetailOne: applicantDetailOne,
        applicantDetailTwo: applicantDetailTwo,
        applicantDetail: applicantDetail,
        appDocuments: appDocuments,
        testScores: testScores,
      };
      const htmlToSend = template(replacements);
      const pdf = await GeneratePdf(htmlToSend);
      res.set("Content-Type", "application/pdf");
      let buff = Buffer.from(pdf, "binary");
      fs.writeFileSync(`uploads/pdf/${fileName}`, buff);
      let filePath = `uploads/pdf/${fileName}`;
      let destFileName = "pdf/" + fileName;
      const options = {
        destination: destFileName,
        predefinedAcl: "publicRead",
      };
      let data = await admin
        .storage()
        .bucket()
        .upload(filePath, options)
        .then((result) => {
          const file = result[0];
          return file.getMetadata();
        });

      const metadata = data[0];
      var mediaLink = metadata.mediaLink;
      // let mediaLink = `${req.body.url}/${fileName}`;
      res.status(200).json({ message: "success", mediaLink, fileName });
    } catch (err) {
      console.log(err.message, "Error--->");
      res.status(400).send(response(err.message));
    }
  }
}
module.exports = new AdminContoller();
