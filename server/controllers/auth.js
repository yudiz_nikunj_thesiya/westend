//Login User
const STRINGS = require("../utils/texts");
const CustomError = require("../utils/customError");
const response = require("../utils/response");
const bcrypt = require("bcryptjs");
const JWT = require("jsonwebtoken");
const crypto = require("crypto");
const MailService = require("../services/mail.service");

var admin = require("firebase-admin");
var Axios = require("axios");
const passwordExpires = Date.now() + 48 * (60 * 60 * 1000) + "";
const prisma = require("../prisma/index");

class AuthContoller {
  async signup(req, res) {
    try {
      const { phone, email } = req.body;
      let Emailuser = await prisma.user.findFirst({
        where: {
          email: email,
        },
      });
      let Phoneuser = await prisma.user.findFirst({
        where: {
          phone: phone,
        },
      });
      if (Emailuser) {
        throw new CustomError(res, STRINGS.ERRORS.userAlreadyExists, 404);
      } else if (Phoneuser) {
        throw new CustomError(res, STRINGS.ERRORS.phoneAlreadyExists, 404);
      } else {
        let user = await prisma.user.create({ data: req.body });
        let newID = Number(user.id);
        await prisma.application.create({
          data: {
            userId: newID,
            //   basic
            firstName: user.firstName,
            lastName: user.lastName,
            phone: user.phone,
            email: user.email,
            date: user.createdAt,
          },
        });
        let tempNum = Math.floor(1000 + Math.random() * 9000);
        let num = tempNum.toString();

        let isExpired = Date.now() + "300000";
        let data = {
          otp: num,
          userId: user.id,
          email: user.email,
          isExpired: isExpired,
        };
        let otpUsers = await prisma.otp.findMany({});

        if (otpUsers && otpUsers.length > 0) {
          let otpUser = await prisma.otp.findFirst({
            where: {
              email: user.email,
            },
          });

          if (!otpUser) {
            await prisma.otp.create({ data: data });
          } else {
            let _id = Number(otpUser.id);
            if (otpUser.userId == null) {
              await prisma.otp.delete({
                where: {
                  id: _id,
                },
              });
              await prisma.otp.create({ data: data });
            } else {
              await prisma.otp.update({
                where: {
                  id: _id,
                },
                data: {
                  otp: data.otp,
                  isExpired: isExpired,
                },
              });
            }
          }
        } else {
          await prisma.otp.create({ data: data });
        }
        Axios({
          method: "get",
          url: `http://2factor.in/API/V1/${process.env.OTP_API_KEY}/SMS/+91${phone}/${tempNum}`,
        })
          .then((response) => {
            console.log("success");
          })
          .catch((error) => {
            console.log("error--->", error.Details);
          });

        let emailService = new MailService();
        await emailService.sendEmail(user, tempNum);
        res.status(200).send(response(STRINGS.TEXTS.userCreated, user, 200));
      }
    } catch (err) {
      console.log(err.message, "err.message");
      res.status(500).send(response(err.message, 400));
    }
  }

  async login(req, res) {
    try {
      const data = req.body;
      if (!data.email)
        res.status(400).send(response(STRINGS.ERRORS.emailRequired, 404));

      // Check if user exist
      const user = await prisma.user.findFirst({
        where: {
          email: data.email,
        },
      });
      if (!user) {
        return res
          .status(404)
          .send(response(STRINGS.ERRORS.emailInvalid, user));
      }
      if (user && user.role === "partner" && user.isVerify !== true) {
        return res.status(404).send(response(STRINGS.ERRORS.emailVerify, user));
      }
      if (user && user.role === "partner" && user.approved !== "approved") {
        return res
          .status(404)
          .send(response(STRINGS.ERRORS.partnerApproval, user));
      }
      if (user && user.role === "student" && user.isVerify !== true) {
        return res.status(404).send(response(STRINGS.ERRORS.emailVerify, user));
      }
      if (user && user.role === "admin" && user.isVerify !== true) {
        return res.status(404).send(response(STRINGS.ERRORS.emailVerify, user));
      }
      if (!user.password && user.role === "partner") {
        return res
          .status(400)
          .send(response(STRINGS.ERRORS.partnerInvalidPassword));
      }
      if (
        user &&
        !user.password &&
        user.role == "student" &&
        user.isVerify === true
      ) {
        const buf = await crypto.randomBytes(20);
        var passwordToken = buf.toString("hex");

        let _id = Number(user.id);
        await prisma.user.update({
          where: {
            id: _id,
          },
          data: {
            isVerify: true,
            passwordExpires: passwordExpires,
            passwordToken: passwordToken,
          },
        });
        let emailService = new MailService();
        await emailService.sendEmail(user, passwordToken);
        const result = {
          user,
        };
        return res
          .status(200)
          .send(response(STRINGS.TEXTS.verifyEmail, result));
      }
      // Check if user password is correct
      let pass = data.password;
      let pass2 = user.password;
      if (pass.length !== pass2.length) {
        const isCorrect = await bcrypt.compare(data.password, user.password);
        if (!isCorrect)
          res.status(400).send(response(STRINGS.ERRORS.passwordInvalid));

        // throw new CustomError(res, STRINGS.ERRORS.passwordInvalid, 401);
      } else {
        if (pass !== pass2)
          res.status(400).send(response(STRINGS.ERRORS.passwordInvalid));
      }

      const token = await JWT.sign({ id: user.id }, process.env.JWT_SECRET);

      const result = {
        user,
        token,
      };

      res.status(200).send(response(STRINGS.TEXTS.loginSuccess, result));
    } catch (error) {
      console.log(error.message, "error.message");
      res.status(400).send(response(error.message));
    }
  }

  // social login
  async socialLogin(req, res) {
    try {
      const data = req.body;

      // Check if user exist
      const user = await prisma.user.findFirst({
        where: {
          email: data.email,
        },
      });
      if (!user) {
        return res
          .status(404)
          .send(response(STRINGS.ERRORS.emailInvalid, user));
      }
      if (user && user.role === "partner" && user.isVerify !== true) {
        return res.status(404).send(response(STRINGS.ERRORS.emailVerify, user));
      }
      if (user && user.role === "partner" && user.approved !== "approved") {
        return res
          .status(404)
          .send(response(STRINGS.ERRORS.partnerApproval, user));
      }
      // if (user && user.role === "student" && user.isVerify !== true) {
      //   return res.status(404).send(response(STRINGS.ERRORS.emailVerify, user));
      // }
      // if (user && user.role === "admin" && user.isVerify !== true) {
      //   return res.status(404).send(response(STRINGS.ERRORS.emailVerify, user));
      // }
      const token = await JWT.sign({ id: user.id }, process.env.JWT_SECRET);

      const result = {
        user,
        token,
      };

      res.status(200).send(response(STRINGS.TEXTS.loginSuccess, result));
    } catch (error) {
      console.log(error.message, "error.message");
      res.status(400).send(response(error.message));
    }
  }
  async updatePassword(req, res) {
    try {
      const data = req.body;
      let oldPassword = data.oldPassword;
      let newPassword = data.newPassword;

      let _id = Number(req.params.id);
      if (!oldPassword)
        res.status(400).send(response(STRINGS.ERRORS.passwordRequired, 404));
      if (!newPassword)
        res.status(400).send(response(STRINGS.ERRORS.passwordRequired, 404));

      // Check if user exist
      const user = await prisma.user.findFirst({
        where: {
          id: _id,
        },
      });

      if (!user)
        res.status(404).send(response(STRINGS.ERRORS.emailInvalid, user));
      // Check if user password is correct
      const isCorrect = await bcrypt.compare(oldPassword, user.password);
      if (!isCorrect)
        throw new CustomError(res, STRINGS.ERRORS.passwordInvalid, 401);
      const salt = bcrypt.genSaltSync(15);

      const hash = await bcrypt.hash(data.newPassword, salt);

      const updatedUser = await prisma.user.update({
        where: {
          id: _id,
        },
        data: {
          password: hash,
        },
      });
      res
        .status(200)
        .send(response(STRINGS.TEXTS.passwordSuccess, updatedUser));
    } catch (err) {
      res.status(400).send(response(err.message));
    }
  }
  async updateProfile(req, res) {
    try {
      let _id = JSON.parse(req.params.id);
      const user = await prisma.user.findFirst({
        where: {
          id: _id,
        },
      });
      if (!user) {
        return res.status(404).send(response(STRINGS.ERRORS.userNotFound));
      }
      if (user && user.role == "partner") {
        let {
          firstName,
          lastName,
          email,
          phone,
          image,
          company_name,
          website,
          reason,
        } = req.body;
        let UploadedPicture = image;
        if (req.file) {
          let filePath = `${req.file.path}`;

          let destFileName = "images/" + req.file.filename;
          const options = {
            destination: destFileName,
            predefinedAcl: "publicRead",
          };
          let response = await admin
            .storage()
            .bucket()
            .upload(filePath, options)
            .then((result) => {
              const file = result[0];
              return file.getMetadata();
            });

          const metadata = response[0];
          UploadedPicture = metadata.mediaLink;
        }

        const updatedUser = await prisma.user.update({
          where: {
            id: _id,
          },
          data: {
            firstName: firstName,
            lastName: lastName,
            email: email,
            phone: phone,
            image: UploadedPicture,
            company_name: company_name,
            website: website,
            reason: reason,
          },
        });
        let students = await prisma.user.findMany({});

        let student = students.find((dt) => dt.partnerId == updatedUser.id);

        if (student) {
          let newId = Number(student.id);
          await prisma.user.update({
            where: {
              id: newId,
            },
            data: {
              partner_name: updatedUser.company_name,
            },
          });
        }
        res.status(200).send(response(STRINGS.TEXTS.userUpdated, updatedUser));
      } else if (user && user.role == "student") {
        let { firstName, lastName, email, phone, image } = req.body;
        let UploadedPicture = image;

        if (req.file) {
          let filePath = `${req.file.path}`;

          let destFileName = "images/" + req.file.filename;
          const options = {
            destination: destFileName,
            predefinedAcl: "publicRead",
          };
          let response = await admin
            .storage()
            .bucket()
            .upload(filePath, options)
            .then((result) => {
              const file = result[0];
              return file.getMetadata();
            });

          const metadata = response[0];
          UploadedPicture = metadata.mediaLink;
        }

        const updatedUser = await prisma.user.update({
          where: {
            id: _id,
          },
          data: {
            firstName: firstName,
            lastName: lastName,
            email: email,
            phone: phone,
            image: UploadedPicture,
          },
        });
        let newId = Number(updatedUser.id);
        const app = await prisma.application.findFirst({
          where: {
            userId: newId,
          },
        });
        if (app) {
          let appId = Number(app.id);
          await prisma.application.update({
            where: {
              id: appId,
            },
            data: {
              firstName: updatedUser.firstName,
              lastName: updatedUser.lastName,
              email: updatedUser.email,
              phone: updatedUser.phone,
            },
          });
        }
        res.status(200).send(response(STRINGS.TEXTS.userUpdated, updatedUser));
      } else {
        let { firstName, lastName, email, phone, image } = req.body;
        let UploadedPicture = image;

        if (req.file) {
          let filePath = `${req.file.path}`;

          let destFileName = "images/" + req.file.filename;
          const options = {
            destination: destFileName,
            predefinedAcl: "publicRead",
          };
          let response = await admin
            .storage()
            .bucket()
            .upload(filePath, options)
            .then((result) => {
              const file = result[0];
              return file.getMetadata();
            });

          const metadata = response[0];
          UploadedPicture = metadata.mediaLink;
        }

        const updatedUser = await prisma.user.update({
          where: {
            id: _id,
          },
          data: {
            firstName: firstName,
            lastName: lastName,
            email: email,
            phone: phone,
            image: UploadedPicture,
          },
        });
        res.status(200).send(response(STRINGS.TEXTS.userUpdated, updatedUser));
      }
    } catch (err) {
      console.log("Errror--->", err.message);
      res.status(400).send(response(err.message));
    }
  }
  // get single user

  async getUser(req, res) {
    try {
      const _id = req.params.id;
      // Check if user exist
      const user = await prisma.user.findFirst({
        where: {
          id: _id,
        },
      });
      res.status(200).send(response(STRINGS.TEXTS.getUser, user));
    } catch (error) {
      res.status(400).send(response(error.message));
    }
  }

  async forgotPassword(req, res) {
    try {
      let data = req.body;

      let user = await prisma.user.findFirst({
        where: {
          email: data.email,
        },
      });

      if (!user) {
        throw new CustomError(res, STRINGS.ERRORS.emailInvalid, 404);
      } else {
        if (user && user.role === "partner" && user.isVerify !== true) {
          return res
            .status(404)
            .send(response(STRINGS.ERRORS.emailVerify, user));
        }
        if (user && user.role === "partner" && user.approved !== "approved") {
          return res
            .status(404)
            .send(response(STRINGS.ERRORS.partnerApproval, user));
        }
        if (user && user.role === "student" && user.isVerify !== true) {
          return res
            .status(404)
            .send(response(STRINGS.ERRORS.emailVerify, user));
        }
        if (user && user.role === "admin" && user.isVerify !== true) {
          return res
            .status(404)
            .send(response(STRINGS.ERRORS.emailVerify, user));
        }
        if (user.password == "" && user.role === "partner")
          return res
            .status(400)
            .send(response(STRINGS.ERRORS.passwordResetPartner));
        const buf = await crypto.randomBytes(20);
        var resetPasswordToken = buf.toString("hex");
        const restPasswordExpires = Date.now() + "300000";
        let _id = Number(user.id);
        await prisma.user.update({
          where: {
            id: _id,
          },
          data: {
            resetPasswordToken: resetPasswordToken,
            restPasswordExpires: restPasswordExpires,
          },
        });

        let emailService = new MailService();
        await emailService.resetEmail(user, resetPasswordToken);
        res.status(200).send(response(STRINGS.TEXTS.passwordResetSent, user));
      }
    } catch (err) {
      console.log(err.message);
      res.status(400).send(response(err.message));
    }
  }
  // reset password
  async ResetPassword(req, res) {
    try {
      let data = req.body;
      let check = await prisma.user.findFirst({
        where: {
          resetPasswordToken: data.resetPasswordToken,
        },
      });
      if (!check)
        res.status(404).send(response(STRINGS.ERRORS.tokenInvalid, users));
      let isExpired = Number(check.restPasswordExpires);
      let _id = Number(check.id);
      const salt = bcrypt.genSaltSync(15);

      const hash = await bcrypt.hash(data.password, salt);
      data.password = hash;
      if (isExpired > Date.now()) {
        const updatedUser = await prisma.user.update({
          where: {
            id: _id,
          },
          data: {
            password: data.password,
          },
        });
        res
          .status(200)
          .send(response(STRINGS.TEXTS.passwordUpdated, updatedUser));
      } else {
        res.status(404).send(response(STRINGS.ERRORS.invalidToken, users));
      }
    } catch (err) {
      console.log(err.message);
      res.status(400).send(response(err.message));
    }
  }
  // SetPassword
  async SetPassword(req, res) {
    try {
      let data = req.body;
      let check = await prisma.user.findFirst({
        where: {
          passwordToken: data.passwordToken,
        },
      });
      if (!check)
        return res
          .status(404)
          .send(response(STRINGS.ERRORS.tokenInvalid, data));

      let _id = Number(check.id);
      const salt = bcrypt.genSaltSync(15);

      const hash = await bcrypt.hash(data.password, salt);
      data.password = hash;
      const user = await prisma.user.update({
        where: {
          id: _id,
        },
        data: {
          password: data.password,
          isVerify: true,
          passwordExpires: "",
        },
      });
      const token = await JWT.sign({ id: user.id }, process.env.JWT_SECRET);

      const result = {
        user,
        token,
      };

      res.status(200).send(response(STRINGS.TEXTS.loginSuccess, result));
      // res
      //   .status(200)
      //   .send(response(STRINGS.TEXTS.passwordUpdated, user));
    } catch (err) {
      console.log(err.message);
      res.status(400).send(response(err.message));
    }
  }

  // ***
  async otpLogin(req, res) {
    try {
      const { email, phone } = req.body;
      let user;
      let phoneNumber = phone;
      if (phone !== "") {
        user = await prisma.user.findFirst({
          where: {
            phone: phoneNumber,
          },
        });
      } else {
        user = await prisma.user.findFirst({
          where: {
            email: email,
          },
        });
      }
      let tempNum = Math.floor(1000 + Math.random() * 9000);
      let num = tempNum.toString();
      if (!user) {
        return res.status(400).send(response(STRINGS.ERRORS.userNotFound));
      }
      if (user && user.role === "partner" && user.isVerify !== true) {
        return res.status(404).send(response(STRINGS.ERRORS.emailVerify, user));
      }
      if (user && user.role === "partner" && user.approved !== "approved") {
        return res
          .status(404)
          .send(response(STRINGS.ERRORS.partnerApproval, user));
      }
      if (user && user.role === "student" && user.isVerify !== true) {
        return res.status(404).send(response(STRINGS.ERRORS.emailVerify, user));
      }
      if (user && user.role === "admin" && user.isVerify !== true) {
        return res.status(404).send(response(STRINGS.ERRORS.emailVerify, user));
      }

      let isExpired = Date.now() + "300000";
      let data = {
        otp: num,
        userId: user.id,
        email: user.email,
        isExpired: isExpired,
      };
      let otpUsers = await prisma.otp.findMany({});

      if (otpUsers && otpUsers.length > 0) {
        let otpUser = await prisma.otp.findFirst({
          where: {
            email: user.email,
          },
        });

        if (!otpUser) {
          await prisma.otp.create({ data: data });
        } else {
          let _id = Number(otpUser.id);
          if (otpUser.userId == null) {
            await prisma.otp.delete({
              where: {
                id: _id,
              },
            });
            await prisma.otp.create({ data: data });
          } else {
            await prisma.otp.update({
              where: {
                id: _id,
              },
              data: {
                otp: data.otp,
                isExpired: isExpired,
              },
            });
          }
        }
      } else {
        await prisma.otp.create({ data: data });
      }

      if (phone !== "") {
        Axios.get(
          `http://2factor.in/API/V1/${process.env.OTP_API_KEY}/SMS/+91${phoneNumber}/${tempNum}`
        )
          .then((response) => {
            console.log("success");
          })
          .catch((error) => {
            console.log(error);
          });
      } else {
        let emailService = new MailService();
        await emailService.sendOtpEmail(user, tempNum);
      }

      res.status(200).send(response(STRINGS.TEXTS.sendOtp, user));
    } catch (error) {
      console.log(error.message, "error.message");
      res.status(500).send(response(error.message));
    }
  }

  async verifyOtp(req, res) {
    try {
      let otp = req.body.otp;
      // Check if user exist
      let otpUser = await prisma.otp.findFirst({
        where: {
          otp: otp,
        },
      });
      if (!otpUser)
        res.status(404).send(response(STRINGS.ERRORS.otpInvalid, otpUser));
      let isExpired = Number(otpUser.isExpired);
      if (isExpired > Date.now()) {
        let userId = otpUser.userId;
        let user = await prisma.user.findFirst({
          where: {
            id: userId,
          },
        });
        const token = await JWT.sign({ id: userId }, process.env.JWT_SECRET);
        const result = {
          user,
          token,
        };

        res.status(200).send(response(STRINGS.TEXTS.loginSuccess, result));
      } else {
        res.status(400).send(response(STRINGS.ERRORS.OtpExpired));
      }
    } catch (err) {
      res.status(400).send(response(err.message));
    }
  }

  // check eligiblity

  async CheckEligibility(req, res) {
    try {
      let {
        firstName,
        lastName,
        phone,
        email,
        monthlyIncome,
        otherEMI,
        loanAmount,
      } = req.body;
      let data = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        phone: phone,
      };
      let user;
      let emailUser = await prisma.user.findFirst({
        where: {
          email: email,
        },
      });

      let userExist = false;
      if (emailUser) {
        userExist = true;
        user = emailUser;
      }
      let phoneUser = await prisma.user.findFirst({
        where: {
          phone: phone,
        },
      });
      if (phoneUser) {
        userExist = true;
        user = phoneUser;
      }

      // if (emailuser) {
      //   let emailService = new MailService();
      //   await emailService.sendEmailNormal(
      //     email,
      //     "Student Loan Eligibility",
      //     ""
      //   );
      //   return res.status(400).send(response(STRINGS.ERRORS.alreadySubmitted));
      // }

      const buf = crypto.randomBytes(20);
      var passwordToken = buf.toString("hex");

      data["passwordExpires"] = passwordExpires;
      data["passwordToken"] = passwordToken;
      data["app_status"] = "Inquired";
      if (!userExist) {
        user = await prisma.user.create({ data: data });
      }
      let emailService = new MailService();
      await emailService.sendEmailToStudent(
        user,
        user.passwordToken,
        "Student Loan Eligibility",
        "Thank you for contacting Us. Our Team will get back to you soon for your Student loan eligibility."
      );
      let _id = Number(user.id);
      let userApp = prisma.application.findFirst({
        where: {
          userId: _id,
        },
      });
      if (!userApp) {
        await prisma.application.create({
          data: {
            userId: _id,
            firstName: firstName,
            lastName: lastName,
            phone: phone,
            email: email,
            monthlyIncome: monthlyIncome,
            otherEMI: otherEMI,
            loanAmount: loanAmount,
          },
        });
      }
      let emailService2 = new MailService();
      await emailService2.sendEmailCheckEligibility(req.body);

      res.status(200).send(response(STRINGS.TEXTS.applicationCreated));
    } catch (err) {
      console.log(err.message);
      res.status(400).send(response(err.message));
    }
  }
  //contact us
  async ContactUs(req, res) {
    try {
      const data = req.body;

      let emailService = new MailService();
      await emailService.sendEmailToContactUS(data);

      res.status(200).send(response(STRINGS.TEXTS.loginSuccess));
    } catch (err) {
      res.status(400).send(response(err.message));
    }
  }

  //request callback
  async RequestCalllback(req, res) {
    try {
      const data = req.body;
      let emailService = new MailService();
      await emailService.sendEmailRequestCallback(data);

      res.status(200).send(response(STRINGS.TEXTS.loginSuccess));
    } catch (err) {
      res.status(400).send(response(err.message));
    }
  }
  //application reminder
  async ApplicationReminder(req, res) {
    try {
      const data = req.body;
      let emailService = new MailService();
      await emailService.sendEmailToStudentForApplicationReminder(data);

      res.status(200).send(response(STRINGS.TEXTS.appemailSent));
    } catch (err) {
      res.status(400).send(response(err.message));
    }
  }
  //apply scholarship
  async applyScholarship(req, res) {
    try {
      let { firstName, lastName, phone, email, university, country } = req.body;
      let data = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        phone: phone,
      };
      let user;
      let emailUser = await prisma.user.findFirst({
        where: {
          email: email,
        },
      });

      let userExist = false;
      if (emailUser) {
        userExist = true;
        user = emailUser;
      }
      let phoneUser = await prisma.user.findFirst({
        where: {
          phone: phone,
        },
      });
      if (phoneUser) {
        userExist = true;
        user = phoneUser;
      }
      const buf = await crypto.randomBytes(20);
      var passwordToken = buf.toString("hex");
      data["passwordToken"] = passwordToken;

      data["passwordExpires"] = passwordExpires;
      data["app_status"] = "Applied for Scholarship";
      data["country"] = country;

      if (!userExist) {
        user = await prisma.user.create({ data: data });
      }
      let emailService = new MailService();
      await emailService.sendEmailToStudent(
        user,
        user.passwordToken,
        "Scholarship Application",
        "Thank you for submitting your request for applying for Scholarship. Our Team will get back to you soon"
      );
      let _id = Number(user.id);
      let userApp = prisma.application.findFirst({
        where: {
          userId: _id,
        },
      });
      if (!userApp) {
        await prisma.application.create({
          data: {
            userId: _id,
            firstName: firstName,
            lastName: lastName,
            phone: phone,
            email: email,
            university: university,
            country: country,
          },
        });
      }
      let emailService2 = new MailService();

      await emailService2.sendEmailToApplyScholarship(req.body);

      res.status(200).send(response(STRINGS.TEXTS.emailSent));
    } catch (err) {
      return res.status(400).send(response(err.message));
    }
  }

  //apply sop
  async applySop(req, res) {
    try {
      let { firstName, lastName, phone, email, university, country } = req.body;
      let data = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        phone: phone,
      };
      let user;
      let emailUser = await prisma.user.findFirst({
        where: {
          email: email,
        },
      });

      let userExist = false;
      if (emailUser) {
        userExist = true;
        user = emailUser;
      }
      let phoneUser = await prisma.user.findFirst({
        where: {
          phone: phone,
        },
      });
      if (phoneUser) {
        userExist = true;
        user = phoneUser;
      }
      const buf = await crypto.randomBytes(20);
      var passwordToken = buf.toString("hex");
      data["passwordToken"] = passwordToken;

      data["passwordExpires"] = passwordExpires;
      data["app_status"] = "Applied for SOP";
      data["country"] = country;

      if (!userExist) {
        user = await prisma.user.create({ data: data });
      }
      let emailService = new MailService();
      await emailService.sendEmailToStudent(
        user,
        user.passwordToken,
        "SOP Request",
        "Thank you for submitting your request for getting your SOP. Our Team will get back to you soon."
      );
      let _id = Number(user.id);
      let userApp = prisma.application.findFirst({
        where: {
          userId: _id,
        },
      });
      if (!userApp) {
        await prisma.application.create({
          data: {
            userId: _id,
            firstName: firstName,
            lastName: lastName,
            phone: phone,
            email: email,
            university: university,
            country: country,
          },
        });
      }
      let emailService2 = new MailService();
      await emailService2.sendEmailToSopReview(req.body);

      res.status(200).send(response(STRINGS.TEXTS.emailSent));
    } catch (err) {
      return res.status(400).send(response(err.message));
    }
  }
  //forex remittances email
  async applyForexRemittance(req, res) {
    try {
      let { firstName, lastName, phone, email, university, country } = req.body;
      let data = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        phone: phone,
      };
      let user;
      let emailUser = await prisma.user.findFirst({
        where: {
          email: email,
        },
      });

      let userExist = false;
      if (emailUser) {
        userExist = true;
        user = emailUser;
      }
      let phoneUser = await prisma.user.findFirst({
        where: {
          phone: phone,
        },
      });
      if (phoneUser) {
        userExist = true;
        user = phoneUser;
      }
      const buf = await crypto.randomBytes(20);
      var passwordToken = buf.toString("hex");

      data["passwordExpires"] = passwordExpires;
      data["passwordToken"] = passwordToken;
      data["country"] = country;

      if (!userExist) {
        user = await prisma.user.create({ data: data });
      }
      let emailService = new MailService();

      await emailService.sendEmailToStudent(
        user,
        user.passwordToken,
        "Forex Request",
        "Thank you for submitting your request for Forex Remittance. Our Financial Advisor Team will get back to you soon"
      );
      let _id = Number(user.id);
      let userApp = prisma.application.findFirst({
        where: {
          userId: _id,
        },
      });
      if (!userApp) {
        await prisma.application.create({
          data: {
            userId: _id,
            firstName: firstName,
            lastName: lastName,
            phone: phone,
            email: email,
            university: university,
            country: country,
          },
        });
      }
      let emailService2 = new MailService();

      await emailService2.sendEmailToForexRemmitance(req.body);

      res.status(200).send(response(STRINGS.TEXTS.emailSent));
    } catch (err) {
      return res.status(400).send(response(err.message));
    }
  }

  //applyStudentaccommodation email
  async applyStudentaccommodation(req, res) {
    try {
      let { firstName, lastName, phone, email, university, country } = req.body;
      let data = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        phone: phone,
      };
      let user;
      let emailUser = await prisma.user.findFirst({
        where: {
          email: email,
        },
      });

      let userExist = false;
      if (emailUser) {
        userExist = true;
        user = emailUser;
      }
      let phoneUser = await prisma.user.findFirst({
        where: {
          phone: phone,
        },
      });
      if (phoneUser) {
        userExist = true;
        user = phoneUser;
      }
      const buf = await crypto.randomBytes(20);
      var passwordToken = buf.toString("hex");
      data["passwordToken"] = passwordToken;

      data["passwordExpires"] = passwordExpires;
      data["country"] = country;

      if (!userExist) {
        user = await prisma.user.create({ data: data });
      }

      let emailService = new MailService();

      await emailService.sendEmailToStudent(
        user,
        user.passwordToken,
        "Accommodation Request",
        "Thank you for submitting your request for Accommodation. Our Team will get back to you soon"
      );
      let _id = Number(user.id);
      let userApp = prisma.application.findFirst({
        where: {
          userId: _id,
        },
      });
      if (!userApp) {
        await prisma.application.create({
          data: {
            userId: _id,
            firstName: firstName,
            lastName: lastName,
            phone: phone,
            email: email,
            university: university,
            country: country,
          },
        });
      }
      let emailService2 = new MailService();

      await emailService2.sendEmailToStudentaccommodation(req.body);

      res.status(200).send(response(STRINGS.TEXTS.emailSent));
    } catch (err) {
      return res.status(400).send(response(err.message));
    }
  }

  //verify partner
  async verifyPartner(req, res) {
    try {
      let check = await prisma.user.findFirst({
        where: {
          passwordToken: req.body.passwordToken,
        },
      });
      if (!check)
        return res.status(404).send(response(STRINGS.ERRORS.tokenInvalid));
      const buf = await crypto.randomBytes(10);
      var verifyPartnerToken = buf.toString("hex");

      let isExpired = Number(check.passwordExpires);
      if (isExpired > Date.now()) {
        let userId = check.id;
        await prisma.user.update({
          where: {
            id: userId,
          },
          data: {
            isVerify: true,
            verifyPartnerToken: verifyPartnerToken,
          },
        });
        res.status(200).send(response(STRINGS.TEXTS.partnerVerify));
      } else {
        res.status(400).send(response(STRINGS.ERRORS.tokenExpired));
      }
    } catch (err) {
      res.status(400).send(response(err.message));
    }
  }
  //verify student after signup
  async verifyStudent(req, res) {
    try {
      let check = await prisma.user.findFirst({
        where: {
          passwordToken: req.body.passwordToken,
        },
      });
      if (!check)
        return res.status(404).send(response(STRINGS.ERRORS.tokenInvalid));

      let isExpired = Number(check.passwordExpires);
      if (isExpired > Date.now()) {
        let _id = Number(check.id);

        const user = await prisma.user.update({
          where: {
            id: _id,
          },
          data: {
            isVerify: true,
            passwordExpires: "",
          },
        });
        const token = await JWT.sign({ id: user.id }, process.env.JWT_SECRET);
        const result = {
          user,
          token,
        };
        res.status(200).send(response(STRINGS.TEXTS.loginSuccess, result));
      } else {
        res.status(400).send(response(STRINGS.ERRORS.tokenExpired));
      }
    } catch (err) {
      res.status(400).send(response(err.message));
    }
  }
}

module.exports = new AuthContoller();
