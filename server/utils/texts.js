module.exports = {
  ERRORS: {
    adminNotFound: "No Admin Found",

    levelNotFound: "No Level Found",
    levelExists: "Level Already Exists",

    userAlreadyExists: "User exists",
    phoneAlreadyExists: "Phone exists",

    error: "There was an error!",
    nameRequired: "Name Is Required",
    typeRequired: "Type Is Required",
    emailRequired: "Email is required",
    imageRequired: "Image is required",
    invalidImage: "Please select an image of File type JPEG/JPG/PNG",

    emailExists: "Email already exists",
    userNotFound: "User does not exist",
    appNotFound: "Application not found of this student",

    phoneLength: "Phone must be 10 digits",
    startDateRequired: "Start Date Required",
    passwordRequired: "Password is required",
    passwordRequired: "Password is required",
    levelNameRequired: "Level Name Required",
    passwordInvalid: "Password is incorrect!",
    OtpExpired: "OTP Expired",
    tokenExpired: "Link Expired",

    oldPasswordInvalid: "Old Password is incorrect!",

    userNameExists: "Username is not available!",
    alreadyVerified: "Email is already verified",

    companyAlreadyExists: "Company Already Exists",
    emailInvalid: "No user found with that Email!",
    emailVerify: "You have not verify your email Yet!",

    partnerApproval:
      "Please wait for sometime until our team evaluates your profile. Thank you for partnering with us.",
    partnerInvalidPassword:
      "Your password has not been yet, Please contact our westend team. Thank you !",

    otpInvalid: "Otp invalid!",
    tokenInvalid: "Token invalid!",
    passwordResetPartner:
      "Your password is not yet set by westend team, please contact them. Thank You!",
    alreadySubmitted:
      "Your request is already under review. Please contact our service desk helpline.",
  },
  TEXTS: {
    allStudents: "All students",
    getUser: "User fetched Successfully",
    approvePartner: "Partner Approved Successfully",

    userData: "User data",
    userAdded: "User Added",
    userCreated: "Signup Successful, please check and verify your email.",

    partnerCreatedByAdmin: "Partner created",
    StudentCreatedByAdmin: "Student created",

    partnerCreated:
      "Your request submitted successfully, admin has to approve it",

    userUpdated: "User updated",
    StudentUpdated: "Student updated",
    partnerUpdated: "Partner updated",
    partnerCreated:
      "Thank you for partnering with Us. Please verify your email and follow the next steps.",

    partnerDeleted: "Partner Deleted",
    studentDeleted: "Student Deleted",

    courseDetails: "Course Details created",
    testScore: "Test Scores created",
    testScoreDeleted: "Test Scores deleted",
    appDocumentDeleted: "Document deleted",
    applicationSubmitted:
      "Your Application is Submitted succesfully. You will be contacted shortly by our Loan Officer.",

    DocumentCreated: "Document created Successfully",

    applicantDetails: "Applicant Details created",
    applicationCreated:
      "Thank you for submitting your details. We will get back to you shortly",
    basicDetails: "Basic Details created",

    userDeleted: "User deleted",
    quoteCreated: "Quote Created",
    userCompany: "User company requested",

    passwordSuccess: "Password updated",
    clientRequest: "Client Request Sent",
    companyRequested: "Company Requested",
    loginSuccess: "User login successful",
    enterpriseDelivery: "Enterprise Delivery",
    siteCompanyUpdated: "Site Company Updated",
    siteCompanyDeleted: "Site Company deleted",
    subscriptionDeleted: "Subscription deleted",
    subscriptonRequested: "Subscription Request",
    emailVerified: "Email verified successfully",
    requestResetPasswordSubject: "Password Reset",
    newPartnerCreated: "New Partner Sign up",

    newStudentCreated: "New Student Sign up",

    sendOtpSubject: "Signin with OTP",
    sendOtp: "OTP sent successfully.",
    verifyEmail:
      "Verification email sent successfully, please check your email",
    partnerVerify: "Partner is verified successfully",
    emailSent:
      "Your inquiry has been sent successfully. Our team will get back to you soon.",
    appemailSent:
      "Email sent successfully to student to reminder for application!",
    passwordResetSent:
      "Updated Password Sent to your email, please check your email",
    siteCompanyRequested: "Site Company Requested",
    completeRegistration: "Registration completed",
    allRequestedSubscriptions: "All Subscriptions",
    requestEmailVerificationSubject: "Verify your email",
    requestEmailSubject: "Email Notification",

    sendSubscriptionActivationConfirmationSubject:
      "Subscription activated successfully",

    success: "Success",
    tokenIsValid: "Token is valid",
    clientUpdated: "Client Updated",
    passwordUpdated: "Password Updated",

    emailVerificationSent: "Email verfication link sent",
    subscriptionStatusChanged: "Subscription Status Changed",
    sendEmailConfirmationSubject: "Email verified successfully",
    requestSubscriptionActivationSubject: "Confirm your subscription",
    sendPassword: "Password Sent Successfully",
  },
  QUOTEREDIRECT: "http://localhost:5000/api/v1/client/",
  ROLES: {
    ALL: "all",
    USER: "student",
    ADMIN: "admin",
    PARTNER: "partner",
  },
  STATUS: {
    PAID: "PAID",
    ACTIVE: "ACTIVE",
    DELETED: "DELETED",
    PENDING: "PENDING",
    REJECTED: "REJECTED",
    APPROVED: "APPROVED",
    INACTIVE: "INACTIVE",
    COMPLETED: "COMPLETED",
    AVAILABLE: "AVAILABLE",
    UNAPPROVED: "UNAPPROVED",
  },

  MODALS: {
    USER: "User",

    CLIENT: "Client",
    COMPANY: "Company",
  },
  FORMATS: {
    DATE: "DD-MM-YYYY hh:mm",
  },
};
