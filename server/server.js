const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const { prisma } = require("./lib");
const passport = require("passport");
const morgan = require("morgan");
const hpp = require("hpp");
var admin = require("firebase-admin");
var serviceAccount = require("./serviceAccountKey.json");

const app = express();
app.use(cors());
app.use("/assets", express.static("assets"));
app.use("/uploads", express.static("uploads"));
app.use(express.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin,X-Requested-With,Content-Type,Accept,content-type,application/json"
  );
  next();
});
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL:
    "",
  storageBucket: process.env.BUCKET_URL,
});
app.locals.bucket = admin.storage().bucket();
// app.use(limiter);
app.use(express.urlencoded({ extended: false }));
// parse requests of content-type - application/json
app.use(bodyParser.json());
// Initializing Passport Middleware
app.use(morgan("dev"));
app.use(hpp());

app.use(passport.initialize());
app.use(passport.session());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// API routes

app.use("/api", require("./routes"));

// simple route
app.get("/", async (req, res) => {
  const feed = await prisma.user.findMany({});
  res.json({ message: "Welcome to  application.", feed });
});
//// Error Handler
app.use((error, req, res, next) => {
  console.log("Main Error =>", error);
  const message = error.message;
  const status = error.status || 500;
  res.status(status).json({ message: message, error: error });
});
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
