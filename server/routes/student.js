const express = require("express");
const upload = require("../middlewares/multer");

const router = express.Router();
const StudentContoller = require("../controllers/student");
const auth = require("../middlewares/auth");
router.post(
  "/createCourseDetail/:id",

  StudentContoller.createCourseDetail
);
router.post(
  "/createBasicDetail/:id",

  StudentContoller.createBasicDetail
);
router.post(
  "/createEducationDetail/:id",

  StudentContoller.createEducationDetail
);
router.post(
  "/createApplicantDetail/:id",

  StudentContoller.createApplicantDetail
);
router.post("/createTestScore/:id", StudentContoller.createTestScore);
router.delete("/deleteScore/:id", StudentContoller.deleteScore);
router.delete("/deleteDocument/:id", StudentContoller.deleteDocument);

router.post(
  "/createDocument/:id",
  upload("applicationDocuments", "file", "single"),
  StudentContoller.createDocument
);
// submit appplication
router.put("/submitApplication/:id", StudentContoller.submitApplication);

//get application
router.get("/getApplication/:id", StudentContoller.getApplication);
module.exports = router;
