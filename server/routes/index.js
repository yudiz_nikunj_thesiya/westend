const router = require("express").Router();
router.use("/v1/auth", require("./auth"));
router.use("/v1/student", require("./student"));
router.use("/v1/partner", require("./partner"));

router.use("/v1/admin", require("./admin"));

module.exports = router;
