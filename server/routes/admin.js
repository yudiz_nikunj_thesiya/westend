const express = require("express");
const router = express.Router();
const AdminContoller = require("../controllers/admin");
const auth = require("../middlewares/auth");

//get all users Route
router.get("/:role", AdminContoller.getAllUsers);
router.get("/", AdminContoller.getsearchPartner);
router.get("/search/filter", AdminContoller.getsearchStudent);
router.get("/application/get", AdminContoller.getAllApplications);

router.post("/createPartner", AdminContoller.createPartner);
router.post("/createStudent", AdminContoller.createStudent);
// approve partner
router.post("/approvePartner", AdminContoller.approvePartner);
// search student
router.get("/search/searchFilter", AdminContoller.searchFilterByStudent);

router.put("/generatePdf/:id", AdminContoller.generatePdf);

router.put("/updateStudent/:id", AdminContoller.updateStudent);
router.put("/updatePartner/:id", AdminContoller.updatePartner);

router.delete("/deleteUser/:id", AdminContoller.deletePartner);
router.delete("/deleteStudent/:id", AdminContoller.deleteStudent);

module.exports = router;
