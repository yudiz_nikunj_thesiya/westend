const express = require("express");
const upload = require("../middlewares/multer.js");
const AuthContoller = require("../controllers/auth");

const router = express.Router();

//register user Route

router.post("/signup", AuthContoller.signup);

//login user Route

router.post("/login", AuthContoller.login);

//social login user Route

router.post("/socialLogin", AuthContoller.socialLogin);

//login otp Route

router.post("/otpLogin", AuthContoller.otpLogin);

//verify otp Route

router.post("/verifyOtp", AuthContoller.verifyOtp);

//verify partner

router.post("/verifyPartner", AuthContoller.verifyPartner);

//verify student

router.post("/verifyStudent", AuthContoller.verifyStudent);
//checkEligibility

router.post("/checkEligibility", AuthContoller.CheckEligibility);

//contact us email

router.post("/contact", AuthContoller.ContactUs);

//request callback  email
router.post("/requestCalllback", AuthContoller.RequestCalllback);

//request callback  email
router.post("/applicationReminder", AuthContoller.ApplicationReminder);
//apply scholarship email
router.post("/applyScholarship", AuthContoller.applyScholarship);

//forex remittances email
router.post("/applyForex", AuthContoller.applyForexRemittance);

//student accommodation email
router.post(
  "/applystudentaccommodation",
  AuthContoller.applyStudentaccommodation
);

//apply sop email
router.post("/applySop", AuthContoller.applySop);

//forgot password Route

router.put("/forgot", AuthContoller.forgotPassword);
router.put("/reset-password", AuthContoller.ResetPassword);
// set password by user
router.put("/set-password", AuthContoller.SetPassword);
//get user
router.get("/:id", AuthContoller.getUser);
//get partner
//update password
router.put("/password/:id", AuthContoller.updatePassword);
//update profile
router.put(
  "/update/:id",
  upload("images", "image", "single"),
  AuthContoller.updateProfile
);

module.exports = router;
