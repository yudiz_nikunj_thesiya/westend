const JWT = require("jsonwebtoken");
const CustomError = require("../utils/customError");
const STRINGS = require("../utils/texts");
/**
 * If no role is passed the default role is user
 *
 * @param  {String} role role allowed to access the route
 */
function auth(role = STRINGS.ROLES.USER) {
  // roles = roles.length > 0 && roles : role.USER;

  return async (req, res, next) => {
    const header = req.get("Authorization");
    if (!header || !header.startsWith("Bearer")) {
      throw new CustomError(res, "Unauthorized access: Token not found", 401);
    }
    const token = header.split(" ")[1];
    const decoded = JWT.verify(token, process.env.JWT_SECRET);
    console.log(token, "token");
    const user = await prisma.user.findFirst({
      where: {
        id: decoded.id,
      },
    });
    if (!user)
      throw new CustomError(
        res,
        "Unauthorized access: User does not exist",
        401
      );
    if (role !== STRINGS.ROLES.ALL && role !== user.role)
      throw new CustomError(res, "Unauthorized access", 401);
    req.user = user;
    req.userId = user.id;

    next();
  };
}

module.exports = auth;
